#include "FishOperateMeta.h"
#include "Engine/Global.h"
#include "Engine/GameObject.h"	//引数にてGameObject型のオブジェクトを受け取るため、シーンのオブジェクトなどを取得しておくため。

#include "FishBase.h"
#include "FishCsvInfo.h"
#include "GroundOperateMeta.h"


//地面の情報を取得するためにアクセスする
//CSVファイルによる地面情報を取得
#include "GameMetaAI.h"






FishOperateMeta::FishOperateMeta() :
	FISH_COUNT_(FISH_MAX),	//魚数を、FishBaseクラスに登録してあるenumのMAX値で更新
	objectDispCounter_(new int[FISH_COUNT_]),
	SEA_POS_Y_(180.0f),	//１８０が海面
	pPlayer_(nullptr),
	VIEW_COST_MAX_(20),
	FIELD_COST_MAX_(VIEW_COST_MAX_ * 3),
	DEAD_SPACE_(3),
	viewCost_(0),
	fieldCost_(0)
{



	//配列要素の初期化
	for (int i = 0; i < FISH_COUNT_; i++)
	{
		//ポインタのポインタではないので必要なし
		////int型の動的確保（ポインタを複数個確保して、その一つ一つの要素にさらにポインタを確保）
		//objectDispCounter_[i] = new int;
		
		
		//中身を０で更新
		(objectDispCounter_[i]) = 0;
	}

}

FishOperateMeta::~FishOperateMeta()
{
	SAFE_DELETE(pFishCsvInfo_);

	/*for (int i = 0; i < FISH_COUNT_; i++)
	{
		SAFE_DELETE(objectDispCounter_[i]);
	}*/
	SAFE_DELETE_ARRAY(objectDispCounter_);	//new int[要素数]でかくほしているので、　中身の要素数の要素は動的確保ではない（実体を複数個確保した。つまり配列である）。なので解放処理は必要ない


}

void FishOperateMeta::Initialize(GameObject* pSceneObject, GameObject* pPlayer)
{
	//rand関数のランダムを時間で管理（ランダム性を増す）
	srand(unsigned int(time(NULL)));


	//引数にてもらった、シーンのオブジェクトポインタ、プレイヤーのポインタを
	//自身のメンバ変数に代入
	pSceneObject_ = pSceneObject;
	pPlayer_ = pPlayer;

	//Csvファイルから得タ情報からオブジェクトを作成するクラスの生成
		//引数にて、シーンのオブジェクトを送る（送ったシーンオブジェクトを親として、オブジェクトを生成）
	pFishCsvInfo_ = new FishCsvInfo(pSceneObject_);


	{
		//描画範囲内（Updateでは、補充を行う場合は、指揮範囲内に）
		//初期の魚のオブジェクトを生成（コスト分魚を生成させる）
			/*
				�@魚の所有する情報（Csvなどから取得）。コストや優先度、などからオブジェクトの選定
					生成時、選定の詳細は、RefillObject　（足りないコスト分補充する。魚生成関数）にて。

				�A生成されると、そのオブジェクトが持つ、標準のコストをFishOperateMetaが持っている、
				　指揮範囲内にいる、計算用の合計コストに加算される。

				�B計算用の合計コストが、コストの最大コスト値を超えているかの判定
					→超えているなら　繰り返しを終了
					→超えていないなら、�@〜�Bを繰り返す

			*/
	}


	//★現段階では、
	//基本は、キャラクタに自身の動きは制御させて、
		//★かつ、プレイヤーの一定範囲外を出たら、キャラクタを消去する、そして、コストが足りなくなったら、補充（キャラ出現）させる
	//★描画範囲内にキャラクタをを移動させて、画面に一定の数表示させる→このように、プレイヤーの動きをリアルタイムで動かし続けると、
		//→常に同じ魚が、キャラクタについてくる→という事態に陥る（それは、魚らしくない。魚は、自由にその場を泳ぐだけ）


	//指揮範囲内
	//プレイヤーの視野範囲内でない（描画範囲内でない）範囲にオブジェクトを出現
	//★初期は、プレイヤー描画範囲内、にオブジェクトを生成させる



	{



		//計算用の合計コストが、最大合計コストを超えないようにオブジェクトの補充
		//RefillObject(VIEW_RADIUS);



	}










}


void FishOperateMeta::Update(std::list<GameObject*> &lookObject)
{
	{
		//指揮メタAIにて操作するため、指揮対象のオブジェクトの型へとキャスト（キャラクタならGameObject型からキャラクタ型へキャスト）
		//拡張を行うために、
		//オブジェクトをFishBase（全キャラクタが継承するクラス）にてキャスト
			//FishBaseに、FishOperateMetaが指揮を行えるように、情報を持たせる。（コスト値など）
		//　！　FishBaseを継承しているクラスを拡張するが、この場合、FishBaseはGameObjectを継承しているので、すべてのGameObjctクラスはFishBaseに拡張することが可能である。！"
			//キャストは可能ではあるが、
			//実際にFishBaseを継承していないオブジェクトを、FishBaseで、キャストして、アクセスしようとすると、
			//エラーになるので注意（テスト済み）
	}
	//引数のリストを自身のメタAIにて操作する型にキャスト
	//★テンプレートの型にて、継承元のクラスに関数用意しておきたい
	//指揮メタAIにて操作するための、指揮対象のオブジェクトの型のリストを用意
	//そのリストに引数にてもらったリストの要素を一つ一つキャストして確保
	std::list<FishBase*> lookingObject;
	for (auto itr = lookObject.begin();
		itr != lookObject.end(); itr++)
	{
		//引数にて受け取った
		//要素一つ一つを、新規リストにキャストして追加
		//*itr = イテレータが指している要素の中身（GameObject*のポインタ）
		//(FishBase*)(*itr)　＝　GameObject*のポインタを（FishBase*）にキャスト（FishBase*はGameObject*）
		lookingObject.push_back((FishBase*)(*itr));

	}
	//引数は参照渡しで中身はアドレスのため、
	//そのアドレスの型をGameObject*からFishBase*という型にキャスト。
		//キャストを行おうとも、アドレスを参照するので、オブジェクトそのものを変更することができる。



	//�@
	//範囲外のオブジェクトを消去
		//指揮範囲内　外のオブジェクトを削除
	SearchFieldKillObject(lookingObject);

	//�A
	//�@により範囲外のオブジェクトを消去したことにより、
		//不足したコストを補充する
	RefillObject(VIEW_RADIUS);


	//当然FishBase*型のlookingObjectは、動的確保されたものを受け取っているだけなので、
		//ここで解放してはいけない。

	//距離を調べて、
	//距離が遠ければ、更新処理をさせない
		//或いは、距離が遠ければ描画しない


}





void FishOperateMeta::GetTopPriorityObj(XMVECTOR& randomPos, int& groundPlateNum, FISH_TYPE& fishType , 
	FISH_DISP_TYPE dispType)
{
	//ランダム位置の取得
		//指定範囲内のランダムの座標にオブジェクトを移動させる処理
	switch (dispType)
	{
	case VIEW_RADIUS : 
		//視野範囲内でランダムに位置を取得
		randomPos = SetRandomPosition(dispType , VIEW_RADIUS_X_, VIEW_RADIUS_Y_, VIEW_RADIUS_Z_);
		break;
	case FIELD_RADIUS : 
		//Y座標を除いた（どのエリアでも海面、海底の高さは大きくならない）
		//XZは描画範囲内（視野範囲外）の範囲内でランダムに位置を取得
		randomPos = SetRandomPosition(dispType, FIELD_RADIUS_X_, VIEW_RADIUS_Y_, FIELD_RADIUS_Z_);
		break;
	default:
		randomPos = SetRandomPosition(dispType, FIELD_RADIUS_X_, VIEW_RADIUS_Y_, FIELD_RADIUS_Z_);
		break;

	}
	




	//出現位置の環境を調べる（ランダム位置に一番近い地面のプレートをもらう）
	groundPlateNum = GetNearGroundPlate(randomPos);


	//現段階では０を入れておく
	//groundPlateNum = 0;
	if (groundPlateNum == -1)
	{
		groundPlateNum = 0;
	}


	//地面のプレート番号を受け取ったことにより
	//その地面番号（環境）と、現在の残コストと、優先度を込みした
	//最優先、一番適した魚オブジェクトの選定

	//現在のオブジェクト
		//配列の動的確保（ポインタのポインタではない）
	int* costByObject = new int[FISH_COUNT_];

	//


	for (int i = 0; i < FISH_COUNT_; i++)
	{
	//	//動的確保（ポインタのポインタでそれぞれ、要素のポインタにて宣言の定義がされただけ、ポインタだけが存在しているので、さらに確保しなければならない）
	//	costByObject[i] = new int;
		
	
		//配列を動的確保したので、
			//実体がFish_COUNT分存在するということ
		costByObject[i] = 0;

		//�@まず、全魚オブジェクトのコストをもらう（配列に登録）
		GetObjectDefaultCost(costByObject, i);

		//要素確認用
		int kk = (costByObject[i]);


		//※�Aで現在のコストを余裕で超えてしまうのは省こうと思ったが、なし。（現在のコストに足される値というのは、結局そのオブジェクトが持つそのものの標準のコスト→優先度込みのコストではない。）
			//上記を行う場合は、魚のタイプを示す番号と、コストを、構造体か何かで持っておけば、その可変長配列で管理できる。
		//�B優先度から、コストの増減を行う（主に減）
		PriorityCalcOfObjectCost(costByObject, i, groundPlateNum);
		//要素確認用
		kk = (costByObject[i]);

		//★ランダム性を増す一つの要素として、
		//指揮メタAIに、魚ごとに別のコストを持っておいて、魚を出現したときに、１匹ごとに、指揮メタAIに持っている、コスト類にその魚のコストの枠に入れる
		//魚のコスト計算の時に、そのコストも足すことで、同じ魚が出にくいようにコストの計算を行う
		//★魚オブジェクトからの関数呼び込みで、同時に処理する。
		//�Cオブジェクト数ごとのカウントをオブジェクトのコストに加算
		//オブジェクト数ごとにカウントされた
		//カウンターを現在のコストに加算
		(costByObject[i]) += (objectDispCounter_[i]);

		//確認用
		kk = (objectDispCounter_[i]);
		int ss = (costByObject[i]);
	}
	/*オブジェクトの出現にランダム性を与えたい

	//�C

	//これは、１つ１つCSVにアクセスする関数を呼び込み１つのオブジェクトごと、コストから優先度まで行い最小を求めるのか
	//或いは、全データを取り込み、優先度の計算を終えたのちに、最小を見つける判断を行うのか。
	//★
	//出現するオブジェクトが１パターンになる。
	//→出現位置と、優先度の計算から求められるオブジェクトは１種類だと思うので、
		//→？そうなると、初めからデータとして持っておくことが可能なのでは？（優先度の部分を、コストにしておけば？？）
	//現在表示中の魚と、被らないようにしたり、現在のイベントごとで、優先度が高くなるオブジェクトを決めたり。（いるかを出してほしいとかのイベントが出て、それに合わせて出現の優先度を高めたり。）
	//★位置パターンになってしまうと、そのエリアが、その魚の専用エリアのようになってしまう。

	*/



	//すべてのコストの計算が終了したのちに最小を出す
	//�D出現オブジェクトの選定
		//最小コストの選定	
	fishType = GetMinCostObjectType(costByObject);

	//解放
	/*for (int i = 0; i < FISH_COUNT_; i++)
	{
		SAFE_DELETE(costByObject[i]);
	}*/
	//ポインタを配列で複数実態を確保したので、
		//配列のポインタ解放だけでよい
	SAFE_DELETE_ARRAY(costByObject);








}

int FishOperateMeta::GetNearGroundPlate(const XMVECTOR& pos)
{
	//全地面プレートから
	//一番近い直線のベクトル（距離）を入れる変数
		//世界の広さによって、初期化の値を変更（どの地面プレートにおいてもこれ以上は離れることはないだろうという距離で初期化（一番近い距離がこれ以上になることはないように））
	XMVECTOR nearGroundPos = XMVectorSet(9999.0f, 9999.0f, 9999.0f, 0.0f);
	//一番近い地面の地面番号を入れる変数
	int groundPlateNum = -1;


	//ランダム出現位置の環境を調べる（ランダム位置に一番近い地面のプレートをもらう）


	//CSVの地面情報から、出現位置の環境を調べ、出現しやすい魚の選定
	//出現位置から、地面プレートのY座標が０としたときの地面の中央の座標から地面のサイズ分四方に伸ばした範囲（四角形）で、出現位置の座標が範囲内か、調べる、（これは、地面のオブジェクトのサイズと、位置がきちんと指定通りでないといけないので、それをそろえるのが大変ではあるが、）
	//（仮）に：地面のプレート０
	//０番目の地面の座標を取得
	for (int i = 0; i < GameMetaAI::GROUND_COUNT_; i++)
	{
		//引数ベクトルと、地面のワールド位置の直線距離を出す
			//メタAIにアクセスし、地面指揮メタAIに一つずつ、地面の座標をもらう。
		XMVECTOR length = XMVector3Length(pos - GameMetaAI::GetGroundPos(i));
		//Length関数は、XYZすべての座標に同じ値（直線距離）を入れる

		if (nearGroundPos.vecX > length.vecX)
		{
			//一番近い直線距離を更新
			nearGroundPos = length;
			//一番近い地面プレートの地面番号を更新
			groundPlateNum = i;
		}
	}

	//一番近いと判定の出た地面のプレート番号を返す
	return groundPlateNum;
}

void FishOperateMeta::GetObjectDefaultCost(int costByObject[],
	const int objectType)
{

	//コストを取得
		//FISH_TYPEにて渡すため。0オリジンから1オリジンへ
	(costByObject[objectType]) +=
		GameMetaAI::GetObjectInfoCsv(FISH_INFORMATION,
		(FISH_TYPE)(objectType + 1));


	//でバック時要素確認用
	int kk = (costByObject[objectType]);
	int s = 0;

}

void FishOperateMeta::PriorityCalcOfObjectCost(int costByObject[],
	const int objectType, const int groundPlateNum)
{
	//CSVより、優先度の文字列の取得
		//オブジェクトの番号は、0オリジンで受け取る　＝　1オリジンにする(わかりやすくするためにキャストした)
		//地面の番号は、0オリジンで受け取る　＝　1オリジンにする(わかりやすくするためにキャストした)
	std::string priority =
		GameMetaAI::GetObjectPriorityCsv(FISH_COST_GROUND_ENV,
		(FISH_TYPE)(objectType + 1), (GROUND_PLATE_HUNDLE)(groundPlateNum + 1));
	//文字列の中の一文字目（char１文字）から優先度の文字列を取得
		//Charの一文字を文字列から取得
	char priorityChar = priority[0];



	float percentage = 0;
	//優先度の文字列から、割合を出す。
		//優先度の割合は、上記のperの割合値を、現在のコストに掛ける
		//現在のコスト　+＝  現在のコスト * 優先度割合
	switch (priorityChar)
	{
	case 'S':
		percentage = -1.0f;	//−１　優先度の割合を−１でマイナスで取ることで、現在のコスト＊優先度割合で現在のコストを０にするような計算となる
		break;
	case 'A':
		percentage = 0.0f;	//０　　現在のコスト値変動なし
		break;
	case 'B':
		percentage = 0.3f;	//０．３
		break;
	case 'C':
		percentage = 0.6f;	//０．６
		break;
	case 'D':
		percentage = 1.0f;	//１．０	現在のコストを丸々増加
		break;
	default:
		percentage = 0.0f;	//例外　０変動なし
		break;
	}


	//現在のコストに、　現在のコスト＊割合分を足す
	(costByObject[objectType]) +=
		(int)(((costByObject[objectType]) * percentage));

	int kk = (costByObject[objectType]);
	int s = 0;


}

FISH_TYPE FishOperateMeta::GetMinCostObjectType(int costByObject[])
{
	//最小コストの選定
	int minCost = 999;	//コスト値で、これ以上のコストにはならないであろう値で初期化

	//初期のオブジェクトタイプとして
		//どの環境においても、出現してもおかしくないオブジェクトを初期値とする
	FISH_TYPE objectType = BUTTER_FLY_FISH;	//オブジェクトタイプは、　１オリジンのため、配列の添え字＋１した値を使用する

	//全オブジェクトのコストから最小のコストを持つオブジェクトの取得
	for (int i = 0; i < FISH_COUNT_; i++)
	{
		if (costByObject[i] < minCost)
		{
			//最小コストの更新
			minCost = costByObject[i];
			//オブジェクトのタイプの更新（１オリジンのため、＋１）
			objectType = (FISH_TYPE)(i + 1);
		}

	}

	//オブジェクトのタイプを返す
	return objectType;
}






bool FishOperateMeta::GroundMatch(XMVECTOR* matchPos,
	int modelHundle, RayCastData* pData, int code)
{

	if (modelHundle == -1)
	{
		return false;
	}

	//レイ発射位置確定
	pData->start = (*matchPos);

	//引数にて符号の値をもらう
	//符号の値をレイを飛ばす方向として使用する。
	code = code / abs(code);	//符号の値＝　符号の値 / 符号の値の絶対値（符号はそのままで、値を１にする）
								//符号にー１OR　1以外の値が入ったときのために、値を−１OR　１にする処理

	//レイの方向（上方向に地面があるかの判定）
	pData->dir = XMVectorSet(0.0f, (float)code, 0.0f, 0.0f);
	//★レイとオブジェクトのメッシュとで衝突しているかの判定
	//★オブジェクトのFBX,FBXパーツ（部品パーツ）とでメッシュ判定をとる

//判定の長さを更新（レイの長さを更新＝レイと判定を行って、その接触面との距離が以下のdist以下であるなら、distにその距離を更新させる）						
	//接触していても、dist以上の場合は、接触していない判定とすることもできる。
	//要するにレイの距離として使用することができる。（今回のレイ判定の計算量削減にはつながらないと思われる）
	pData->dist = 999999;

	//引数モデル番号のモデルと、引数レイとの当たり判定
	Model::RayCast(modelHundle, pData);




	//レイトの衝突判定を行った結果を受け取る
		//衝突しているとき
		//distには、発射位置から衝突位置までの距離が入る。
	if (pData->hit)
	{
		//衝突していたら、
		//ランダム位置のY座標を衝突した長さ分だけ、上にあげる。（地面に沿う位置に配置）
		(*matchPos).vecY += (pData->dist * (float)code);
	}


	//当たったかの判定を返す
	return pData->hit;
}



//ランダムの位置を取得
XMVECTOR FishOperateMeta::SetRandomPosition(FISH_DISP_TYPE dispType, int radiusX, int radiusY, int radiusZ)
{
	//プレイヤーからFIELD_RADIUS　〜　-FIELD_RADIUS 範囲内の（プレイヤー一定範囲（この空間を除いた距離））
	//プレイヤーの出現しない範囲 = プレイヤー位置　＋　プレイヤー位置から表示させたくない距離

	//方向をランダムで取得（＋２０を表示したくない範囲としたら-20 ~ 20の範囲を表示したくない
					//仮にランダム値が-30 だとしたら　20 -30 で、-10という表示したくない位置になる
					//それを避けるために、まず＋、−を決めて、その方向にランダムに範囲を決める）
				// + 1 or -1
	int code = RandCode();


	//ランダム位置の取得(xyzそれぞれ)
	XMVECTOR randPos = XMVectorSet(0, 0, 0, 0);
	XMVECTOR playerPos = pPlayer_->GetPosition();

	//引数の出現位置のタイプから
	//出現してはいけない範囲（プレイヤー位置を原点として出現してはいけない範囲・距離）の選定
	int notPos = 0;
	switch (dispType)
	{
	case VIEW_RADIUS : 
		notPos = VIEW_NOT_POS_;
		break;
	case FIELD_RADIUS : 
		notPos = FIELD_NOT_POS_;
		break;
	default : 
		notPos = FIELD_NOT_POS_;
	}



	//各方向のランダム座標値　＝　符号＊表示させない範囲　＋　符号＊プレイヤーオブジェクトを原点とした半径分の範囲
	randPos.vecX = (code * notPos) + (code * (playerPos.vecX + rand() % radiusX));
	code = RandCode();	//１つの軸の座標を取得後に符号を再び取得
	randPos.vecY = (code * notPos) + (code * (playerPos.vecY + rand() % radiusY));
	code = RandCode();	//１つの軸の座標を取得後に符号を再び取得
	randPos.vecZ = (code * notPos) + (code * (playerPos.vecZ + rand() % radiusZ));

	//ランダム位置をセットしたベクトルを返す
	return randPos;
}

//符号を取得
int FishOperateMeta::RandCode()
{

	//−１を取得して起き
	int code = -1;

	// -1 OR 1の値に
	int ran = rand() % 2; // 0 OR 1
	//０の場合は、‐１を符号とする
	//１の場合は、１を符号とする
		//ranの値をそのまま符号へ代入
	if (ran == 1)
	{
		code = ran;
	}

	return code;
}



void FishOperateMeta::RefillObject(FISH_DISP_TYPE dispType)
{
	//★コストを超えないように、これ以上コストを払って出現できるオブジェクトがいなくなるまでオブジェクトを出現
	//★繰り返し条件：合計最大値と現在のコスト値の差が3(DeadSpace(この差までは、超えていない、下回っていない。と判断する。))以上なら
	//オブジェクトの削除などによる、（コストの減算は、オブジェクト消去時に、関数を呼ぶようにしているため、この時点で、最新のコストを所有している）
	//不足分のコストの補充（オブジェクトを生成し、コストを増やす(じっさいいにコストを加算するのは、オブジェクトを生成し、オブジェクト側が、コスト加算の関数を呼んだとき)）
	{
		//現在のコストを参照し、現コストが、MAXコスト値との差を一定量以上離しているとき（コストが足りていない）
		//差がDEAD_SPACE_以上　
		if(FIELD_COST_MAX_ - fieldCost_ > DEAD_SPACE_)
		{
			/*
			{
				//コストの不足分、オブジェクトの補充を行う（オブジェクトの生成）
				//生成
				FishBase* fishObject = Instantiate<ButterflyFish>(pSceneObject_);

				//オブジェクトの座標を範囲内にランダムに設置
				fishObject->SetPosition(
					//指定範囲内のランダムの座標にオブジェクトを移動させる処理
					SetRandomPosition(VIEW_RADIUS_X_, VIEW_RADIUS_Y_, VIEW_RADIUS_Z_)
				);
			}
			*/

			//オブジェクト生成を行うクラスにアクセスし、
			//enum値により、生成するオブジェクトを決定し、該当のオブジェクトを生成する
			{


				//オブジェクトを1匹生成
					//オブジェクトのタイプの選定や、生成したオブジェクトの位置管理なども行う
				CreateOneObject(dispType);


			}
		}

	}
}

//１匹のオブジェクトの生成
	//生成するオブジェクトの選定（特定条件によりランダムに選定）
	//オブジェクトの位置の決定
void FishOperateMeta::CreateOneObject(FISH_DISP_TYPE dispType)
{




	FishBase* fishObject;

	//ランダム座標を入れる変数
	XMVECTOR randomPos = XMVectorSet(0, 0, 0, 0);
	//地面プレート番号
	int groundPlateNum = 0;
	//初期オブジェクトタイプとして標準の魚を実装
	FISH_TYPE fishType = BUTTER_FLY_FISH;

	//上記にて宣言した変数を参照渡しで受け取り、変数の中身を更新
	//特定範囲のランダム位置
	//ランダム位置に該当する地面のプレート番号
	//優先度などから選定された生成オブジェクトタイプ
	GetTopPriorityObj(randomPos, groundPlateNum, fishType , dispType);



	//引数にて指定されたオブジェクトを生成する関数
	//関数内で、引数にて渡された値にて識別されるオブジェクトのInstantiateを呼び込む
	fishObject = (FishBase*)pFishCsvInfo_->CreateObject(fishType);


	////★★現段階では地面のプレートは１つなので、
	////★★ここで０番目のプレートに初期化
	//groundPlateNum = 0;






	//上記から地面情報を取得し（地面のプレートの番号を取得）
	//その位置にて、出現する魚のオブジェクトを選別（どの魚を出現させるか、）
	//引数にて渡した、地面プレートの番号のモデル番号を取得
	int modelHundle = GameMetaAI::GetGroundModelHundle(groundPlateNum);


	//その地面との出現位置とで、レイキャストによる、当たり判定
	//地面のプレートへ向けて
	//�@まず、自身より上に地面がないことを確認、（この場合、あまり地面が急すぎると、埋まるので注意）
		//→地面があったなら、その位置まで自身の座標を上げる、
		//地面の座標を出す、（つまりその座標における地面のY座標を求める）
	//★ランダム位置のからレイを飛ばして地面の座標を求める
	//レイキャストの情報初期化
	RayCastData* pData = new RayCastData;


	//ランダム位置の座標をその地面に沿う座標に移動させる（レイとの衝突判定を使用して）
	if (!(GroundMatch(&randomPos, modelHundle, pData, -1)))//地面に沿わせる座標ベクトル、レイデータ、方向の符号
	{
		//地面との衝突がされなかったとき
		//引数にて指定した方向には地面がなかったということなので、
		//逆の方向に、レイを飛ばす。
		if (!GroundMatch(&randomPos, modelHundle, pData, 1))
		{
			GroundMatch(&randomPos, GameMetaAI::GetWorldGroundHundle(), pData, -1);
		
		};
		
	
	};
	//上記のレイによる当たり判定により、衝突した地面の沿う位置にY座標を移動させる。


	//地面と
	//衝突していた時

	//出現する高さの割合をCSVから取得し、
		//海面と海底で直線距離を出し、海面を０海底を１として、取得した割合の位置ベクトルを出現位置とする
	if (pData->hit)
	{
		




		//�BCSV座標から、その環境における魚の出現のY座標値（海面なら、海面近くに出現させるように位置を調整するし、Lowといわれたら、地面より上の段階に位置を調整、地面といわれたら、その地面の位置に調整するし）
			//★だが、あまり、決めすぎると、一定の位置に同じ魚がたくさん出現という事態になりかねない。ある程度のランダム性も必要か？？？（今は良い）
			//その環境における、その魚の出現位置の高さ、を調整。
		//（地面には埋まらず、）
			//ベクトル間の位置で、どの位置にするかという調整は、→Learpという関数でベクトル間の位置の間の座標は出すことが可能（Learp＝直線のベクトルを０〜１の指定float値で直線間のベクトルを出すことが可能）
			//０〜１の割合で高さの調整をするので、高さにランダム性が持てなくなる
		float heightPer = GameMetaAI::GetObjectHeight(groundPlateNum, fishType);	//地面番号と、魚オブジェクトのタイプを送り、高さ（独自に決めた割合に割り当てるため）

		//割合を直接受け取ったため、
		//海面から、地面までの直線のベクトル（0.0f ~ 1.0f）の中で割合が示す位置のベクトル取得
		//引数：割合０にて示されるベクトル位置
		//引数：割合１にて示されるベクトル位置
		//引数：割合（０．０ｆ〜１．０ｆ）
		//差ベクトル　＝　random座標 - （randomX,海面Y値、randomZ）;
		//海面から地面へ向けたベクトル
		randomPos = XMVectorLerp(
			XMVectorSet(randomPos.vecX, SEA_POS_Y_, randomPos.vecZ, 0),
			randomPos,
			heightPer);

		//この時点で、
		//魚のタイプが、チンアナゴであった場合、
		//問答無用で
		//地面の高さ（ここにおける地面とは世界の地面の高さ）に設置して終了
		if (fishType == SPOTTED_GARDEN_ELL)
		{
			//Y座標が、海底の高さより２０ｍ上の場合、
				//それは、海底ではなく、ほかのオブジェクトであったということになる。
				//そのため、Y座標を海底まで下げて、
				//出現
			if (randomPos.vecY >= -380)
			{
				//だが、　このままでは、地面に置かれているオブジェクト（サンゴや岩など）に埋もれてしまう可能背がある。
				randomPos = XMVectorSet(randomPos.vecX, -390.0f, randomPos.vecZ, 0);
			
			}
			
		}

	}
	else
	{
		//チンアナゴは、
		//特別、地面に埋まって出現しないといけないので、
			//強制的に地面の高さに出現させる
		if (fishType == SPOTTED_GARDEN_ELL)
		{
			//だが、　このままでは、地面に置かれているオブジェクト（サンゴや岩など）に埋もれてしまう可能背がある。
			randomPos = XMVectorSet(randomPos.vecX, -390.0f, randomPos.vecZ, 0);
			
		}
		else
		{

			//ランダム値０〜１０ / 10.0f
				//＝求められる値＝0.0f ~ 1.0f
			//０〜１の割合として使用する
			float randomPer = (float)(rand() % 11 / 10.0f);


			//2つのレイと当たらなかった場合、
					//オブジェクトの位置を海面から(海面/2)の位置の高さでランダムの位置を取得する
					//Y座標が海面〜(海面/2)の高さの間の位置で取られる。
			randomPos = XMVectorLerp(
				XMVectorSet(randomPos.vecX, SEA_POS_Y_, randomPos.vecZ, 0),
				XMVectorSet(randomPos.vecX, SEA_POS_Y_ - SEA_POS_Y_ - SEA_POS_Y_, randomPos.vecZ, 0),
				randomPer);

			//上記の処理により、
			//地面プレートがない位置に出現しても、ある程度のランダムな高さに出現するようにする。
		}
	}

	//割合にて出された割合を設定
	fishObject->SetPosition(randomPos);


	//レイキャストに用いる構造体の解放
	SAFE_DELETE(pData);

}

void FishOperateMeta::UpdateAndDrawExe(GameObject* pGameObject)
{

		//Update許可範囲内か調べる
			//許可範囲内であれば、関数内にてUpdate許可
		InRange(pGameObject , UPDATE_RANGE_, UPDATE_TYPE_);
		//Draw許可範囲内か調べる
		InRange(pGameObject, DRAW_RANGE_, DRAW_TYPE_);

}

void FishOperateMeta::InRange(GameObject* pGameObject , int range, TYPE type)
{
	//ウツボの位置を取得
	XMVECTOR objPos = pGameObject->GetPosition();
	objPos = XMVectorSet(objPos.vecX, objPos.vecY, objPos.vecZ, 0);


	//プレイヤーの現在値取得
	XMVECTOR playPos = pPlayer_->GetPosition();
	playPos = XMVectorSet(playPos.vecX, playPos.vecY, playPos.vecZ, 0);



	//プレイヤーと地面との距離をとる
	float length = XMVector3Length(objPos - playPos).vecX;


	if (length > range)
	{
		//範囲を超えた時
		if (type == UPDATE_TYPE_)
		{
			//Updateを拒否
			pGameObject->Leave();
		}
		else if (type == DRAW_TYPE_)
		{
			//Drawを拒否
			pGameObject->Invisible();
		}
	}
	else
	{
		//範囲を超えた時
		if (type == UPDATE_TYPE_)
		{
			//Updateを許可
			pGameObject->Enter();
		}
		else if (type == DRAW_TYPE_)
		{
			//Drawを許可
			pGameObject->Visible();
		}

	}

}


//リストのアドレスを受け取り
//オブジェクトそのもののアドレスを示す
void FishOperateMeta::SearchFieldKillObject(std::list<FishBase *> &lookingObject)
{

	//動作テスト
	//★プレイヤーを原点に、指揮範囲外かどうかを調べ、指揮範囲外であればオブジェクトの削除
	//本来は消去されるオブジェクトは、プレイヤーの視認できない距離で消去されることになる（視認できない距離まで指揮範囲が広がっている）
	//範囲内かどうか調べる方法
	//＝プレイヤーのベクトル - オブジェクトのベクトルにて距離を出す（距離はLength関数にて出すことが可能）
	//＝その距離が、指揮範囲の長さより
	//短いなら：範囲内
	//長いなら：範囲外
	//総当たりになってしまうので、重くはなる
	{
		//保存用プレイヤーベクトル
		XMVECTOR playVec = pPlayer_->GetPosition();
		//4次元の値を０で初期化
		playVec = XMVectorSet(playVec.vecX, playVec.vecY, playVec.vecZ, 0);


		//X値の距離と、Z値の距離をそれぞれ別に取得する
			//計算量はｘｙｚの３次元で距離を求めるときより２倍になるが、
			//斜めの距離ではなく、直線のまっすぐな距離での離れ具合による消去のほうが、確実に距離による消去管理を行えると思ったため以下の方式をとる
		for (auto itr = lookingObject.begin();
			itr != lookingObject.end(); itr++)
		{
			////計算用プレイヤーベクトル
			//XMVECTOR playVecCalc;

			//保存用オブジェクトベクトル
			XMVECTOR objVec = (*itr)->GetPosition();

			//計算用オブジェクトベクトル
			XMVECTOR objVecCalc;



			//プレイヤーと各オブジェクトのベクトル間の長さを取得
			//X方向のベクトル間の長さ
				//X方向以外を共通の高さにしてその距離を出す。
				//オブジェクトのベクトルを、プレイヤーのベクトルに合わせる
			objVecCalc = XMVectorSet(objVec.vecX, playVec.vecY, playVec.vecZ, 0);
			float length = XMVector3Length(playVec - objVecCalc).vecX;	//ベクトル間の距離を求める（YZ値は、同じ値のため、X値の差を求める）
			//差ベクトルの長さが
			//ｘの範囲外（距離以上）であれば消去処理
			if (length >= FIELD_RADIUS_X_)
			{
				//範囲外であると示されたオブジェクトを消去
				(*itr)->KillMe();


				//消去した（この段階では消去されていないが、次のフレームの*itrのオブジェクトの親のUpdateSubの時点で消去される）
				//消去しオブジェクトのコストを回復させる
				//各オブジェクト内で減算の関数を呼び込むようにする→書き忘れた場合、コストが残ったままになってしまって困るので注意。
					//、メタAIだけが、　オブジェクトの消去を行うわけではない
			}
			//上記の時点で次のオブジェクトに行きたいが、
				//breakをするとforを抜けてしまうし、
				//itr++;して、continueは、あまりcontinueを使いたくはない。
				//そのため、else分にする
			else
			{
				//プレイヤーと各オブジェクトのベクトル間の長さを取得
				//Z方向のベクトル間の長さ
				objVecCalc = XMVectorSet(playVec.vecX, playVec.vecY, objVec.vecZ, 0);
				length = XMVector3Length(playVec - objVecCalc).vecX;	//ベクトル間の距離を求める（YZ値は、同じ値のため、X値の差を求める）
																			//Length関数の戻値のベクトルには、xyzすべてに同じ値が入る（距離の長さがfloatで返ってくる）

				//差ベクトルの長さが
				//zの範囲外（距離以上）であれば消去処理
				if (length >= FIELD_RADIUS_Z_)
				{
					//範囲外であると示されたオブジェクトを消去
					(*itr)->KillMe();
				}
				else
				{
				//まだ消去されないとき
					//距離からUpdateを行うかの処理を行う
					//距離からDrawを行うかの処理を行う
					UpdateAndDrawExe(*itr);
				
				}
			
			}



			////差ベクトルを取得
			//XMVECTOR difference = pPlayer_->GetPosition() - (*itr)->GetPosition();

			////差ベクトルの長さを取得（ｘｙｚの要素にそれぞれ共通して同じ長さの値が入る）
			//float length = XMVector3Length(difference).vecX;


			////差ベクトルの長さが
			////ｘｙｚそれぞれ１つでも指揮範囲外であるならば
			//if (length >= FIELD_RADIUS_X_ ||
			//	length >= FIELD_RADIUS_Z_)
			//	//				length >= FIELD_RADIUS_Y_ ||	//Y座標は、ほかのXZに比べて、みじかくせっていされているので、　そのため、Y軸の判断がtrueになれば｜｜なので、条件が通ってしまう
			//{
			//	//範囲外であると示されたオブジェクトを消去
			//	(*itr)->KillMe();

			//	//消去した（この段階では消去されていないが、次のフレームの*itrのオブジェクトの親のUpdateSubの時点で消去される）
			//	//消去しオブジェクトのコストを回復させる
			//	//各オブジェクト内で減算の関数を呼び込むようにする→書き忘れた場合、コストが残ったままになってしまって、困るが、メタAIだけが、　オブジェクトの消去を行うわけではない
			//}




		}
	}
}

void FishOperateMeta::Release()
{
	//ポインタも、引数でアドレスをもらって、
	//それを自身のポインタに取得しているだけなので、→解放はなし
}

void FishOperateMeta::AddCost(int addCost)
{
	fieldCost_ += addCost;

}

void FishOperateMeta::AddObjectCounter(int type, int addCost)
{
	//オブジェクトごとに持つ、コストのカウンターに
		//引数にて渡された、オブジェクトの標準コストの、半分の値をコストとして加算
	(objectDispCounter_[(type - 1)]) += (addCost / 2);

}

void FishOperateMeta::SubCost(int subCost)
{
	fieldCost_ -= subCost;
}

void FishOperateMeta::SubObjectCounter(int type, int subCost)
{
	//加算した分減算
	(objectDispCounter_[(type - 1)]) -= (subCost / 2);
}

//インクルード
#include "NoMove.h"
#include "FishBase.h"


//コンストラクタ
NoMove::NoMove(GameObject* pGameObject)
{
	pObject_ = pGameObject;
	pFish_ = dynamic_cast<FishBase*>(pObject_);
}

//デストラクタ
NoMove::~NoMove() 
{
}

//実行可能か
bool NoMove::ShouldExecute()
{
	//タイミング指定なし
	return !pFish_->IsCongnition();
}

//実行
void NoMove::Execute()
{
	//動作なし
	return;
}

#pragma once
#include "Behavior.h"
#include <string>

class RunAway : public Behavior
{
	XMVECTOR objectPos_;
	XMVECTOR playerDir_;
	float speedMag_;		//逃げる際の速度倍率
	float rotate_;
	
	float MAX_ROTATE_;
	/*XMVECTOR rightRange_;
	XMVECTOR leftRange_;*/

public:
	
	//コンストラクタ
	//params : ゲームオブジェクトポインタ、どのオブジェクトから逃げるのか
	//return : NULL
	RunAway(GameObject* pGameObject, std::string target);

	//デストラクタ
	//params : NULL
	//return : NULL
	~RunAway();

	//実行可能か
	//params : NULL
	//return : 実行可能ならばTrue
	bool ShouldExecute() override;

	//実行
	//params : NULL
	//return : NULL
	void Execute() override;
};


#include "Advance.h"
#include "FishBase.h"

Advance::Advance(GameObject* pGameObject)
{
	pObject_ = pGameObject;
	pFish_ = dynamic_cast<FishBase*>(pObject_);
}

Advance::~Advance() {
}

bool Advance::ShouldExecute()
{
	//認識(衝突)したかどうか
	return !pFish_->IsCongnition();
}

void Advance::Execute()
{

	//前進
	XMVECTOR move = XMVectorSet(0, 0, pFish_->GetMoveSpeed() , 0);
	move = XMVector3TransformCoord(move, XMMatrixRotationX(XMConvertToRadians(pObject_->GetRotate().vecX)));
	move = XMVector3TransformCoord(move, XMMatrixRotationY(XMConvertToRadians(pObject_->GetRotate().vecY)));
	pObject_->SetPosition(pObject_->GetPosition() + move);
}

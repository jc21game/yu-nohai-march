//インクルード
#include <assert.h>
#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "PlayerCamera.h"
#include "Engine/BoxCollider.h"
#include "Engine/SceneManager.h"


//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"),
	moveStrength_(3.0f), rotateVolume_(3.0f), mouseSensitivity_(0.2f), maxAngle_(60.0f), moveAngleSpd_(1.0f),
	rotateMatX_(XMMatrixRotationX(ROTATE_X)), rotateMatY_(XMMatrixRotationY(ROTATE_Y)), hitDownFlag_(0), hitStreatFlag_(0), disGroundFlag_(false),
	fitGroundAngle_(0), splitAngle_(15.0f), upRest_(150.0f), rightRest_(2060.0f), leftRest_(-820.0f), streatRest_(2060.0f),
	backRest_(-820.0f), restFlag_(0), adMove_(0.1f)
{

	collCenter_ = XMVectorSet(0.f, 5.f, 0.f, 0.f);
	collSize_ = XMVectorSet(3.f, 3.f, 10.f, 0.f);

}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//プレイヤーのカメラに関する動作関数
	PlayerCamera::SetPlayerCamera(this);

	BoxCollider* pCollider = new BoxCollider(collCenter_, collSize_);
	AddCollider(pCollider);


	//transform_.position_.vecX = 2000;
}

//更新
void Player::Update()
{
	//プレイヤーの移動に関する動作関数
	BlockingPlayerMove();

	//マウス操作なのかキーボード操作なのか関数
	BlockingRotate();

	//プレイヤーのカメラに関する動作関数
	PlayerCamera::SetPlayerCamera(this);
}

//描画
void Player::Draw()
{
}

//開放
void Player::Release()
{
}

void Player::ChangeGroundModelHundle(int hGroundModel)
{
	//メンバ変数のモデルハンドルを変更
	hRayCastModel_.hGroundModel = hGroundModel;
}

void Player::ChangeMorayeelModelHundle(int hMorayellModel)
{
	//メンバ変数のモデルハンドルを変更
	hRayCastModel_.hMorayEel = hMorayellModel;
}

void Player::SetWorldGroundModelHundle(int hWorldGround)
{
	//メンバ変数のモデルハンドルにセット
	hRayCastModel_.hWorldGround = hWorldGround;
}

void Player::OnCollision(GameObject * pTarget)
{

}

//プレイヤーの移動に関する動作関数
void Player::BlockingPlayerMove()
{
	/*
	*
	* 移動距離を計算(正規化後設定移動距離を掛ける)
	* X:必ず0
	* Y:必ず0
	* Z:入力があったらmoveStrength_、なかったら0
	* その後プレイヤーの向いている方向に移動方向を変換
	*
	*/

	//制限フラグの初期化
	restFlag_ = 0;

	//移動距離
	XMVECTOR moveVec = XMVectorSet(0, 0, moveStrength_ * Input::IsKey(DIK_W), 0);

	//上記移動量をプレイヤーの向きによって回転
	moveVec = XMVector3TransformCoord(moveVec, rotateMatX_);		//移動量ベクトルをrotateMatX_行列で回転
	moveVec = XMVector3TransformCoord(moveVec, rotateMatY_);		//移動量ベクトルをrotateMatY_行列で回転

																	//地面との当たり判定処理
	Player::RayShot();

	//レイが地面(正面・真下)に触れていたら
	if (hitStreatFlag_ > 0 || hitDownFlag_ > 0)
	{
		//プレイヤーの位置を上げる
		transform_.position_.vecY += 0.5f;
		//上下回転の限界角度内の場合
		if (transform_.rotate_.vecX > -56)
		{
			//調整角度分だけ回転
			transform_.rotate_.vecX -= diffAngle_;
		}
		//キー入力での移動量(Z方向)の半分をプレイヤーに適用
		transform_.position_.vecZ += moveVec.vecZ * 2.5f;
	}

	//レイが地面に当たっていたら
	//どちらかのレイが触れていたら動かない
	if ((hitDownFlag_ + hitStreatFlag_) < 0)
	{
		//プレイヤーの移動
		MoveRest(moveVec);
	}
}

//キー入力チェック関数
int Player::CheckInput(int keyIDPlus, int keyIDMinus)
{
	/*
	*
	* キー入力用のキーコードを二つ渡し
	* 1つ目のキー入力を確認できたら 1を返し
	* 2つ目のキー入力を確認できたら-1を返す
	* ただし、ともにtrueであったり、ともにfalseの場合には0を返す
	*
	*/

	//一つ目の結果 - 二つ目の結果を返す
	return (int)Input::IsKey(keyIDPlus) - (int)Input::IsKey(keyIDMinus);
}

//マウス操作なのかキーボード操作なのか関数
void Player::BlockingRotate()
{
	//マウス操作時
	if (PlayerCamera::GetOperate())
	{
		//マウス操作時の方向転換関数
		BlockingMouseRotate();
	}
	//キーボード操作時
	else
	{
		//プレイヤーの回転に関する動作関数
		BlockingPlayerRotate();

		//プレイヤーを上・下に向かせる(遊泳)関数
		//BlockingPlayerSwim();
	}
}

//マウス操作時の方向転換関数
void Player::BlockingMouseRotate()
{
	/*マウスで方向転換*/
	//移動後の上下向き
	float rotateX = transform_.rotate_.vecX + Input::GetMouseMove().vecX;

	//向いても大乗な場合
	if (rotateX >= -maxAngle_ && rotateX <= maxAngle_)
		//X成分は角度指定を満たしていたら回転
		transform_.rotate_.vecX += Input::GetMouseMove().vecX;

	//Y成分は回転
	transform_.rotate_.vecY += Input::GetMouseMove().vecY * mouseSensitivity_;

	//回転行列の更新
	rotateMatX_ = XMMatrixRotationX(ROTATE_X);		//プレイヤーのX軸の現在の値をラジアン度で行列化
	rotateMatY_ = XMMatrixRotationY(ROTATE_Y);		//プレイヤーのY軸の現在の値をラジアン度で行列化
}

//プレイヤーの回転に関する動作関数
void Player::BlockingPlayerRotate()
{
	//Aキー入力で左回転しDキー入力で右回転
	transform_.rotate_.vecY += rotateVolume_ * CheckInput(DIK_D, DIK_A);

	//回転行列の更新
	rotateMatX_ = XMMatrixRotationX(ROTATE_X);		//プレイヤーのX軸の現在の値をラジアン度で行列化
	rotateMatY_ = XMMatrixRotationY(ROTATE_Y);		//プレイヤーのY軸の現在の値をラジアン度で行列化
}

//プレイヤーを上・下向にかせる(遊泳)関数
void Player::BlockingPlayerSwim()
{
	//キー入力によって回転した場合の値を求める
	float rotateX = transform_.rotate_.vecX + moveAngleSpd_ * CheckInput(DIK_LSHIFT, DIK_SPACE);

	//地面との距離が9以上のとき、
	//かつ、
	//キー入力によって回転した場合の値が地面に沿わせるまでの角度の値よりも小さい場合
	if (disGroundFlag_ && rotateX < fitGroundAngle_)
	{
		//スペース入力で上を向く
		rotateX = transform_.rotate_.vecX + moveAngleSpd_ * -(int)Input::IsKey(DIK_SPACE);
	}

	//可変領域内チェック
	if (rotateX >= -maxAngle_ && rotateX <= maxAngle_)
		transform_.rotate_.vecX = rotateX;			//角度変更
}

//地面との当たり判定処理
void Player::RayShot()
{
	//true = 衝突していない
	//false = 衝突した
	bool flg = true;										//プレイヤーの回転に関するフラグ

															//伊達追記
															//追記理由：地面のモデルハンドルをInitializeにて確保。かつ、一番近い地面のモデルハンドルをメタAIから取得するようにする
															//Ground* pGround = (Ground*)FindObject("Ground");		//ステージオブジェクトを探す
															//int hGroundModel = pGround->GetModelHundle();			//モデル番号を取得




															//伊達追記
															//地面のモデルハンドルにー１が入る
															//メタAIから、当たり判定を行わなくてよいといわれているとき
															//そのため、レイキャストの処理をカットする
															//レイに衝突したときにそれぞれ処理を行っているので、そもそもレイを発射させなくすれば、処理は早いはず
	//if (hRayCastModel_.hGroundModel == -1)
	//{
	//	//レイキャストは行わずに、制限なしに移動可能にする

	//	//前進に進めるようにする
	//	hitStreatFlag_ = -1;
	//	//真下に進めるようにする
	//	hitDownFlag_ = -1;
	//	//回転フラグを更新
	//	disGroundFlag_ = false;

	//}
	//else
	{

		//レイキャストの実行
		//地面モデルとのレイキャスト判定
		if (flg &&
			hRayCastModel_.hGroundModel != -1)
		{
			ExecutionRayCast(&flg, hRayCastModel_.hGroundModel);
		}

			/*地面との衝突をしていない
			かつ、
			ウツボのモデルハンドルがー１でない（衝突判定を行うモデルがいる）*/
			if (flg &&
				hRayCastModel_.hMorayEel != -1)
			{
				//レイキャストの実行
				//ウツボモデルとのレイキャスト判定
				ExecutionRayCast(&flg, hRayCastModel_.hMorayEel);

			}

			//ウツボとの衝突をしていない
			//かつ、
			//世界の地面のモデルハンドルがー１でない（衝突判定を行うモデルがいる）
			if (flg &&
				hRayCastModel_.hWorldGround != -1)
			{
				//レイキャストの実行
				//世界の地面モデルとのレイキャスト判定
				ExecutionRayCast(&flg, hRayCastModel_.hWorldGround);

			}


	}


	//	//プレイヤーを上・下に向かせる(遊泳)関数
	if (flg)
	{
		BlockingPlayerSwim();
	}
	

}


void Player::ExecutionRayCast(bool* flg, int hRayCastModel)
{
	XMMATRIX rotateMatXY = GetRotMatX() * GetRotMatY();		//プレイヤーの回転


															//正面のレイ
	RayCastData dataStreat;
	XMVECTOR streatPos = { transform_.position_.vecX,transform_.position_.vecY,transform_.position_.vecZ + 2,0 };
	dataStreat.start = streatPos;							//レイの発射位置をセット
	XMVECTOR dire = { 0,0,1.0f,0 };							//レイの方向ベクトル
	dire = XMVector3TransformCoord(dire, rotateMatXY);		//プレイヤーの回転を適用
	dataStreat.dir = dire;									//レイの方向をセット
	Model::RayCast(hRayCastModel, &dataStreat);				//レイを発射
															//レイが当たったら
	if (dataStreat.hit)
	{
		float dis = dataStreat.dist;
		//レイが地面に触れている場合
		if (dis <= 8)
		{
			//移動フラグを更新
			hitStreatFlag_ = 1;

			//回転角度を求める
			XMVECTOR playerDire = dire;									//プレイヤーのベクトル
			XMVECTOR wallNormal = dataStreat.normal;					//当たった地面の法線ベクトル

			playerDire = XMVector3Normalize(playerDire);				//正規化
			wallNormal = XMVector3Normalize(wallNormal);				//正規化

			float dot = XMVector3Dot(playerDire, wallNormal).vecX;		//内積を求める
			float angle = acos(dot);									//acosを求めれば角度
			diffAngle_ = XMConvertToDegrees(angle);						//ラジアンを角度に変更

			rotateMax_ = 3.0f;											//一回で調整できる最大角度
			rotateMin_ = 0.05f;											//一回で調整できる最小角度
																		//調整したい角度が90度以上の場合
			if (diffAngle_ > UNDER_RECTANGULAR)
			{
				//一回の処理で調整したい角度を求める
				diffAngle_ = (OVER_RECTANGULAR - diffAngle_) / splitAngle_;
			}

			//それ以外の場合
			else
			{
				//一回の処理で調整したい角度を求める
				diffAngle_ = (UNDER_RECTANGULAR - diffAngle_) / splitAngle_;
			}

			//最大角度よりも大きい場合
			if (diffAngle_ > rotateMax_)
				diffAngle_ = rotateMax_;		//最大角度値を設定
												//最小角度よりも小さい場合
			else if (diffAngle_ < rotateMin_)
				diffAngle_ = rotateMin_;		//最小角度値を設定

												//回転フラグを更新
			*flg = false;
		}
		//それ以外
		else
		{
			//移動フラグを更新
			hitStreatFlag_ = -1;
		}
	}

	//真下のレイ
	RayCastData dataDown;
	XMVECTOR DownPos = { transform_.position_.vecX,transform_.position_.vecY - 2,transform_.position_.vecZ + 2,0 };
	dataDown.start = DownPos;								//レイの発射位置をセット
	XMVECTOR Down = XMVectorSet(0, -1, 0, 0);				//レイの方向ベクトル
	Down = XMVector3TransformCoord(Down, rotateMatXY);		//プレイヤーの回転を適用
	dataDown.dir = Down;									//レイの方向をセット
	Model::RayCast(hRayCastModel, &dataDown);				//レイを発射
															//レイが地面に当たっている場合
	if (dataDown.hit)
	{
		float dis = dataDown.dist;
		//レイが地面に触れている場合
		if (dis <= 8)
		{
			//移動フラグを更新
			hitDownFlag_ = 1;

			//回転角度を求める
			XMVECTOR playerDire = dire;									//プレイヤーのベクトル
			XMVECTOR wallNormal = dataStreat.normal;					//当たった壁の法線ベクトル

			playerDire = XMVector3Normalize(playerDire);				//正規化
			wallNormal = XMVector3Normalize(wallNormal);				//正規化

			float dot = XMVector3Dot(playerDire, wallNormal).vecX;		//内積を求める
			float angle = acos(dot);									//acosを求めれば角度
			diffAngle_ = XMConvertToDegrees(angle);						//ラジアンを角度に変更


			rotateMax_ = 4.5f;											//一回で調整できる最大角度
			rotateMin_ = 0.5f;											//一回で調整できる最小角度
																		//調整したい角度が90度以内の場合
			if (diffAngle_ < UNDER_RECTANGULAR)
			{
				//一回の処理で調整したい角度を求める
				fitGroundAngle_ = UNDER_RECTANGULAR - diffAngle_;
				diffAngle_ = (OVER_RECTANGULAR - diffAngle_) / splitAngle_;
			}
			//それ以外
			else
			{
				//一回の処理で調整したい角度を求める
				fitGroundAngle_ = OVER_RECTANGULAR - diffAngle_;
				diffAngle_ = (UNDER_RECTANGULAR - diffAngle_) / splitAngle_;
			}

			//最大角度よりも大きい場合
			if (diffAngle_ > rotateMax_)
				diffAngle_ = rotateMax_;		//最大角度値を設定
												//最小角度よりも小さい場合
			else if (diffAngle_ < rotateMin_)
				diffAngle_ = rotateMin_;		//最小角度値を設定

												//回転フラグを更新
			*flg = false;
		}
		//それ以外
		else
		{
			//移動フラグを更新
			hitDownFlag_ = -1;
		}


		//レイの発射地点から地面までの距離が9以上の場合
		if (dis <= 9)
		{
			//回転フラグを更新
			disGroundFlag_ = true;
		}
		//それ以外
		else
		{
			//回転フラグを更新
			disGroundFlag_ = false;
		}

	}



	////レイ(正面・真下)が地面に触れていないとき
	//if (flg)
	//{
	//	//プレイヤーを上・下に向かせる(遊泳)関数
	//	BlockingPlayerSwim();
	//}


}


//移動できる範囲での移動処理を行う関数
void Player::MoveRest(XMVECTOR moveVec)
{
	//水面に当たったとき
	if (transform_.position_.vecY >= upRest_)
	{
		//キー入力での移動分をプレイヤーに適用
		transform_.rotate_.vecX += adMove_ * 14.f;
		transform_.position_.vecY -= adMove_;
		transform_.position_.vecZ += moveVec.vecZ * 5.5f;
		restFlag_ = 1;
	}

	//正面に当たったとき
	if (transform_.position_.vecZ >= streatRest_)
	{
		//キー入力での移動分をプレイヤーに適用
		transform_.position_.vecZ -= adMove_;
		restFlag_ = 2;
	}

	//後ろに当たったとき
	if (transform_.position_.vecZ <= backRest_)
	{
		//キー入力での移動分をプレイヤーに適用
		transform_.position_.vecZ += adMove_;
		restFlag_ = 3;
	}

	//右に当たったとき
	if (transform_.position_.vecX >= rightRest_)
	{
		//キー入力での移動分をプレイヤーに適用
		transform_.position_.vecX -= adMove_;
		restFlag_ = 4;
	}

	//左に当たったとき
	if (transform_.position_.vecX <= leftRest_)
	{
		//キー入力での移動分をプレイヤーに適用
		transform_.position_.vecX += adMove_;
		restFlag_ = 5;
	}

	if (restFlag_ == 0)
	{
		transform_.position_ += moveVec;
	}
}
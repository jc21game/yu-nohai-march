#pragma once
#include "Behavior.h"


//上昇に関するクラス
class Rise : public Behavior
{
	float MAX_ROTATE_;
public:

	Rise(GameObject* pObject);

	~Rise();

	bool ShouldExecute() override;

	void Execute() override;
};


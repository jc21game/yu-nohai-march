//インクルード
#include "Ground.h"
#include "Engine/Model.h"
#include "Player.h"

//コンストラクタ
Ground::Ground(GameObject * parent)
	:GameObject(parent, "Ground"),
	worldModel_(-1)
{

}

//デストラクタ
Ground::~Ground()
{
}

//初期化
void Ground::Initialize()
{
	//地面のモデルデータをロード
	for (int z = 0; z < SPLIT_COUNT; z++)
	{
		for (int x = 0; x < SPLIT_COUNT; x++)
		{
			//fbxファイル名
			char path[_MAX_FNAME];
			wsprintf(path, "3DModel/TestGround/Ground%dFull_.fbx", (x + z * SPLIT_COUNT));

			//モデルデータのロード
			hModel_[z][x] = Model::Load(path);
			assert(hModel_[z][x] >= 0);
		}
	}

	std::string fileName = "3DModel/TestGround/GroundWorld8.fbx";


	worldModel_ = Model::Load(fileName);
	assert(worldModel_ >= 0);

	//プレイヤー情報を保持
	pPlayer_ = (Player*)FindObject("Player");



	//地面の座標を登録
		//地面の表示位置指定
	XMVECTOR savePos = XMVectorSet(0,0,0,1);
	//trans.scale_ = XMVectorSet(SPLIT_SIZE / 2, SPLIT_SIZE / 2, SPLIT_SIZE / 2, 1.0f);
	//trans.position_.vecY = -100;
	//trans.rotate_.vecX = -90.0f;
	savePos.vecY = SPLIT_POS_Y;

	//地面を表示するかどうか（Y座標は加味しない）
	for (int z = 0; z < SPLIT_COUNT; z++)
	{
		for (int x = 0; x < SPLIT_COUNT; x++)
		{
			pos_[z][x] = XMVectorSet(savePos.vecX ,
				savePos.vecY , savePos.vecZ , 1);

			savePos.vecX += SPLIT_SIZE;
		}

		savePos.vecX = 0.0f;
		savePos.vecZ += SPLIT_SIZE;
	}

	//微調整
		//モデルの関係で少し調整
	
	//GROUND_０
	pos_[0][0].vecY += 10;
	//GROUND_1
	pos_[0][1].vecY += 20;
	//GROUND_２
	pos_[0][2].vecY += 20;
	//GROUND_3
	pos_[0][3].vecY += 20;
	//GROUND_4
	pos_[0][4].vecY += 20;
	//GROUND_5
	pos_[0][5].vecY += 10;
	//GROUND_6
	pos_[0][6].vecY += 20;
	//GROUND_7
	pos_[0][7].vecY += 20;
	//GROUND_8
	pos_[0][8].vecY += 10;

}

//更新
void Ground::Update()
{
}

//描画
void Ground::Draw()
{
	//地面の座標を登録
	//地面の表示位置指定
	XMVECTOR scale;
	scale = XMVectorSet(SPLIT_SIZE / 2, SPLIT_SIZE / 2, SPLIT_SIZE / 2, 1.0f);
	//trans.position_.vecY = -100;
	//trans.rotate_.vecX = -90.0f;


	//地面を表示するかどうか（Y座標は加味しない）
	for (int z = 0; z < SPLIT_COUNT; z++)
	{
		for (int x = 0; x < SPLIT_COUNT; x++)
		{
			Transform trans;
			trans.position_ = pos_[z][x];
			trans.scale_ = scale;

			//表示
				Model::SetTransform(hModel_[z][x], trans);
				Model::Draw(hModel_[z][x]);
		}


	}

	////地面を表示するかどうか（Y座標は加味しない）
	//for (int z = 0; z < SPLIT_COUNT; z++)
	//{
	//	for (int x = 0; x < SPLIT_COUNT; x++)
	//	{
	//		//地面とプレイヤーの距離を計算
	//		//float disX, disZ;
	//		//差を求める
	//		//disX = trans.position_.vecX - pPlayer_->GetPosition().vecX;
	//		//disX = fabsf(trans.position_.vecX - pPlayer_->GetPosition().vecX);
	//		//disZ = fabsf(trans.position_.vecZ - pPlayer_->GetPosition().vecZ);



	//		//地面とプレイヤーは近いか
	//			//ｘの差2乗＋ｚの差2乗の平方根
	//		//if (sqrt(disX * disX + disZ * disZ) <= DISPLAY_RANGE * DISPLAY_RANGE)
	//		//if ((disX * disX + disZ * disZ) <= DISPLAY_RANGE * DISPLAY_RANGE)
	//		{
	//			//表示
	//			Model::SetTransform(hModel_[z][x], trans);
	//			Model::Draw(hModel_[z][x]);
	//		}

	//		trans.position_.vecX += SPLIT_SIZE;
	//	}

	//	trans.position_.vecX = 0.0f;
	//	trans.position_.vecZ += SPLIT_SIZE;
	//}


	Transform worldTrans;
	worldTrans.position_.vecY = SPLIT_POS_Y;
	worldTrans.scale_ = XMVectorSet(5000, 5000, 5000, 0);
	//worldTrans.rotate_.vecX -= 90;



	Model::SetTransform(worldModel_ , worldTrans);
	Model::Draw(worldModel_);


}

//開放
void Ground::Release()
{
}

int Ground::GetOneModelHundle(int groundNumber)
{
	//モデル番号配列に登録
	//行
		// 0 / 3 = 0行目
		// 0 / 3 = 1行目
		// 0 / 8 = 2行目
	int z = groundNumber / SPLIT_DATE_COUNT;

	//列
		// 0 % 3 = 0列目
		// 1 % 3 = 1列目
		// 2 % 3 = 2列目
		// 8 % 3 = 2列目
	int x = groundNumber % SPLIT_DATE_COUNT;

	//モデル番号を返す
	return hModel_[z][x];
}

//現在未使用
int Ground::LoadGround(int groundNumber)
{
	//ファイルパス(名)を入れる文字配列
	char path[_MAX_FNAME];

	std::string rootPath = "3DModel/Ground";

	//ファイルパスを通す
	wsprintf(path, "3DModel/TestGround/Ground%dFull_.fbx", groundNumber);


	//モデル番号配列に登録
	//行
		// 0 / 3 = 0行目
		// 0 / 3 = 1行目
		// 0 / 8 = 2行目
	int z = groundNumber / SPLIT_DATE_COUNT;

	//列
		// 0 % 3 = 0列目
		// 1 % 3 = 1列目
		// 2 % 3 = 2列目
		// 8 % 3 = 2列目
	int x = groundNumber % SPLIT_DATE_COUNT;

	//モデルデータのロード
	hDateModel_[z][x] = Model::Load(path);
	assert(hDateModel_[z][x] >= 0);


	//地面の表示位置指定
		//すぐにTransform値をセットするのは、
		//地面生成をしてすぐに、オブジェクトを生成させて、地面に沿わせる作業を行う。
		//その時に、地面のTransform値が、描画の位置になっていないと、衝突していない判定になる。
	Transform trans;
	trans.scale_ = XMVectorSet(SPLIT_SIZE, SPLIT_SIZE, SPLIT_SIZE, 1.0f);
	trans.rotate_.vecX = -90.0f;
	trans.position_.vecY = SPLIT_POS_Y;
	//座標値指定
	trans.position_.vecX += SPLIT_SIZE * x;
	trans.position_.vecZ += SPLIT_SIZE * z;

	//Transform値をセット
	Model::SetTransform(hModel_[z][x], trans);






	//モデルハンドルを返す
	return hDateModel_[x][z];
}

XMVECTOR Ground::GetGroundPos(int groundNumber)
{
	
	//行
	// 0 / 3 = 0行目
	// 0 / 3 = 1行目
	// 0 / 8 = 2行目
	int z = groundNumber / SPLIT_COUNT;

	//列
		// 0 % 3 = 0列目
		// 1 % 3 = 1列目
		// 2 % 3 = 2列目
		// 8 % 3 = 2列目
	int x = groundNumber % SPLIT_COUNT;


	//座標を変えす
	return pos_[z][x];
}

//インクルード
#include "PlayerCamera.h"
#include "Engine/Input.h"


//設定集
namespace PlayerCamera
{
	//ウィンドウ
	HWND hWnd_;						//ウィンドウハンドル

	//操作
	bool operate_ = KEY;			//カメラの操作
	float distance_ = 1.0f;			//マウスホイールの移動量を保持
	Transform transform_Key_;		//カメラ自身の回転を記憶するための変数
	Transform transform_Mouse_;		//カメラ自身の回転を記憶するための変数

	//カメラ
	bool viewer_ = FPS;				//カメラの視点
	float angle_ = 45.0f;			//カメラの座標限界
	XMVECTOR forFPSPos_ = XMVectorSet(0, 9.0f, 0, 0);						//FPS視点の位置
	XMVECTOR forFPSTar_ = XMVectorSet(0, 9.0f, 5.0f, 0);					//FPS視点の焦点
	XMVECTOR forFPSandTPSUp_ = XMVectorSet(0, 1.0f, 0, 0);					//カメラの上方向(両カメラ共通)

	//倍率
	float magnification = 2.0f;
}


//ウィンドウハンドル設定
void PlayerCamera::SetWindowHandle(HWND hWnd)
{
	hWnd_ = hWnd;
}

//プレイヤーのカメラに関する動作関数
void PlayerCamera::SetPlayerCamera(Player* player)
{
	//カメラに関するメンバ
	XMVECTOR forTPSPos_ = XMVectorSet(0, 25.0f, -50.0f, 0) * distance_;		//TPS視点の位置
	XMVECTOR forTPSTar_ = XMVectorSet(0, 15.0f, 0, 0) * distance_;			//TPS視点の焦点
	XMVECTOR camPos;														//カメラの位置
	XMVECTOR camTar;														//カメラの焦点
	XMVECTOR camUp;															//カメラの上方向

	//視点切り替え
	//if (Input::IsKeyDown(DIK_F5))
	//{
	//	//視点切り替え
	//	viewer_ = !viewer_;

	//	//入力をキーボードに切り替え
	//	operate_ = KEY;

	//	//視点がFPSに切り替えられたとき
	//	if (viewer_ == FPS)
	//	{
	//		//マウス表示
	//		int i = ShowCursor(true);

	//		//カーソルの表示カウントがおかしいとき
	//		if (i > 0)
	//			ShowCursor(false);		//カーソルの表示カウント調整
	//	
	//		//カメラの方向修正
	//		transform_Key_.rotate_.vecX = 0;
	//		transform_Key_.rotate_.vecY = 0;
	//		transform_Mouse_.rotate_ = XMVectorSet(0, 0, 0, 0);
	//	}
	//}

	/**
	須田信行
	ズームインズームアウト
	*/
	magnification += (Input::IsKey(DIK_UP) - Input::IsKey(DIK_DOWN));
	if (magnification > 4.0f)magnification = 4.0f;
	if (magnification < -30.0f)magnification = -30.0f;

	//視点決め
	switch (viewer_)
	{
	case FPS:
		camPos = forFPSPos_;		//カメラの位置
		camPos.vecZ = magnification;
		camTar = forFPSTar_;		//カメラの焦点
		camUp = forFPSandTPSUp_;	//カメラの上方向
		break;

	case TPS:
		camPos = forTPSPos_;		//カメラの位置
		camTar = forTPSTar_;		//カメラの焦点
		camUp = forFPSandTPSUp_;	//カメラの上方向
		break;
	}

	//TPS視点の時
	if (viewer_ == TPS)
	{
		camPos = XMVector3TransformCoord(camPos, RotateOwn(player));			//位置ベクトルをrotateCamMatXY_行列で回転
		camTar = XMVector3TransformCoord(camTar, RotateOwn(player));			//焦点ベクトルをrotateCamMatXY_行列で回転
		camUp = XMVector3TransformCoord(camUp, RotateOwn(player));				//上方向ベクトルをrotateCamMatXY_行列で回転
	}

	//カメラベクトルをプレイヤーの向きによって回転
	camPos = XMVector3TransformCoord(camPos, player->GetRotMatX());		//位置ベクトルをrotateMatX_行列で回転
	camPos = XMVector3TransformCoord(camPos, player->GetRotMatY());		//位置ベクトルをrotateMatY_行列で回転
	camTar = XMVector3TransformCoord(camTar, player->GetRotMatX());		//焦点ベクトルをrotateMatX_行列で回転
	camTar = XMVector3TransformCoord(camTar, player->GetRotMatY());		//焦点ベクトルをrotateMatY_行列で回転
	camUp = XMVector3TransformCoord(camUp, player->GetRotMatX());		//上方向ベクトルをrotateMatX_行列で回転
	camUp = XMVector3TransformCoord(camUp, player->GetRotMatY());		//上方向ベクトルをrotateMatY_行列で回転

	//マウスホイールでズームイン・ズームアウト
	float willDist = distance_ + Input::GetWheelMove().vecZ * -0.001f;
	if (willDist < 2.0f && willDist > 0.5f)
		distance_ = willDist;

	//カメラの位置をセット
	Camera::SetPosition(player->GetPosition() + camPos);

	//カメラの焦点をセット
	Camera::SetTarget(player->GetPosition() + camTar);

	//カメラの上方向をセット
	Camera::SetUp(camUp);
}

//カメラ自身に関する動作関数
XMMATRIX PlayerCamera::RotateOwn(Player* player)
{
	XMMATRIX rotateCamMatX_;				//X軸の回転角度を記憶するための変数
	XMMATRIX rotateCamMatY_;				//Y軸の回転角度を記憶するための変数
	XMMATRIX rotateCamMatXY_;				//XY軸の回転角度の合成行列

	//カメラ操作の切り替え
	if (Input::IsKeyDown(DIK_F6))
	{
		//キーボード・マウス切り替え
		operate_ = !operate_;

		//キーボード操作
		if (operate_ == KEY)
		{
			//切り替え時の位置を修正
			transform_Key_.rotate_ = transform_Mouse_.rotate_;

			//マウス表示
			ShowCursor(true);
		}

		//マウス操作
		if (operate_ == MOUSE)
		{
			//切り替え時の位置を修正
			transform_Mouse_.rotate_ = transform_Key_.rotate_;

			//マウス非表示
			ShowCursor(false);
		}
	}

	//マウス操作の時にはマウスの位置固定
	if (operate_ == MOUSE)
	{
		//ウィンドウのサイズ取得
		RECT size;
		GetWindowRect(hWnd_, &size);

		//マウスポインタの位置
		POINT pt;
		pt.x = size.left + (size.right - size.left) / 2;
		pt.y = size.top + (size.bottom - size.top) / 2;
		SetCursorPos(pt.x, pt.y);
	}

	//キー操作の時
	if (operate_ == KEY)
	{
		//キー入力
		if (Input::IsKey(DIK_RIGHT))		//→キー入力でY軸の値を加算
			transform_Key_.rotate_.vecY += 0.5f;
		if (Input::IsKey(DIK_LEFT))			//←キー入力でY軸の値を減算
			transform_Key_.rotate_.vecY -= 0.5f;

		//キー入力(75度まで上下回転できる)
		if (Input::IsKey(DIK_UP) && transform_Key_.rotate_.vecX <= angle_)			//↑キー入力でX軸の値を加算
			transform_Key_.rotate_.vecX += 0.5f;
		if (Input::IsKey(DIK_DOWN) && transform_Key_.rotate_.vecX >= -angle_)		//↓キー入力でX軸の値を減算
			transform_Key_.rotate_.vecX -= 0.5f;
	}
	
	//マウス操作の時
	if (operate_ == MOUSE)
	{
		//マウスの移動量
		float moveMousePosX = transform_Mouse_.rotate_.vecX + Input::GetMouseMove().vecX * player->GetMouseSensitivity();
		float moveMousePosZ = transform_Mouse_.rotate_.vecX + Input::GetMouseMove().vecX * player->GetMouseSensitivity();

		/*マウスで移動*/
		//X成分は角度指定を満たしていたら移動
		if (moveMousePosX <= angle_ && moveMousePosX >= -angle_)
			transform_Mouse_.rotate_.vecX = moveMousePosX;		//移動量加算
		
		//Z成分は角度指定を満たしていたら移動
		if (moveMousePosZ <= angle_ && moveMousePosZ >= -angle_)
			transform_Mouse_.rotate_.vecZ = moveMousePosZ;		//移動量加算
	}

	//エスケープキー入力でカメラのみ初期化
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		transform_Key_.rotate_.vecX = 0;
		transform_Key_.rotate_.vecY = 0;
		transform_Mouse_.rotate_ = XMVectorSet(0, 0, 0, 0);
		distance_ = 1.0f;
	}

	//キー操作の時
	if (operate_ == KEY)
	{
		//カメラ自体の回転を行列に変換
		rotateCamMatY_ = XMMatrixRotationY(XMConvertToRadians(transform_Key_.rotate_.vecY));		//Y軸の回転
		rotateCamMatX_ = XMMatrixRotationX(XMConvertToRadians(transform_Key_.rotate_.vecX));		//X軸の回転
	}

	//マウス操作の時
	if (operate_ == MOUSE)
	{
		//カメラ自体の回転を行列に変換
		rotateCamMatY_ = XMMatrixRotationY(XMConvertToRadians(transform_Mouse_.rotate_.vecY));		//Y軸の回転
		rotateCamMatX_ = XMMatrixRotationX(XMConvertToRadians(transform_Mouse_.rotate_.vecX));		//X軸の回転
	}

	//Y軸の回転行列とX軸の回転行列を合成
	rotateCamMatXY_ = rotateCamMatX_ * rotateCamMatY_;

	//合成した回転行列を返す
	return rotateCamMatXY_;
}

//現在マウスでカメラを操作するかどうか
bool PlayerCamera::GetOperate()
{
	return operate_;
}
#pragma once
#include "MetaBase.h"


//ウツボの自動生成クラス
//ウツボに関しては、自分のタイミングで生成、配置を行わなければいけない。
//ウツボだけは特別で、
	//途中で消えたりする管理の仕方はできない。



//ウツボの作成数
#define IS_CREATE_ 3

//レイ判定を行わなくて済む最高距離
#define MAX_DIST 100.0f

//更新フレーム
#define UPDATE_FRAME_ 30


//更新を開始する距離
#define UPDATE_RANGE_ 500

//描画を開始する距離
#define DRAW_RANGE_ 700


class MorayEel;

class MorayEElOperateMeta :
	public MetaBase
{
	enum TYPE
	{
		UPDATE_ = 0,
		DRAW_,

		TYPE_END

	};

	//シーンのオブジェクトポインタ
	GameObject* pSceneObject_;

	//プレイヤーオブジェクトポインタ（地面のオブジェクトは１つで、そのオブジェクト内に複数の地面モデルを取得する）
	GameObject* pPlayer_;

	//フレームカウンター
	int frameCounter_;


	//ウツボモデルのモデル番号群
	int hModel_[IS_CREATE_];

	//ウツボを生成する地面モデル番号
		//ウツボを生成し、設置する地面の番号を持って置き、生成が支持されたときに、その地面に生成する
		//地面によっては、生成できないような地面もあるので、こちらから静的に指定
	int createPlate[IS_CREATE_];
		//生成位置は地面プレートの中ではランダムで、ある

	//ウツボの表示位置
	XMVECTOR dispPos[IS_CREATE_];

	//ウツボオブジェクトのポインタを入れておく配列
	MorayEel* pMorayEel_[IS_CREATE_];



	//ウツボのロード制御を行う
	//毎フレームウツボモデルのまだロードを完了していないモデルのロードを行う（条件付き）
	//void UpdateLoadMorayEel();




	//符号をランダムで取得
	int GetRandomCood();

	//地面に沿わせる
		//地面とレイキャストを行い、地面に沿わせる
	//引数：ウツボモデルの番号（自身のクラスが所有しているウツボの何番目のデータか）
	//引数：ウツボモデルのオブジェクトポインタ
	//引数：地面番号にて示されるモデルのハンドル
	void FitGround(int morayEelNum , GameObject* pMorayEel ,int hGroundModel);

	//プレイヤーの一番近いウツボを更新
		//プレイヤーと衝突判定を行うウツボを判定
	void ChangeRayCastModel();


	//void DrawExe();

	void UpdateAndDrawExe();

	//引数：モデル番号（添え字）
	//引数：範囲内距離
	void InRange(int suffix, int range , TYPE type);

public:
	//コンストラクタ
	//引数：なし
	//戻値：なし
	MorayEElOperateMeta();
	//デストラクタ
	//引数：なし
	//戻値：なし
	~MorayEElOperateMeta();

	// MetaBase を介して継承されました

	// MetaBase を介して継承されました
	//初期化（地面オブジェクトの生成）
	//引数：シーンのオブジェクトポインタ
	//引数：プレイヤーのオブジェクトポインタ（自身の指揮メタAIでは、プレイヤーオブジェクトは使用しないが、ベースクラスにキャストした際に一度に呼べるように関数形式はどのクラスでも共通した形にした。）
	//戻値：なし
	void Initialize(GameObject * pSceneObject, GameObject * pPlayer) override;

	//更新
	//引数：管理下のオブジェクトリスト（毎フレーム更新として何かの指揮をする場合は、このリストに、そのオブジェクト群を送ってもらう（その際、リストへの追加は、地面オブジェクトがメタAIにリストへの追加を呼び込む（魚指揮メタAIと同じ）呼び方を行う））
			//指揮メタAIから、メタAIのリストに追加する形は、理想ではない。
	//戻値：なし
	void Update(std::list<GameObject*>& lookObject) override;

	



	//解放
	//引数：なし
	//戻値：なし
	void Release() override;



	//ウツボのロード
		//引数にて、
		//地面モデルがロードされたときの、地面モデル番号をもらう。
		//引数にてもらった地面番号（0オリジン）と、ウツボが生成される地面番号（1オリジン）とを比較して、
		//同じ地面番号を持っていたら、ウツボを生成させる。
			//この時点で、地面は生成されているはずなので、レイキャストで地面に沿わせて生成されることは可能。
	//引数：地面番号（0オリジン）
	//引数：地面番号にて示されるモデルのハンドル
	void LoadMorayEel(int groundNumber, int hGroundModel);


};


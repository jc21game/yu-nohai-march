//インクルード
#include "Animation.h"
#include "FishAssets.h"
#include "Engine/Model.h"

//指定したモデル(魚)のアニメーションを行う関数
void Animation::SwimFish(int modelHandle, AnimationSet& anime)
{
	int nowFrame = Model::GetAnimFrame(modelHandle);

	//アニメーションフレームを状態によって変化させる
	switch (anime.style)
	{
	case STOP:
		//アニメーションリミット関数
		LimitAnimation(nowFrame);

		//フレームが中央フレームを上回っている場合場合
		if (nowFrame > CENTER_FRAME)
			nowFrame -= (nowFrame - LEFT_LIMIT) / QUARTER_FRAME;		//中央フレームへ近づける

		//フレームが中央フレームを下回っている場合場合
		if (nowFrame < CENTER_FRAME)
			nowFrame += (RIGHT_LIMIT - nowFrame) / QUARTER_FRAME;		//中央フレームへ近づける
		break;

	case ADVANCE:
		//全フレームが左回転だった場合
		if (anime.preStyle == LEFT)
		{
			//1フレーム分マイナス方向へアニメーションを行い、もし0フレームを下回っていたら
			if (--nowFrame < 0)
				nowFrame = FRAME;		//フレームを最大へ置き換える
		}

		//全フレームが右回転だった場合
		if (anime.preStyle == RIGHT)
		{
			//1フレーム分プラス方向へアニメーションを行い、もし最大フレームを上回っていたら
			if (++nowFrame > FRAME)
				nowFrame = 0;			//フレームを0へ置き換える
		}
		break;

	case LEFT_TRUN:
		//アニメーションリミット関数
		LimitAnimation(nowFrame);

		//フレームを左端フレームへ近づける
		nowFrame -= nowFrame / LEFT_LIMIT;

		//フレームが左端フレームを下回っている場合
		if (nowFrame < LEFT_LIMIT)
			nowFrame = LEFT_LIMIT;		//フレームを左端フレームに置き換える

		//前状態を左回転にする
		anime.preStyle = anime.style;
		break;

	case RIGHT_TRUN:
		//アニメーションリミット関数
		LimitAnimation(nowFrame);

		//フレームを右端フレームへ近づける
		nowFrame += (FRAME - nowFrame) / LEFT_LIMIT;

		//フレームが右端フレームを上回っている場合
		if (nowFrame > RIGHT_LIMIT)
			nowFrame = RIGHT_LIMIT;		//フレームを右端フレームに置き換える

		//前状態を右回転にする
		anime.preStyle = anime.style;
		break;
	}

	//アニメーション再生
	Model::SetAnimFrame(modelHandle, nowFrame, nowFrame, 0);
}

//アニメーションリミット(制限)
void Animation::LimitAnimation(int& nowFrame)
{
	//フレームが右端フレームを上回っている場合
	if (nowFrame > RIGHT_LIMIT)
		nowFrame = RIGHT_LIMIT - (nowFrame - RIGHT_LIMIT);		//フレームをモデルの位置に合わせる

	//フレームが左端フレームを下回っている場合
	if (nowFrame < LEFT_LIMIT)
		nowFrame = CENTER_FRAME - nowFrame;						//フレームをモデルの位置に合わせる
}
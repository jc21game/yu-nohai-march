//インクルード
#pragma once
#include "Behavior.h"


//動作:動かない
class NoMove : public Behavior
{
public:

	//コンストラクタ
	//params : ゲームオブジェクトポインタ
	//return : NULL
	NoMove(GameObject* pGameObject);

	//デストラクタ
	//params : NULL
	//return : NULL
	~NoMove();

	//実行可能か
	//params : NULL
	//return : 実行可能ならばTrue
	bool ShouldExecute() override;

	//実行
	//params : NULL
	//return : NULL
	void Execute() override;
};
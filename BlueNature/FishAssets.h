#pragma once

//ディファイン
#define FRAME				86		//アニメーションの全フレーム数
#define CENTER_FRAME		45		//待機フレーム
#define QUARTER_FRAME		22		//全フレームの1/4
#define RIGHT_LIMIT			22		//右端フレーム
#define LEFT_LIMIT			67		//左端フレーム

//enum
//プレイヤーの状態
enum STYLE
{
	STOP = 0,		//停止
	ADVANCE,		//前進
	LEFT,			//左回転
	RIGHT,			//右回転
};

//プレイヤー以外の魚の状態(プレイヤーの状態を一部上書きする形)
enum ADD_STYLE
{
	//STOP = 0,			//停止
	//ADVANCE,			//前進
	LEFT_TRUN = 2,		//左旋回
	RIGHT_TRUN,			//右旋回
};

//魚の上下
enum UP_DOWN
{
	NORMAL = 0,			//維持
	UP,					//上を向く
	DOWN				//下を向く
};


//struct
//モーション用設定集
struct AnimationSet
{
	int style;			//モデルの状態
	int preStyle;		//モデルの一個前の状態

	//初期化
	AnimationSet() :style(STOP), preStyle(LEFT) {}
};

﻿//インクルード
#include "Timer.h"
#include <chrono>

//ライブラリリンカ
#pragma comment(lib, "winmm.lib")


namespace Timer 
{
	
	static auto deltaTime = std::chrono::microseconds(0);	//デルタタイム
	static auto	elapsedTime = std::chrono::microseconds(0);	//経過時間
	static float microDiv = 1000000.f;	//マイクロ(10の6乗)

	//シーンがスタートしてからの経過時間(sec)
	float	GetElapsedSecounds() 
	{
		return elapsedTime.count() / microDiv;	//経過時間をマイクロ秒から秒に変換してから返す
	}

	//フレームのデルタタイム(sec)
	float	GetDelta() 
	{
		return deltaTime.count() / microDiv;	//デルタタイムをマイクロ秒から秒に変換してから返す
	}

	//シーンタイマーのリセット
	void	Reset()	
	{
		elapsedTime = std::chrono::microseconds(0);
		deltaTime = std::chrono::microseconds(0);
	}

	//デルタタイムの更新
	void	UpdateFrameDelta()
	{	
		static auto prevTime = std::chrono::system_clock::now();	// 前回のマイクロ秒

		auto now = std::chrono::system_clock::now();				// 現在のマイクロ秒
		
		deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(now - prevTime);	//現在と前回の差

		prevTime = now;	//現在の時間を保持する

		elapsedTime += deltaTime;	//デルタタイムを加算
	}
};

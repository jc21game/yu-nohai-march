#include "SphereCollider.h"
#include "BoxCollider.h"
#include "OrientedBoxCollider.h"
#include "Model.h"
#include "GameObject.h"

//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
SphereCollider::SphereCollider(XMVECTOR center, float radius)
{
	center_ = center;
	size_ = XMVectorSet(radius, radius, radius, 0);
	

	//XMVECTORからXMFLOAT3に変換
	XMStoreFloat3(&colliderCenter_, center);

	//指定された値でコライダーを生成
	pCollider_ = std::make_unique<BoundingSphere>(colliderCenter_, radius);

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	hDebugModel_ = Model::Load("DebugCollision/sphereCollider.fbx");
#endif
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool SphereCollider::IsHit(Collider* target)
{
	//相手オブジェクトのコライダーもSphereCollider
	if (auto* targetSphere = dynamic_cast<SphereCollider*>(target))
		return IsHitSame<SphereCollider>(this, targetSphere);

	//相手オブジェクトのコライダーはBoxSphere
	if (auto* targetBox = dynamic_cast<BoxCollider*>(target))
		return IsHitWrong<SphereCollider, BoxCollider>(this, targetBox);

	if (auto* targetOriBox = dynamic_cast<OrientedBoxCollider*>(target))
		return IsHitWrong<SphereCollider, OrientedBoxCollider>(this, targetOriBox);

	return false;
}

void SphereCollider::UpdateState()
{
	XMVECTOR pos = center_ + pGameObject_->GetPosition();
	XMStoreFloat3(&pCollider_->Center, pos);
}

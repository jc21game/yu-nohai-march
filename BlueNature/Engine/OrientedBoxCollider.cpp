#include "OrientedBoxCollider.h"
#include "BoxCollider.h"
#include "SphereCollider.h"

#include "GameObject.h"

//コンストラクタ
OrientedBoxCollider::OrientedBoxCollider(XMVECTOR basePos, XMVECTOR size, XMVECTOR rotate)
{
	center_ = basePos;
	size_ = size;
	orient_ = XMVectorSet(rotate.vecX, rotate.vecY, rotate.vecZ, 1.0f);

	XMStoreFloat3(&colliderCenter_, basePos);
	XMStoreFloat3(&colliderSize_, size);
	XMStoreFloat4(&colliderOrient_, rotate);

	pCollider_ = std::make_unique<BoundingOrientedBox>(colliderCenter_, colliderSize_, colliderOrient_);
}

//衝突判定
bool OrientedBoxCollider::IsHit(Collider * target)
{
	if (auto* targetOriBox = dynamic_cast<OrientedBoxCollider*>(target))
		return IsHitSame<OrientedBoxCollider>(this, targetOriBox);

	if (auto* targetSphere = dynamic_cast<SphereCollider*>(target))
		return IsHitWrong<OrientedBoxCollider, SphereCollider>(this, targetSphere);

	if (auto* targetBox = dynamic_cast<BoxCollider*>(target))
		return IsHitWrong<OrientedBoxCollider, BoxCollider>(this, targetBox);

	return false;
}

//ステータス更新
void OrientedBoxCollider::UpdateState()
{
	XMVECTOR pos = center_ + pGameObject_->GetPosition();
	XMStoreFloat3(&pCollider_->Center, pos);
	
	XMVECTOR rotate = pGameObject_->GetRotate();
	rotate = XMVectorSet(rotate.vecX, rotate.vecY, rotate.vecZ, 1.0f);
	XMStoreFloat4(&pCollider_->Orientation, rotate);

	
}


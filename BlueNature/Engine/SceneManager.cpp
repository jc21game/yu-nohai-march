/*
	SceneManagerの仕事。
	・シーンの機能を持つクラス（シーン内の各オブジェクトの出現などなど）を呼び込み
	・シーンの機能を持つクラスの切り替え
	・切り替えが行われた時の前シーンないの全ての子供（シーン内の全てのオブジェクト）を消去
	・前モデルデータの消去
	（ロードによって、登録したモデルデータの開放→一度ロードしたモデルは、2度ロードしないように、
		モデルのデータベースに登録している→それの解放？）
		
*/




#include "sceneManager.h"
/*
	シーンとして登録する、クラスのヘッダのインクルード
*/
//スプラッシュシーン（ゲームの開始のシーン）
#include "../SplashScene.h"
//タイトルシーン
//#include "../TitleScene.h"
//プレイシーン
//#include "../PlayScene.h"
#include "../GameScene.h"
//クレジットシーン
#include "../CreditScene.h"


#include "Timer.h"	//シーンの経過時間・デルタタイムを管理するクラス
#include "Model.h"
#include "Image.h"
#include "Audio.h"


//コンストラクタ
SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager")
		//GameObjectクラスのコンストラクタに、
		//StageManagerのクラスに読み込んだ、GameObjectのクラス内のポインタへコンストラクタに情報を与える
		//→コンストラクタに渡された、GameObjectのポインタを、別のGameObject（インクルードしたGameObjectで作成するGameObjectのコンストラクタ？）
		//→引数でもらったGameObjectのポインタと、自身のクラスの名前を送り、親と、オブジェクトの名前を設定
{
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	//初期に表示するシーンのenumの値を設定する
	currentSceneID_ = SCENE_ID_SPLASH;	//現在のシーンを、指定のenumの値（シーンID）にて登録
	nextSceneID_ = currentSceneID_;		//初期のシーンに置いては、まだ何も表示されていないところに、シーンを読み込むことになるので、
																	//→Updateにて、if判断を抜けない用に、nextIDも、初期のシーンIDにて更新する
	Instantiate<SplashScene>(this);				//初期シーンの初期化（SceneManagerと、初期シーンの親子づけ（SceneManagerを親に））
	////親子付によって、シーンのクラスのInitializeを呼ぶことになり、そのシーンの初期化が始まる。
	//currentSceneID_ = SCENE_ID_GAME;	//現在のシーンを、指定のenumの値（シーンID）にて登録
	//nextSceneID_ = currentSceneID_;		//初期のシーンに置いては、まだ何も表示されていないところに、シーンを読み込むことになるので、
	//																//→Updateにて、if判断を抜けない用に、nextIDも、初期のシーンIDにて更新する
	//Instantiate<GameScene>(this);				//初期シーンの初期化（SceneManagerと、初期シーンの親子づけ（SceneManagerを親に））
						//親子付によって、シーンのクラスのInitializeを呼ぶことになり、そのシーンの初期化が始まる。
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
		//シーンIDの切り替えは、シーン切り替えを行うタイミング（シーンから、各オブジェクトのクラスからでも、）
		//任意のタイミングで、SceneManagerクラスのChangeSceneの関数から切り替え先のシーンを選択してもらう
	if (currentSceneID_ != nextSceneID_)
	{
		/*******現在のシーンの消去*************************************************************************/
		//現在表示中のシーン
		//そのシーンのオブジェクトを全削除（シーン下の、子供になっているオブジェクトの全消去）
			//＝　子供の子供がいれば、それも続けて消去するようにしなくてはいけない。
			//＝　GameObjectのクラスにて、そのオブジェクトに子供がいれば、さらに子供がいるかの確認で、一番下の子供を消去してからそのおや、その親と、シーンに存在するオブジェクトを完全に消去する
		KillAllChildren();

		//ロードしたデータを全削除
			//Model、Image、Audio内には、それぞれモデルデータなどのLoadを使用して、それぞれのFBXクラスや、 Spriteクラスなどを
			//それらの情報を、クラス内のDB（可変長配列の構造体）として、保存している→これによって、Modelで同じファイルを Loadした時に、
			//すでに、DBには、そのファイルの情報が登録されているので、2度ロードは、せずに、登録されたFBXのクラスを、使用するということ工夫がされている
		//それらのDBのデータを消去
		Model::AllRelease();
		Image::AllRelease();
	/*******現在のシーンの消去*************************************************************************/

	/*******次のシーンの登録*************************************************************************/
				//次のシーンを作成
			//nextIDにて、設定されたシーンを作成＝SceneManagerの子供として、シーンを親子付。と共に、シーン側のInitializeが、（次のフレーム？）に呼ばれる。
			//実際にInitializeが呼ばれるタイミングは、GameObjectクラスで、各オブジェクトのInitializeを呼ぶタイミングをどのように設定してイルカにもよる
		switch (nextSceneID_)
		{
		//シーンをSceneManageerに登録した場合、
		//そのシーンIDと、そのシーンIDが呼ばれた時のシーンの親子づけの処理を以下に書かなければいけない
			//→でないと、シーンの切り替えができない
			
		//テストシーン
		case SCENE_ID_SPLASH: Instantiate<SplashScene>(this); break;
		//case SCENE_ID_TITLE: Instantiate<TitleScene>(this); break;
		//case SCENE_ID_PLAY: Instantiate<PlayScene>(this); break;
		case SCENE_ID_GAME: Instantiate<GameScene>(this); break;
		case SCENE_ID_CREDIT: Instantiate<CreditScene>(this); break;

		}

		//シーン登録完了
		//現在のシーンを、切り替え後のシーンIDに
		currentSceneID_ = nextSceneID_;
			//この段階で、currentとnextには、同じ値＝次フレームにて、シーン切り替えが行われない
			//nextが切り替えられるまで、切り替えのいf文は、行われない

		//タイマーリセット
		Timer::Reset();
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
	//SceneManagerを読み込んだクラスで、シーンを切り替えたいタイミングで呼び出したいシーンを引数として以下の関数を呼ぶ
	//SceneのIDは、ヘッダにて、登録したenumの値である。
void SceneManager::ChangeScene(SCENE_ID next)
{
	//引数のIDを次のシーンのIDとする
		//＝次のフレームでupdateが呼ばれる際に、currentとnextが違うので、シーン切り替えが行える
	nextSceneID_ = next;	
}
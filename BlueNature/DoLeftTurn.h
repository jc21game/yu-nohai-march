//インクルード
#pragma once
#include "Behavior.h"


//動作:左旋回
class DoLeftTurn : public Behavior
{
public:

	//コンストラクタ
	//params : ゲームオブジェクトポインタ
	//return : NULL
	DoLeftTurn(GameObject* pGameObject);

	//デストラクタ
	//params : NULL
	//return : NULL
	~DoLeftTurn();

	//実行可能か
	//params : NULL
	//return : 実行可能ならばTrue
	bool ShouldExecute() override;

	//実行
	//params : NULL
	//return : NULL
	void Execute() override;
};
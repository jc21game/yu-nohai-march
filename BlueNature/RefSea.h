#pragma once
#include "Engine/Texture.h"

namespace RefSea
{
	void Initialize(int hModel);

	Texture* GetTexture();

	void Release();

	int GetTime();
}
/*
	include の横にコメントを付けると
	C4067の警告が出るので注意

*/

#include "GameMetaAI.h"
#include "Engine/Global.h"
//CSV読み取り専用
#include "Engine/CsvReader.h"	

#include "Player.h"
//指揮メタAI　継承元クラス
#include "MetaBase.h"			

//各指揮メタAI
//魚キャラクタ指揮メタAI
#include "FishOperateMeta.h"	
//地面キャラクタ指揮メタAI
#include "GroundOperateMeta.h"	
//海背景色指揮メタAI
#include "SeaOperateMeta.h"




//名前空間の実装部
//各メタAIの総括を行うクラス（各オブジェクトの所有、各メタAIの更新呼び出しなど）
namespace GameMetaAI
{
	/*extern宣言した変数の初期化*/
	//地面数
		//地面指揮メタAIが所有する地面数から取得
	const int GROUND_COUNT_ = GROUND_MAX;



	//プレイヤーオブジェクト
	GameObject* pPlayer_;
	//メタAI適用シーン
	GameObject* pSceneObject_;



	//各指揮メタAIにて監視、指揮をするオブジェクト群（指揮メタAI指揮下のオブジェクト群）
//名前空間なので、各オブジェクト宣言時に追加。各オブジェクト消去時に解放。を指揮してもらう。
	//各指揮メタAIごとに、必要なオブジェクトの型が違う（GameObject型→Baseクラスなどなど。各指揮メタAIが指揮をする際に、GameObject型のままでは困るので、継承した形を使用したい。）
/*
・各指揮メタAI下で管理できるようにオブジェクトの型を変形させる
	具体的に

	リストにはGameObject型で登録し、
		関数受け取り側で、引数キャスト(GameObject型で送ってもらい、自身関数でFishBaseなどでキャスト)　OR


*/
//第一要素：各指揮メタAIのタイプ（enumの値）
//第二要素：第一要素にて使用するオブジェクト配列
	std::map<MetaType, std::list<GameObject*>> metaGroup_;


	//CSVファイル読み取りクラス
	//オブジェクトの情報を所有するCSVファイルの取得（ファイル分クラスを取得）
	//→各オブジェクトは、下記から該当情報を取得する（時にコストや優先度）
	//CSVReaderをファイルごとに取得	
	CsvReader* csvReader_[CSV_MAX];


	//各指揮メタAIを所有しておく配列
	//MetaBase* meta[META_MAX];
	//ポインタのポインタにて、ポインタにてMetaBaseを複数取得することを宣言し、
	//さらにポインタにて、配列要素をポインタにて確保（ポインタを入れる枠をポインタで取得する）
	MetaBase** meta_;





	//オブジェクトをリストに追加(Add)
	//引数：自身を指揮するメタAIのタイプ
	//引数：リストに追加するオブジェクトポインタ
	//戻値：なし
	void AddListObject(MetaType metaType, GameObject* pAddObject);

	//オブジェクトをリストから解放(Remove)
	//引数：自身を指揮するメタAIのタイプ
	//引数：リストに追加するオブジェクトポインタ
	//戻値：なし
	void RemoveListObject(MetaType metaType, GameObject* pRemoveObject);



	//オブジェクト生成時にコストの加算(Add)
		//オブジェクトを生成したときに、そのオブジェクトのコストを、各指揮メタAIの所有するコストに加算
		//AddMetaAIMember時点で、引数にコストをもらうこともできるが、
		//必ずしも、オブジェクトがコストを持っているとは限らない（コストを使用しない、（コストによって管理しない）オブジェクトである可能性もある。）
	//引数：自身を指揮していたメタAIのタイプ（自身のタイプを所有していたオブジェクトのポインタ）
	//引数：自身のフィールド上でのコスト（このコストをもとに、オブジェクトの生成、消去を管理する）
	//戻値：なし
	void AddObjectCost(MetaType metaType, int addCost);


	//オブジェクト出現のカウント（詳しくは以下）（Add)
	//オブジェクトごとに出現したことを記録（オブジェクトの出現にランダム性を上げるために、１体出現したら、別に取得しているカウンターに
		//別のコスト値を取得して起き、出現オブジェクトの選定の時などに、使用して（そのカウンターを加算して）、すでに出現しているオブジェクトは出現しにくくなる）
	//引数：自身を指揮していたメタAIのタイプ（自身のタイプを所有していたオブジェクトのポインタ）
	//引数：自身のフィールド上でのコスト（このコストをもとに、オブジェクトの生成、消去を管理する）
	//引数：タイプ（int）(enum値)
	//戻値：なし
	void AddObjectCounter(MetaType metaType, int addCost, int type);

	//オブジェクト生成時にコストの減算(Subtraction)
	//引数：自身を指揮していたメタAIのタイプ（自身のタイプを所有していたオブジェクトのポインタ）
	//引数：自身のフィールド上でのコスト（このコストをもとに、オブジェクトの生成、消去を管理する）
	//戻値：なし
	void SubObjectCost(MetaType metaType, int subCost);

	//オブジェクト出現の減算（詳しくは以下）（Sub)
	//引数：自身を指揮していたメタAIのタイプ（自身のタイプを所有していたオブジェクトのポインタ）
	//引数：自身のフィールド上でのコスト（このコストをもとに、オブジェクトの生成、消去を管理する）
	//引数：タイプ（int）(enum値)
	void SubObjectCounter(MetaType metaType, int subCost, int type);





};

/*関数*/

void GameMetaAI::Initialize(GameObject* pSceneObject, GameObject* pPlayer)
{
	//引数の登録
	pSceneObject_ = pSceneObject;
	pPlayer_ = pPlayer;

	//各メタAIのInitializeを呼ぶ前に
	//オブジェクトを入れておく、リストを宣言（Initialize先でオブジェクトを生成するので）

	//指揮メタAIのタイプとそのメタAIにて使用するオブジェクト軍のリストの初期化
	//map要素の初期化
	//★META_MAX分（各指揮メタAI数分）
	//第一要素の登録と、それに伴う第二要素の登録
	//第二要素のリストの初期化
	//初期化なので、確保した要素分初期化を行いたい
	for (int i = 0; i < META_MAX; i++)
	{
		//新しいリストの生成
		std::list<GameObject*> newList;
		//リストの初期化（クリア）
		newList.clear();
		//mapの第二要素へ登録
			//Typeを０オリジンから連番でMAXまで取得しているので、
			//０から取得した整数値をMetaTypeというenum型にキャストして、iをenumの値として登録
		metaGroup_[(MetaType)i] = newList;
	}
	/*
	//新しいリストの生成
	std::list<GameObject*> newList;
	//リストの初期化（クリア）
	newList.clear();
	//mapの第二要素へ登録
	metaGroup_[FISH_OPERATE_META] = newList;
	*/


	//CSVファイルの読み込み
	{
		//CSVファイルのファイル名宣言（CSVReaderに読み取らせるファイル名（enumにて登録した順番でファイル名確保））
		std::string csvFileName[CSV_MAX]
		{
			"Csv/Fish_Information.csv",
			"Csv/Ground_Environment.csv",
			"Csv/FishCost_GroundENV.csv",
			"Csv/FishHeight_GroundENV.csv",


		};

		//CSVファイルの要素の動的確保
			//とともに、ファイルのロード
		for (int i = 0; i < CSV_MAX; i++)
		{
			//動的確保
			csvReader_[i] = new CsvReader;
			//ファイルのロード(確保しておいたファイル名を引数としてロードを行う)
			csvReader_[i]->Load(csvFileName[i]);

		}
	}



	{
		//指揮メタAIのインスタンスを動的確保
			//MetaBaseをポインタで複数個動的確保
		//★META_MAX分（各指揮メタAI数分）
		meta_ = new MetaBase*[META_MAX];
		//初期化
		for (int i = 0; i < META_MAX; i++)
		{
			meta_[i] = nullptr; 
		}


		{
			//各指揮メタAIの確保、とともに、MetaBase型の配列に登録（キャストして）
			//〜（Initialize関数内にて、宣言し、登録する。）
			//順番（他オブジェクトと情報を共有するために）
			//�@地面指揮メタAI
			//�A魚キャラクタ指揮AI

			meta_[GROUND_OPERATE_META] = new GroundOperateMeta();	//地面キャラクタ指揮メタAI(MetaBase*　にキャスト)
			meta_[FISH_OPERATE_META] = new FishOperateMeta();	//魚キャラクタ指揮メタAI(MetaBase*　にキャスト)

			meta_[SEA_OPERATE_META] = new SeaOperateMeta();		//海の背景色指揮メタAI
		}


		for (int i = 0; i < META_MAX; i++) { meta_[i]->Initialize(pSceneObject_, pPlayer_); }
	}




}

//毎フレーム
//各指揮メタAIのUpdateを呼ぶ
void GameMetaAI::Update()
{
	//各指揮メタAIを指定
	//★META_MAX分（各指揮メタAI数分）
	for (int i = 0; i < META_MAX; i++)
	{
		//mapの第一要素にて、指揮メタAIのタイプを指定して、
		//第二要素の、タイプに該当するオブジェクト群（リスト）をもらう
		//そのリストを実体ではなく、参照渡しでアドレスのみ渡す（正しくは、受け取り側で＆で受け取る）（実体を送ると多少なりとも重くなってしまうため、参照渡しのアドレスだけを送るようにする（引数受け取り側で、変更したくない場合は、constをつける））



		meta_[i]->Update(metaGroup_[(MetaType)i]);


	}

}

//動的確保順とは逆に解放（最後に確保したものから解放）
void GameMetaAI::Release()
{
	//１．各指揮メタAIの解放
	//２．オブジェクトの全解放（シーン終了時（シーン内完結のメタAIのため、シーンが終了した時点で終了させる）→SceneのReleaseにて、自身のRelease関数を呼ぶようにすれば、解放がされる。）

	//CSV読み取りクラスの解放
	for (int i = 0; i < CSV_MAX; i++)
	{
		SAFE_DELETE(csvReader_[i]);
	}

	//指揮メタAIの解放（ポインタのポインタの解放）
	//★META_MAX分（各指揮メタAI数分）
	for (int i = 0; i < META_MAX; i++)
	{
		//指揮メタAIのReleaseを呼ぶ
		meta_[i]->Release();


		//指揮メタAIの解放
			//配列の要素内のポインタを確保（配列に動的確保されたポインタの中身をさらに動的確保した）
		SAFE_DELETE(meta_[i]);
	}
	//中身を解放したので、配列の箱も解放（配列の要素を動的確保しているので、解放が必要）
	SAFE_DELETE_ARRAY(meta_);

	//各指揮メタAIにて使用していた、指揮管理下のオブジェクトのリストの解放
	//リストの全開放
	AllReleaseMember();
}

void GameMetaAI::AllReleaseMember()
{
	//イテレータにて、リストを回し、リスト内の要素を全消去
	//mapの第一要素を一つ一つまわし、全第一要素を網羅して、第二要素のリストを解放
	//★META_MAX分（各指揮メタAI数分）
	for (int i = 0; i < META_MAX; i++)
	{
		//全要素のリストからの削除
		//std::list<GameObject*>::iterator itr = metaGroup_[(MetaType)i].begin();
		//イテレータにてリストの頭から、終わりまで回す
		for (auto itr = metaGroup_[(MetaType)i].begin();
			itr != metaGroup_[(MetaType)i].end(); itr++)
		{
			//イテレータにて示された、リスト内の一つ一つの要素
			//要素のリストからの解放（ポインタの解放は、オブジェクト自身のKillMeから行うようにする。）

			//現在見ているリストから指定されているイテレータ（リストの要素を示すポインタのようなもの）で示された要素の解放
			metaGroup_[(MetaType)i].erase(itr);


		}
		//リストの
		//クリア
		metaGroup_[(MetaType)i].clear();

	}
}


//オブジェクトを追加する関数
//オブジェクトのコストを追加する関数とで分けていたが、
	//オブジェクト自身がこの関数を呼びのに、呼び忘れてしまわないように、呼ぶ関数は１つに
void GameMetaAI::AddMetaAIMember(MetaType metaType,
	GameObject * pAddObject, int addCost, int type)
{
	//オブジェクトをリストに追加
	AddListObject(metaType, pAddObject);
	//コストの加算
	AddObjectCost(metaType, addCost);

	//オブジェクト別、コストのカウント
	//オブジェクトごとに出現したことを記録（オブジェクトの出現にランダム性を上げるために、１体出現したら、別に取得しているカウンターに
		//別のコスト値を取得して起き、出現オブジェクトの選定の時などに、使用して（そのカウンターを加算して）、すでに出現しているオブジェクトは出現しにくくなる）
	AddObjectCounter(metaType, addCost, type);



}


void GameMetaAI::RemoveMetaAIMember(MetaType metaType,
	GameObject * pRemoveObject, int subCost, int type)
{
	//オブジェクトをリストに削除
	RemoveListObject(metaType, pRemoveObject);
	//コストの減算
	SubObjectCost(metaType, subCost);

	//オブジェクト別、コストのカウント減算
	SubObjectCounter(metaType, subCost, type);


}

//オブジェクトの情報を取得
//オブジェクト名、コスト
int GameMetaAI::GetObjectInfoCsv(MetaCsvType metaCsvType,
	FISH_TYPE fishTypeRow)
{
	//引数のCSVファイルタイプを指定し、
	//専用のCSVReader_を参照する。（CSVReaderには、それぞれ、一つのCSVファイルがロードされているので、それを指定する）

	//CSVのセルから値を取得するためには、
		//左上のセルを（０，０）としたときに、
		//第一引数：（列）左から●番目、	第二引数：（行）上から●番目
		//第一引数：１〜ほしい情報まで回す	第二引数：fishType（オブジェクトを識別する番号として、行を指定する。）

	//例
	//MetaCsvType：Fish_Information.csv(魚のコスト、情報)
	//第一引数：（列）１					第二引数：（行）BUTTER_FLY_FISH
	//第一引数：（列）コスト				第二引数：（行）チョウチョウウオ
	return csvReader_[(int)metaCsvType]->GetValue(1, fishTypeRow);


}

//優先度を返す
std::string GameMetaAI::GetObjectPriorityCsv(
	MetaCsvType metaCsvType,
	FISH_TYPE fishTypeColumn, int row)
{
	//第一引数：（列）左から●番目、		第二引数：（行）上から●番目
	//第一引数：（列）fishTypeColumn		第二引数：（行）row
	//第一引数：（列）オブジェクトタイプ	第二引数：（行）優先度を決めるオブジェクトのタイプ、番号

	return csvReader_[(int)metaCsvType]
		->GetString(fishTypeColumn, row);;
}

//引数にて指定された地面プレート番号を指定し、その座標を返してもらう
XMVECTOR GameMetaAI::GetGroundPos(int groundPlateNumber)
{
	//（０行目　：情報の目次）
	//（１行目〜：地面ごとの座標値）

	//XYZ座標の３つの座標をCSVファイルから読み取る
	XMVECTOR groundPos;
	//(左から何番目、上から何番目)
	//引数指定の地面番号のX座標値を取得
	groundPos.vecX = (float)(csvReader_[GROUND_ENVIRONMENT]->GetValue(1 , groundPlateNumber + 1));
	//Y座標値を取得
	groundPos.vecY = (float)(csvReader_[GROUND_ENVIRONMENT]->GetValue(2, groundPlateNumber + 1));
	//Z座標値を取得
	groundPos.vecZ = (float)(csvReader_[GROUND_ENVIRONMENT]->GetValue(3, groundPlateNumber + 1));
	
	//4次元目を0にする
		//Lengthの関数にて長さを求めるときに、４次元目に値が入っていると、3次元における距離として正しい値が出てこない可能性がある。
		//そのため初期化
	groundPos = XMVectorSet(groundPos.vecX, groundPos.vecY, groundPos.vecZ, 0);


	return groundPos;
}

//地面指揮メタAIから地面のFbxモデル番号を取得
int GameMetaAI::GetGroundModelHundle(int groundPlateNumber)
{
	//メンバ関数にアクセスするためにキャスト（地面指揮メタAIにキャスト）
	GroundOperateMeta* groundMeta
		= (GroundOperateMeta*)meta_[GROUND_OPERATE_META];

	//地面指揮メタAIから、地面のモデルハンドル（番号）の取得
		//1オリジンの番号にする
	return groundMeta
		->GetModelHundle((GROUND_PLATE_HUNDLE)(groundPlateNumber + 1));

}

//魚オブジェクトごとのその地面における、出現高さの受け取り（出現位置の高さを０〜１の割合で受け取り、段階によって、出現位置を変更させる。）
float GameMetaAI::GetObjectHeight(int groundPlateNumber, FISH_TYPE objectHundle)
{
	//CSV
	//・FishHeight_GroundENV.csv
	//１行目：CSVのヘルプ
	//２行目：０列目（地面番号）、１列目（魚１の地面に対する高さ（０〜１の割合））、２列目（魚２の地面に対する高さ（０〜１の割合））

	//左から何番目（魚オブジェクト番目）::引数値は１オリジン、CSVも１オリジン（そのままCSVへの引数として使用可能）
	//上から何番目（地面オブジェクト番目）::引数値は０オリジン、CSVは１オリジン（＋１をして引数として使用可能）
	return csvReader_[FISH_HEIGHT_GROUND_ENV]
		->GetValueFloat(objectHundle, groundPlateNumber + 1);
}

int GameMetaAI::GetWorldGroundHundle()
{
	//メンバ関数にアクセスするためにキャスト（地面指揮メタAIにキャスト）
	GroundOperateMeta* groundMeta
		= (GroundOperateMeta*)meta_[GROUND_OPERATE_META];

	//地面指揮メタAIから、地面のモデルハンドル（番号）の取得
		//1オリジンの番号にする
	return groundMeta
		->GetWorldGroundHundle();
}

//引数にてもらった
//mapに登録した、MetaTypeのenum値を第一要素とし、第二要素のリストに引数のオブジェクトのポインタを登録する
void GameMetaAI::AddListObject(MetaType metaType, GameObject * pAddObject)
{
	//追加
	//配列のように[]内に、mapの第一要素の値を入れることで
	//戻値として、登録したときの第二要素が返ってくる
	//今回の第二要素は、GameObject型のポインタを代入するリスト
	//上記のリストにポインタを追加
	metaGroup_[metaType].push_back(pAddObject);
}
//引数の要素を解放する（要素から指定する）
void GameMetaAI::RemoveListObject(MetaType metaType, GameObject * pRemoveObject)
{
	//消去
	//リストから消去するだけ
	//ポインタ自身の解放は、オブジェクト自身のKillMeにて行うことなので、ここでは行わない。
	//指定した要素を消去（Remove）
	metaGroup_[metaType].remove(pRemoveObject);
}

void GameMetaAI::AddObjectCost(MetaType metaType, int addCost)
{
	//引数のMetaTypeの値により指揮メタAIを判別し
		//指揮メタAIが所有しているコストを足す関数にコストを代入し加算させる（この関数はvirtualで、何も中身が書かれていない関数→そのため、継承先で変なオーバーライドをしなければ、呼び込むが何もしないという処理になる。）
	meta_[metaType]->AddCost(addCost);

}

void GameMetaAI::AddObjectCounter(MetaType metaType, int addCost, int type)
{
	meta_[metaType]->AddObjectCounter(type, addCost);
}

void GameMetaAI::SubObjectCost(MetaType metaType, int subCost)
{
	//指揮メタAIにコストを減算
	meta_[metaType]->SubCost(subCost);
}

void GameMetaAI::SubObjectCounter(MetaType metaType, int subCost, int type)
{
	meta_[metaType]->SubObjectCounter(type, subCost);
}


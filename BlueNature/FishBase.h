#pragma once
#include <vector>
#include <string>
#include <memory>
#include <DirectXCollision.h>

#include "Engine/GameObject.h"
#include "Engine/Model.h"
#include "CharacterAI.h"
#include "Engine/Timer.h"
#include "Animation.h"


//NPC(魚)作成のベースとなるクラス
class FishBase : public GameObject
{
private:

	XMVECTOR objectPos_;
	XMVECTOR targetPos_;
	XMVECTOR playerDir_;
	float eyeRate_;


	GameObject* pPlayer_;
	float degrees_;

	int animFrame_;


protected:


	//魚の各ステータス
	struct FishStatus
	{
		//コンストラクタ
		FishStatus() {};

		float moveSpeed_;			//移動速度

		XMVECTOR viewDir_;			//視線
		float verticalAngle_;

		float rotateSpeedX_;		//回転速度(縦)
		float rotateSpeedY_;		//回転速度(横)


		/*****衝突判定・認識*****/
		//身体の当たり判定
		XMVECTOR bodyCenter_;		//中心位置
		XMVECTOR bodySize_;			//サイズ

		//左目
		XMVECTOR leftEyeCenter_;	//中心位置
		XMVECTOR leftEyeRange_;		//認識範囲

		//右目
		XMVECTOR rightEyeCenter_;	//中心位置
		XMVECTOR rightEyeRange_;	//認識範囲

		float congnitionTime_;
		float maxCongnitionTime_;

		float visuallyTime_;
		float maxVisuallyTime_;

		bool isCong_ = false;		//認識フラグ
		bool isVis_ = false;		//目視版の認識フラグ
		bool isRightEye_ = false;	//右目で認識しているのか
		bool isLeftEye_ = false;	//左目で認識しているのか
	
		/*bool isCalcOfDir_ = false;*/
		
	};
	std::unique_ptr<FishStatus> pStatus_;

	XMVECTOR front_;

	int MAX_ANIM_FRAME_;


	//自身を指揮するメタAIからの指示を入れておく
	struct MetaInfo
	{
		int thisCost_;//自身のオブジェクトのコスト

		//コンストラクタにて初期化
		MetaInfo():thisCost_(10)//初期値で１０を代入
		{
		}

	}*metaInfo;//変数の宣言


	std::unique_ptr<CharacterAI> pCharAI_;	//AI機能

	int modelHandle_;	//モデル番号

	//モデルロード
	//params : モデルが格納されているファイルのパス
	//return : NULL
	void LoadModel(const std::string& filePath);

	//コストを構造体のMetaInfoの要素に登録する
	//継承先にて、それぞれのオブジェクトごとのコストを設定するときに使用する
	//params：自身のオブジェクトのコスト
	//return：なし
	void SetThisCost(int thisCost) { metaInfo->thisCost_ = thisCost; };

	//自身のコストを取得
	//構造体の変数にアクセスし、登録された値を取得
	//params：NULL
	//return：自身のオブジェクトのコスト
	int GetThisCost() { return metaInfo->thisCost_; };

	//認識したならば、指定時間フラグを立てる
	void DidCongnition();

	void DidVisually();

	//プレイヤーの距離・方向を更新
	//params : NULL
	//return : NULL
	void UpdatePlayerDir();

	//どのくらい縦に回転しているか、角度を求める
	//params : NULL
	//return : NULL
	void CalcVerticalAngle();

	//アニメーション再生
	//params : NULL
	//return : NULL
	void PlayAnimation();


public:
	//コンストラクタ
	//params : 親オブジェクト、名前
	//return : NULL
	FishBase(GameObject* parent, const std::string& name);

	//デストラクタ
	//params : NULL
	//return : NULL
	~FishBase();

	//初期化
	//params : NULL
	//return : NULL
	virtual void Initialize() override;

	//更新
	//params : NULL
	//return : NULL
	virtual void Update() override;

	//描画
	//params : NULL
	//return : NULL
	virtual void Draw() override;

	//解放
	//params : NULL
	//return : NULL
	virtual void Release() override;


	//当たり判定
	//params : 当たった対象オブジェクト
	//return : NULL
	virtual void OnCollision(GameObject* target) override {};

	//認識時間を更新
	//params : NULL
	//return : NULL
	void UpdateCongnitionTime();

	//認識時間を更新
	//params : NULL
	//return : NULL
	void UpdateVisuallyTime();

	//認識しているか
	//params : NULL
	//return : 認識しているならばTrue
	bool IsCongnition();

	//どれだけ縦回転しているかの角度を取得
	//params : NULL
	//return : 角度(度)
	float GetVerticalAngle();

	//設定された移動速度を取得
	//params : NULL
	//return : デフォルトの移動速度
	float GetMoveSpeed();

	//回転速度(縦)を取得
	//params : NULL
	//return : 回転速度(縦)
	float GetRotateSpeedX();

	//回転速度(横)を取得
	//params : NULL
	//return : 回転速度(横)
	float GetRotateSpeedY();

	//プレイヤーの方向を取得
	//params : NULL
	//return : プレイヤーの方向ベクトル
	XMVECTOR GetPlayerDir();

	//近づきたい相手を目視した場合に、認識フラグを変更する処理
	//params : 目視したい対象オブジェクト
	//return : NULL
	virtual void Look(GameObject* target) {};

	//目視しているか
	//params : NULL
	//return : 目視しているならばTrue
	bool IsVisually();


	//目視の対象が範囲内にいるかどうかをチェックする処理
	//params : 目視したい対象オブジェクト、自分の位置、右目の範囲、左目の範囲
	//return : 範囲内にいたらTrue
	void InRange(GameObject* own, GameObject* target, XMVECTOR rightRange, XMVECTOR leftRange);

	bool GetIsRightEye();

	bool GetIsLeftEye();

};
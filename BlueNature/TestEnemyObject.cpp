//#include "TestEnemyObject.h"
//
////メタAI
//#include "GameMetaAI.h"
//
////コンストラクタ
//TestEnmeyObject::TestEnmeyObject(GameObject* parent)
//	: FishBase(parent, "TestEnemyObject")
//{
//
//
//	{
//		//★
//		//自身のオブジェクトのコストをセットする(正確に言うと更新、FishBaseにて、初期コストは設定されているので、更新を行わずともエラーにはならない)
//		//SetThisCost(10);
//
//		//CSVファイルよりコストを取得
//		//MetaCsvType：FISH_INFORMATION（魚情報CSVファイル）
//		//FishType	 ：BUTTER_FLY_FISH（チョウチョウウオ）
//		//戻値：10
//		int cost = GameMetaAI::GetObjectInfoCsv(FISH_INFORMATION,
//			TEST_ENEMY_OBJECT);
//		SetThisCost(cost);
//
//	}
//
//}
//
////デストラクタ
//TestEnmeyObject::~TestEnmeyObject()
//{
//
//}
//
////初期化
//void TestEnmeyObject::Initialize()
//{
//	//モデルロード
//	LoadModel("3DModel/TestModel/Enemy.fbx");
//
//	//初期位置(仮)
//	transform_.position_.vecZ -= 50;
//	//初期拡大（仮）
//	transform_.scale_ = XMVectorSet(20.0f, 20.0f, 20.0f, 0);
//
//
//	{
//		//★
//		//メタAIに自身を追加させる（名前空間）
//		//引数1：自身を指揮するメタAIの種類・タイプ（enum値）
//		//引数2：自身のオブジェクトポインタ (自身のオブジェクト型(GameObject継承必須（メタAIによって、特定のクラス継承前提）))
//		//引数3：自身のオブジェクトのコスト（int）
//		GameMetaAI::AddMetaAIMember(FISH_OPERATE_META,
//			this, GetThisCost() , (int)TEST_ENEMY_OBJECT);
//	}
//
//}
//
////更新
//void TestEnmeyObject::Update()
//{
//	////メタAIからの指揮のフラグがたっていたら
//	////Updateを行わない
//	//if (!metaInfo->conductor)
//	//{
//
//		transform_.position_.vecZ += 1.0f;
//	//}
//}
//
////解放
//void TestEnmeyObject::Release()
//{
//	
//	{
//		//★
//		//メタAIに自身を消去させる（名前空間）
//		//引数1：自身を指揮するメタAIの種類・タイプ（enum値）
//		//引数2：自身のオブジェクトポインタ
//		//引数3：自身のオブジェクトのコスト（int）
//		GameMetaAI::RemoveMetaAIMember(FISH_OPERATE_META,
//			this, GetThisCost() , (int)TEST_ENEMY_OBJECT);
//	}
//	
//}

//インクルード
#include "DoLeftTurn.h"
#include "FishBase.h"


//コンストラクタ
DoLeftTurn::DoLeftTurn(GameObject* pGameObject)
{
	pObject_ = pGameObject;
	pFish_ = dynamic_cast<FishBase*>(pObject_);
}

//デストラクタ
DoLeftTurn::~DoLeftTurn()
{
}

//実行可能か
bool DoLeftTurn::ShouldExecute()
{
	//タイミング指定なし
	return !pFish_->IsCongnition();
}

//実行
void DoLeftTurn::Execute()
{
	//左回転
	pFish_->SetRotateY(pFish_->GetRotate().vecY - pFish_->GetRotateSpeedX());
}

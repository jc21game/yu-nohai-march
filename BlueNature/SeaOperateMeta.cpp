#include "SeaOperateMeta.h"

//世界の
//背景色を変えるために３Dの根幹クラスをインクルード
#include "Engine/Direct3D.h"


SeaOperateMeta::SeaOperateMeta():
	pPlayer_(nullptr),
	NORMAL_COLOR_(XMVectorSet(0.1f, 0.5f, 0.8f, 1.0f)),
	frameCounter_(0),
	CALC_FRAME_(30),	//６０ｆｐｓのため、その半分
	posY_(180.0f),
	changeValue_(40.0f)
{
}

SeaOperateMeta::~SeaOperateMeta()
{
}

void SeaOperateMeta::Initialize(GameObject * pSceneObject, GameObject * pPlayer)
{
	pPlayer_ = pPlayer;

}

void SeaOperateMeta::Update(std::list<GameObject*>& lookObject)
{
	//フレームをカウントする
	frameCounter_++;

	if (frameCounter_ > CALC_FRAME_)
	{
		//背景色計算
		CalculationColor();
	
		//カウンターを初期化
		frameCounter_ = 0;
	}



}

void SeaOperateMeta::CalculationColor()
{

	//プレイヤーの現在値を調べ、
	//現在値から、背景色を変化させていく

	//海面近く（ｙ１８０ｆ）水色
	//海底近く（ｙ-３６０ｆ）濃い青
	//上記の間をグラデーション


	//濃い青
	float r = 0.0f / 255.0f;
	float g = 152.0f / 255.0f;
	float b = 255.0f / 255.0f;


	//上記の値を基準として、
	//上下４０の変化を行いたい
	float playVecY = pPlayer_->GetPosition().vecY;

	//プレイヤーの高さが０以上の時
	if (playVecY >= 0)
	{
		//背景色を明るくするために
		//Gの色値を上げる


		//プレイヤーのY座標と、Y座標の最高値とで割り算を行い、
		//プレイヤーの現在の位置を割合として出す
		//原点０を０として、Y座標の最高値を１として、プレイヤーの位置を０〜１の割合で求める
		float per = playVecY / posY_;

		g = (152.0f + (UP_VALUE_GREEN * per)) / 255.0f;

	}
	//プレイヤーの高さが０より小さいとき
	else
	{
		//背景色を暗くするために
		//Gの色値を下げる


		//０より小さい座標のY座標の最低値は、
		//Y座標の２倍になる
		float per = playVecY / posY_;
		//この場合プレイヤーの座標は
		//-360 ~ 0になる
		//つまりposY_は、180fのため、
		//per　は、0 ~ 2.0fになる

		//割合による計算に用いるため
		//０〜１．０ｆの値にする
		per = per / 2.0f;


		g = (152.0f + (DOWN_VALUE_GREEN * per)) / 255.0f;

	}



	Direct3D::SetClearColor(r, g, b, 1.0f);



	////グラデーションの条件式
	//if (pPlayer_->GetPosition().vecY >= 100.0f)
	//{
	//	Direct3D::SetClearColor(0.1f, 0.5f, 1.0f, 1.0f);
	//}
	//else if (pPlayer_->GetPosition().vecY <= 100.0f)
	//{
	//	Direct3D::SetClearColor(0.1f, 0.5f, 0.8f, 1.0f);
	//}
}

void SeaOperateMeta::Release()
{
}

#include "FishCsvInfo.h"

#include "Engine/Global.h"
#include "Engine/GameObject.h"


//生成オブジェクトの情報取得
//魚オブジェクト
#include "Bluefintrevally.h"	//カスミアジ
#include "Blueheadtilefish.h"	//アオマスク
#include "Bluetang.h"			//ナンヨウハギ
#include "ButterflyFish.h"		//チョウチョウウオ
#include "Crownfish.h"			//アネモネ
#include "Moorishidol.h"		//ツノダシ
#include "Ray.h"				//エイ
#include "Scolopsisbilineata.h"	//フタスジタマガシラ
#include "Spottedgardeneel.h"	//チンアナゴ
#include "WhaleShark.h"			//ジンベイザメ


#include "TestEnemyObject.h"	//テストオブジェクト
#include "TestBarObject.h"	//テストオブジェクト
#include "TestTankBodyObject.h"	//テストオブジェクト
#include "TestTankHeadObject.h"	//テストオブジェクト


//オブジェクトのタイプごとに、タイプの順番でオブジェクトを生成する関数のポインタを配列で取得し、
//メタAIなどからオブジェクトのタイプ別にオブジェクトを生成するとき、
	//→タイプを添え字として、配列を選択し、オブジェクトの生成する関数を変化させる。
//オブジェクト生成の一行を書かずに、関数のポインタ配列から指定して呼び込めればコードの削減できる

FishCsvInfo::FishCsvInfo(GameObject* pSceneObject) :
	pSceneObject_(pSceneObject)
{




	//ポインタのポインタとして
	//ポインタに動的確保のポインタを複数個確保(構造体を確保する数分、だけ、ポインタを確保する)
		//動的確保された配列領域に、さらに１つ１つの要素にポインタで動的確保
	instantiateFunc = new InstantiateFunc<GameObject>*[FISH_MAX];

	/*
	//★FISH_MAX分
	//関数ポインタを入れる構造体の動的確保
	//オブジェクト一つ一つ確保（別々の型を宣言し、それを一つ一つキャストのコードを書かないといけないので）
			//オブジェクトを生成する関数を関数ポインタに登録
		//enumによって登録されている、オブジェクト順に登録
	//★登録するときの型が、それぞれ違うので、for分で回せない
	//BUTTER_FLY_FISH
	{
		//オブジェクトクラス：：ButterflyFish

		//キャストして、インスタンスの動的確保
		instantiateFunc[0] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<ButterflyFish>);
		//関数ポインタを確保
		//GameObject型にて関数ポインタの配列を取得しているため、そのまま登録できないので、キャスト（キャストも関数のポインタの形にキャストする
			//（難しく考えず、GameObject*のインスタンスにキャストするときは(GameObject*)にキャストするように、関数のポインタの型が存在するので、その型に合わせて、キャスト））
		//関数のポインタ＝　(戻値型(ポインタに)(引数型))
		instantiateFunc[0]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<ButterflyFish>);




	}
	//TEST_ENEMY_OBJECT
	{
		//オブジェクトクラス：：TestEnemyObject

		//キャストして、インスタンスの動的確保
		instantiateFunc[1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<TestEnmeyObject>);
		//関数ポインタを確保
		//関数のポインタ＝　(戻値型(ポインタに)(引数型))
		instantiateFunc[1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<TestEnmeyObject>);

	}
	//TEST_BAR_OBJECT
	{
		//オブジェクトクラス：：TestBarObject

		//キャストして、インスタンスの動的確保
		instantiateFunc[2] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<TestBarObject>);
		//関数ポインタを確保
		//関数のポインタ＝　(戻値型(ポインタに)(引数型))
		instantiateFunc[2]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<TestBarObject>);

	}
	//TEST_TANKBODY_OBJECT
	{
		//オブジェクトクラス：：TestTankBodyObject

		//キャストして、インスタンスの動的確保
		instantiateFunc[3] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<TestTankBodyObject>);
		//関数ポインタを確保
		//関数のポインタ＝　(戻値型(ポインタに)(引数型))
		instantiateFunc[3]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<TestTankBodyObject>);

	}
	////TEST_TANKHEAD_OBJECT
	//{
	//	//オブジェクトクラス：：TestTankHeadObject

	//	//キャストして、インスタンスの動的確保
	//	instantiateFunc[4] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<TestTankHeadObject>);
	//	//関数ポインタを確保
	//	//関数のポインタ＝　(戻値型(ポインタに)(引数型))
	//	instantiateFunc[4]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<TestTankHeadObject>);

	//}
	//テスト　カスミアジ
	{
		//オブジェクトクラス：：TestTankHeadObject

		//キャストして、インスタンスの動的確保
		instantiateFunc[4] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Bluefintrevally>);
		//関数ポインタを確保
		//関数のポインタ＝　(戻値型(ポインタに)(引数型))
		instantiateFunc[4]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Bluefintrevally>);

	}
	*/







	//★クラス型が違うので、
	//関数化などができない、
	//一つ一つ、関数ポインタとして取得しなくてはいけない。

	//全部の準備が整い次第
	{
		//	BLUE_FIN_TREVALLY = 1,	//カスミアジ
//	BLUE_HEADTILE,			//アオマスク
//	BLUE_TANG,				//ナンヨウハギ
//	BUTTERFLY_FISH,			//チョウチョウウオ
//	CROWN_FISH,				//アネモネ
//	MOORISH_IDOL,			//ツノダシ
//	RAY,					//エイ
//	SCOLOPSIS_BILINEAT,		//フタスジタマガシラ
//	SPOTTED_GARDEN_ELL,		//チンアナゴ
//	WHALE_SHARK,			//ジンベイザメ



		//BLUE_FIN_TREVALLY
		{
			//オブジェクトクラス：：Bluefintrevally.h

			//テンプレート関数の関数ポインタを取得
			//キャストして、インスタンスの動的確保
			instantiateFunc[BLUE_FIN_TREVALLY - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Blurfintrevally>);
			//関数ポインタを確保
			//GameObject型にて関数ポインタの配列を取得しているため、そのまま登録できないので、キャスト（キャストも関数のポインタの形にキャストする
				//（難しく考えず、GameObject*のインスタンスにキャストするときは(GameObject*)にキャストするように、関数のポインタの型が存在するので、その型に合わせて、キャスト））
			//関数のポインタ＝　(戻値型(ポインタに)(引数型))
			instantiateFunc[BLUE_FIN_TREVALLY - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Blurfintrevally>);
		}
		//BLUE_HEADTILE
		{
			//オブジェクトクラス：：Blueheadtilefish.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[BLUE_HEADTILE - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Blueheadtilefish>);
			//関数ポインタを確保
			instantiateFunc[BLUE_HEADTILE - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Blueheadtilefish>);
		}
		//BLUE_TANG
		{
			//オブジェクトクラス：：Bluetang.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[BLUE_TANG - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Bluetang>);
			//関数ポインタを確保
			instantiateFunc[BLUE_TANG - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Bluetang>);
		}
		//BUTTERFLY_FISH
		{
			//オブジェクトクラス：：ButterflyFish.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[BUTTER_FLY_FISH - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<ButterflyFish>);
			//関数ポインタを確保
			instantiateFunc[BUTTER_FLY_FISH - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<ButterflyFish>);
		}
		//CROWN_FISH
		{
			//オブジェクトクラス：：Crownfish.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[CROWN_FISH - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Crownfish>);
			//関数ポインタを確保
			instantiateFunc[CROWN_FISH - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Crownfish>);
		}
		//MOORISH_IDOL
		{
			//オブジェクトクラス：：Moorishidol.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[MOORISH_IDOL - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Moorishidol>);
			//関数ポインタを確保
			instantiateFunc[MOORISH_IDOL - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Moorishidol>);
		}
		//RAY
		{
			//オブジェクトクラス：：Ray.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[RAY - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Ray>);
			//関数ポインタを確保
			instantiateFunc[RAY - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Ray>);
		}
		//SCOLOPSIS_BILINEAT
		{
			//オブジェクトクラス：：Scolopsisbilineat.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[SCOLOPSIS_BILINEAT - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Scolopsisbilineata>);
			//関数ポインタを確保
			instantiateFunc[SCOLOPSIS_BILINEAT - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Scolopsisbilineata>);
		}
		//SPOTTED_GARDEN_ELL
		{
			//オブジェクトクラス：：Spottedgardenell.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[SPOTTED_GARDEN_ELL - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<Spottedgardeneel>);
			//関数ポインタを確保
			instantiateFunc[SPOTTED_GARDEN_ELL - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<Spottedgardeneel>);
		}
		//WHALE_SHARK
		{
			//オブジェクトクラス：：WhaleShark.h

			//テンプレート関数の関数ポインタを取得
			instantiateFunc[WHALE_SHARK - 1] = (InstantiateFunc<GameObject>*)(new InstantiateFunc<WhaleShark>);
			//関数ポインタを確保
			instantiateFunc[WHALE_SHARK - 1]->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<WhaleShark>);
		}


	
	}







	//★成功（キャスト後でも、動作確認済み）
		//動作テスト
		//Instantiateの関数を関数ポインタで取得して生成できるか、
	{



		//関数ポインタを取得しておく構造体のオブジェクト個数分取得
		//InstantiateFunc<GameObject>* instantiateFunc[FISH_MAX];


		//構造体の要素確保
		{
			//★成功
			/*
			//完全に１つの型に共通すれば宣言可能
			//関数ポインタ所有の構造体生成
			InstantiateFunc<ButterflyFish>* instant1;
			//動的確保
			instant1 = new InstantiateFunc<ButterflyFish>;
			//関数ポインタにオブジェクト生成の関数のアドレスを代入（テンプレート型にオブジェクトの型）
			instant1->funcPointa = Instantiate<ButterflyFish>;
			//関数呼び出しとともにオブジェクトの取得
			ButterflyFish* bb = instant1->funcPointa(this);
			*/



			/*
			//★成功（きちんとオブジェクト生成できた）
			//キャストを使用することで、それぞれのオブジェクトの型を、すべてのオブジェクトが継承sるうGameObject型にする
				//そうすれば、配列として複数個取得可能
			InstantiateFunc<GameObject>* instant2;
			//一応キャストは可能
			instant2 = (InstantiateFunc<GameObject>*)(new InstantiateFunc<ButterflyFish>);
			//戻り値の型をGameObject*にして、キャスト
			instant2->funcPointa = (GameObject*(*)(GameObject*))(Instantiate<ButterflyFish>);
			ButterflyFish* bb = (ButterflyFish*)instant2->funcPointa(this);

			//★成功
			//動くかの確認
			//動きを止める
			bb->metaInfo->conductor = true;
			*/


			//delete instant1;
			//delete instant2;
		}
	}





}


FishCsvInfo::~FishCsvInfo()
{
	//動的確保した、構造体インスタンスの解放
	for (int i = 0; i < FISH_MAX; i++)
	{
		SAFE_DELETE(instantiateFunc[i]);
	}
	SAFE_DELETE_ARRAY(instantiateFunc);
}


GameObject * FishCsvInfo::CreateObject(FISH_TYPE fishType)
{
	//引数にて取得した、タイプから、
	//配列に登録されている、関数のポインタを選択、
	//関数のポインタから関数を呼び込み（Instantiateにてオブジェクトを生成する関数）
	//オブジェクトを生成する
	/*
		配列の登録＝０オリジン
		タイプのenum値の値＝１オリジン
	*/
	/*
		関数のポインタ＝Instantiate<オブジェクトの型>
		関数のポインタ呼び出し ＝ 関数のポインタ(pSceneObject_);
	*/

	//配列に登録された関数のポインタは
	//引数に手渡されているタイプの値とは１ずれている（ずれている理由はヘッダファイルのenum登録の部分を参照）
	return instantiateFunc[((int)fishType) - 1]->funcPointa(pSceneObject_);
}


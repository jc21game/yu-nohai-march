#include "CharacterAI.h"
#include <algorithm>
#include "FishBase.h"



//コンストラクタ
CharacterAI::CharacterAI()
	: waitChoiceTime_(0.f), CHOICE_TIME_(10.f)
{
	ChoiceBehavior();
}

//デストラクタ
CharacterAI::~CharacterAI()
{

}

//実行
void CharacterAI::Execute()
{	

	waitChoiceTime_ += Timer::GetElapsedSecounds();


	if (waitChoiceTime_ > CHOICE_TIME_)
	{
		choicedBehaviors_.clear();
		ChoiceBehavior();
		waitChoiceTime_ = 0;
		Timer::Reset();
		
	}

	//追加行動の実行
	for (auto&& data : addedBehaviors_)
	{
		auto& behave = data->behavior_;

		if (behave->ShouldExecute())
		{
			behave->Execute();
		}
	}

	//基本行動の実行
	for (auto itr = choicedBehaviors_.begin(); itr != choicedBehaviors_.end(); ++itr)
	{

		auto& behavior = defaultBehaviors_[*itr]->behavior_;

		if ((*itr) != NO_CHOICED_ && behavior->ShouldExecute())
		{
			behavior->Execute();
		}

	}



	
}

void CharacterAI::ChoiceBehavior()
{
	std::random_device engine;
	std::uniform_int_distribution<> dist(0, 1);
	choicedBehaviors_.emplace_back(WhetherToAdvance(dist(engine)));
	choicedBehaviors_.emplace_back(ChoiceLeftRight(dist(engine)));
	choicedBehaviors_.emplace_back(ChoiceRiseDescent(dist(engine)));
}

BehaviorPattern CharacterAI::WhetherToAdvance(int rand)
{
	return ADVANCE_;
	/*if (rand) 
	{
		return ADVANCE_;
	}
	else 
	{
		return NO_MOVE_;
	}*/
}

BehaviorPattern CharacterAI::ChoiceLeftRight(int rand)
{
	switch (rand) 
	{
	case 0:
		return LEFT_TURN_;

	case 1:
		return RIGHT_TURN_;

	default:
		return NO_CHOICED_;
	}
}

BehaviorPattern CharacterAI::ChoiceRiseDescent(int rand)
{
	switch (rand) 
	{
	case 0:
		return RISE_;

	case 1:
		return DESCENT_;

	default:
		return NO_CHOICED_;
	}
}

//行動追加
void CharacterAI::AddBehavior(int weight, std::unique_ptr<Behavior> behavior)
{
	auto params = std::make_unique<AddedBehaviorParams>(weight, behavior);
	addedBehaviors_.emplace_back(std::move(params));
	addedBehaviors_.sort([](auto&& x, auto&& y) { return x->weight_ > y->weight_; });
}

void CharacterAI::SetDefaultBehavior(BehaviorPattern pattern, std::unique_ptr<Behavior> behavior)
{
	auto params = std::make_unique<DefaultBehaviorParams>(pattern, behavior);
	defaultBehaviors_[pattern] = std::move(params);
}

#include "Descent.h"
#include "FishBase.h"

Descent::Descent(GameObject* pObject) 
	: MAX_ROTATE_(29.f)
{
	pObject_ = pObject;

	pFish_ = dynamic_cast<FishBase*>(pObject);

}

Descent::~Descent() 
{
}

bool Descent::ShouldExecute() 
{
	return pFish_->GetVerticalAngle() < MAX_ROTATE_ && !pFish_->IsCongnition();
}

void Descent::Execute() 
{
	pFish_->SetRotateX(pFish_->GetRotate().vecX + pFish_->GetRotateSpeedX());
}

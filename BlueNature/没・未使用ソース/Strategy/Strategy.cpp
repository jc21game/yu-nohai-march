//インクルード
#include "Strategy.h"
#include "FishAssets.h"

//timeフレーム待機
bool Stop::Action(GameObject& gb, int& time)
{
	//時間まで行動を続ける
	if(!--time)
		return false;

	//次の行動へ移る
	return true;
}

//timeフレーム前進
bool Adv::Action(GameObject& gb, int& time)
{
	//前進
	XMVECTOR move = XMVectorSet(0, 0, 0.1f, 0);
	move = XMVector3TransformCoord(move, XMMatrixRotationX(XMConvertToRadians(gb.GetRotate().vecX)));
	move = XMVector3TransformCoord(move, XMMatrixRotationY(XMConvertToRadians(gb.GetRotate().vecY)));
	gb.SetPosition(gb.GetPosition() + move);

	//時間まで行動を続ける
	if (!--time)
		return false;

	//次の行動へ移る
	return true;
}

//timeフレーム左旋回
bool LeftTurn::Action(GameObject & gb, int & time)
{
	//左回転
	gb.SetRotateY(gb.GetRotate().vecY - 1);

	//前進
	XMVECTOR move = XMVectorSet(0, 0, 0.1f, 0);
	move = XMVector3TransformCoord(move, XMMatrixRotationX(XMConvertToRadians(gb.GetRotate().vecX)));
	move = XMVector3TransformCoord(move, XMMatrixRotationY(XMConvertToRadians(gb.GetRotate().vecY)));
	gb.SetPosition(gb.GetPosition() + move);

	//時間まで行動を続ける
	if (!--time)
		return false;

	//次の行動へ移る
	return true;
}

//timeフレーム右旋回
bool RightTurn::Action(GameObject & gb, int & time)
{
	//右回転
	gb.SetRotateY(gb.GetRotate().vecY + 1);

	//前進
	XMVECTOR move = XMVectorSet(0, 0, 0.1f, 0);
	move = XMVector3TransformCoord(move, XMMatrixRotationX(XMConvertToRadians(gb.GetRotate().vecX)));
	move = XMVector3TransformCoord(move, XMMatrixRotationY(XMConvertToRadians(gb.GetRotate().vecY)));
	gb.SetPosition(gb.GetPosition() + move);

	//時間まで行動を続ける
	if (!--time)
		return false;

	//次の行動へ移る
	return true;
}
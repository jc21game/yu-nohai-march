#pragma once
//インクルード
#include"Engine/GameObject.h"

//魚の行動をストラテジを使用した例
class Strategy
{
public:
	//引数:呼んだゲームオブジェクト
	//引数:動作時間
	//戻値:目的地にたどり着いたかどうか
	//注文された行動を実行する関数
	virtual bool Action(GameObject& gb, int& time) = 0;
};


//停止
class Stop : public Strategy
{
public:
	//timeフレーム待機
	bool Action(GameObject& gb, int& time) override;
};

//前進
class Adv : public Strategy
{
public:
	//timeフレーム前進
	bool Action(GameObject& gb, int& time) override;
};

//左旋回
class LeftTurn : public Strategy
{
public:
	//timeフレーム左旋回
	bool Action(GameObject& gb, int& time) override;
};

//右旋回
class RightTurn : public Strategy
{
public:
	//timeフレーム右旋回
	bool Action(GameObject& gb, int& time) override;
};
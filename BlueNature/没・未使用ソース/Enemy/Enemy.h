#pragma once
#include "Engine/GameObject.h"
#include "FishAssets.h"

//テスト魚を管理するクラス
class Enemy : public GameObject
{
	//テストデータ(本番では動く時間は設定しないが、今はこれで)
	const int TIME;				//行動時間(テストデータ)
	int time_;					//残り行動時間(テストデータ)

	//モデルに関するメンバ
	int modelHandle_;			//モデル番号

	//モーションに関するメンバ
	AnimationSet anime_;		//アニメーション用設定集

public:
	//コンストラクタ
	Enemy(GameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
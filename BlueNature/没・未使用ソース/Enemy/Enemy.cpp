#include "Enemy.h"
#include "Engine/Model.h"
#include "Animation.h"
#include "GameScene.h"

//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:GameObject(parent, "Enemy"),
	TIME(100), time_(TIME), modelHandle_(-1)
{
	//初期行動変更(テストデータ)
	anime_.style = ADVANCE;
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	modelHandle_ = Model::Load("3DModel/Butterflyfish/Butterflyfish.fbx");
	assert(modelHandle_ >= 0);

	//初期位置
	transform_.position_ = XMVectorSet(0, 0, (float)(rand() % 100) + 20.0f, 0);

	//初期向き
	transform_.rotate_.vecY = 90.0f;
}

//更新
void Enemy::Update()
{
}

//描画
void Enemy::Draw()
{
	//キノコの描画
	Model::SetTransform(modelHandle_, transform_);
	Model::Draw(modelHandle_);
}

//開放
void Enemy::Release()
{
}
#pragma once

#include <list>
#include <queue>
#include <vector>
#include <array>
#include <memory>
#include <random>

#include "Engine/Direct3D.h"
#include "Engine/GameObject.h"
#include "Engine/Timer.h"
#include "Behavior.h"


//キャラクタAIを管理するクラス
class CharacterAI 
{

private:

	//追加された行動に関する構造体
	struct AddedBehaviorParams
	{
		//コンストラクタ(引数あり)
		//params : 比重(優先度<昇順>)、行動(クラスのポインタ)
		//return : NULL
		AddedBehaviorParams(int weight, std::unique_ptr<Behavior>& behavior)
		{
			weight_ = weight;
			behavior_ = std::move(behavior);
		};

		int weight_;							//比重
		std::unique_ptr<Behavior> behavior_;	//行動
	};
	std::list<std::unique_ptr<AddedBehaviorParams>> addedBehaviors_;	//行動リスト
	

	//基本行動に関する構造体
	struct DefaultBehaviorParams 
	{
		DefaultBehaviorParams(BehaviorPattern pattern, std::unique_ptr<Behavior>& behavior) 
		{
			pattern_ = pattern;
			behavior_ = std::move(behavior);
		}

		BehaviorPattern pattern_;				//種類(enum)
		std::unique_ptr<Behavior> behavior_;	//行動
	};
	std::array<std::unique_ptr<DefaultBehaviorParams>,BEHAVIOR_MAX_> defaultBehaviors_;		//基本行動

	std::vector<BehaviorPattern> choicedBehaviors_;	//選択された行動の種類
	
	float waitChoiceTime_;		//実行待ち時間
	const float CHOICE_TIME_;	//実行するタイミング

public:

	//コンストラクタ
	//params : NULL
	//return : NULL
	CharacterAI();

	//デストラクタ
	//params : NULL
	//return : NULL
	~CharacterAI();

	//実行
	//params : NULL
	//return : NULL
	void Execute();

	//行動選択
	//params : NULL
	//return : NULL
	void ChoiceBehavior();

	//前進するかどうか
	//params : 乱数
	//return : 前進or停止
	BehaviorPattern WhetherToAdvance(int rand);

	//左右どっちに旋回するか
	//params : 乱数
	//return : 左or右
	BehaviorPattern ChoiceLeftRight(int rand);

	//上下どっちに動くか
	//params : 乱数
	//return 上or下
	BehaviorPattern ChoiceRiseDescent(int rand);

	//行動追加(基本行動以外)
	//params : weight(比重・優先度<昇順>)、behavior(行動)
	//return : NULL
	void AddBehavior(int weight, std::unique_ptr<Behavior> behavior);

	//基本行動の設定
	//params : 行動の種類、行動
	//return : NULL
	void SetDefaultBehavior(BehaviorPattern pattern, std::unique_ptr<Behavior> behavior);

};


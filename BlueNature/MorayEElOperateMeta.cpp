#include "MorayEElOperateMeta.h"

#include "Engine/Model.h"
#include "GameMetaAI.h"
#include "GroundOperateMeta.h"
#include "Ground.h"
#include "MorayEel.h"
#include "Player.h"



MorayEElOperateMeta::MorayEElOperateMeta():
	frameCounter_(0)
{
	for (int i = 0; i < IS_CREATE_; i++)
	{
		hModel_[i] = -1;
		pMorayEel_[i] = nullptr;
	}

	//ウツボを生成する地面プレート番号を登録
	createPlate[0] = GROUND_0;
	createPlate[1] = GROUND_0;
	createPlate[2] = GROUND_0;


}

MorayEElOperateMeta::~MorayEElOperateMeta()
{
}

void MorayEElOperateMeta::Initialize(GameObject * pSceneObject, GameObject * pPlayer)
{
	pSceneObject_ = pSceneObject;
	pPlayer_ = pPlayer;


	//地面のプレートから
	//ウツボの生成位置を指定する

	//地面プレートの範囲は、
		//５００＊５００＊５００なので、
		//仮に地面の原点が（０，０，０，０）なら
		//範囲は、ｘ　-250 ~ 250
		//		　ｚ　-250 ~ 250
		//上記の範囲になる。　つまり -サイズ/2  ~ サイズ / 2になるということ

		//その範囲内でランダムに位置をとればよい
		//そして、生成の時に、Y軸は、地面に沿うようにレイキャストする。
	

	//地面プレートの横幅、縦幅の半分の長さ。
	//原点から左辺、右辺までの距離
	int size = (SPLIT_SIZE * MODEL_SIZE) / 2.0f;

	for (int i = 0; i < IS_CREATE_; i++)
	{
		XMVECTOR vec = GameMetaAI::GetGroundPos(createPlate[i] - 1);
		dispPos[i] = XMVectorSet(vec.vecX + (GetRandomCood() * (rand()%size)),
			0, vec.vecZ + (GetRandomCood() * (rand()%size)), 0);


		/*XMVECTOR vec = GameMetaAI::GetGroundPos(createPlate[i] - 1);
		dispPos[i] = XMVectorSet(vec.vecX + (GetRandomCood() * (rand()%250)),
			0, vec.vecZ + (GetRandomCood() * (rand()%250)), 0);*/
	}


	//設置位置を指定
		//本来であれば、ランダムで位置を取得したかった
	Transform trans[3];
	{
		dispPos[0] = trans[0].position_ = XMVectorSet(50, -360, 0, 0);
	}
	{
		dispPos[1] = trans[1].position_ = XMVectorSet(300, -380, 0, 0);
	
	}
	{
		dispPos[2] = trans[2].position_ = XMVectorSet(0, -400, 1500, 0);

	}


	for (int i = 0; i < IS_CREATE_; i++)
	{
		pMorayEel_[i] = (MorayEel*)Instantiate<MorayEel>(pSceneObject_);
		hModel_[i] = pMorayEel_[i]->GetModelHundle();
		pMorayEel_[i]->SetPosition(trans[i].position_);
	}





}

//１OR-1の符号として扱う値を返す
int MorayEElOperateMeta::GetRandomCood()
{
	int cood = rand() % 2;
	
	//1なら
	if (cood)
	{
		return 1;
	}
	//0なら
	else
	{
		return -1;
	}

}


void MorayEElOperateMeta::Update(std::list<GameObject*>& lookObject)
{
	//カウンターを更新
	frameCounter_++;

	
	////ウツボモデルのDrawを行うかの判定
	//DrawExe();

	if (frameCounter_ > UPDATE_FRAME_)
	{
		//ウツボモデルのUpdateとDrawを行うかの判定
		UpdateAndDrawExe();

		//レイキャスト対象のモデルを切り替える
		ChangeRayCastModel();

		//フレームカンターの初期化
		frameCounter_ = 0;
	}

}

//void MorayEElOperateMeta::DrawExe()
//{
//	int b;
//}

void MorayEElOperateMeta::UpdateAndDrawExe()
{
	//Player* pPlayer = (Player*)pPlayer_;

	////プレイヤーの現在位置を取得
	//XMVECTOR playPos = pPlayer->GetPosition();




	////全ウツボの座標を受け取り、
	//それぞれ距離を取る
	for (int i = 0; i < IS_CREATE_; i++)
	{
		//Update許可範囲内か調べる
		InRange(i, UPDATE_RANGE_, UPDATE_);
		//Draw許可範囲内か調べる
		InRange(i, DRAW_RANGE_, DRAW_);
	}
}

void MorayEElOperateMeta::InRange(int suffix, int range, TYPE type)
{
	//void Enter();			// Updateを許可
	//void Leave();			// Updateを拒否
	//void Visible();			// Drawを許可
	//void Invisible();		// Drawを拒否


	//ウツボがすでに生成済みであり
	if (hModel_[suffix] != -1)
	{


		//ウツボの位置を取得
		XMVECTOR morayEelPos = dispPos[suffix];
		morayEelPos = XMVectorSet(morayEelPos.vecX, morayEelPos.vecY, morayEelPos.vecZ, 0);


		//プレイヤーの現在値取得
		XMVECTOR playPos = pPlayer_->GetPosition();
		playPos = XMVectorSet(playPos.vecX, playPos.vecY, playPos.vecZ, 0);



		//距離をY座標抜きで行うために
		//地面のY座標をプレイヤーのY座標に合わせる
		morayEelPos.vecY = playPos.vecY;


		//プレイヤーと地面との距離をとる
		float length = XMVector3Length(morayEelPos - playPos).vecX;

		
		if (length > range)
		{
			//範囲を超えた時
			if (type == UPDATE_)
			{
				//Updateを拒否
				pMorayEel_[suffix]->Leave();
			}
			else if (type == DRAW_)
			{
				//Drawを拒否
				pMorayEel_[suffix]->Invisible();
			}
		}
		else
		{
			//範囲を超えた時
			if (type == UPDATE_)
			{
				//Updateを許可
				pMorayEel_[suffix]->Enter();
			}
			else if (type == DRAW_)
			{
				//Drawを許可
				pMorayEel_[suffix]->Visible();
			}
		
		}
	}
}

void MorayEElOperateMeta::Release()
{
}



//void MorayEElOperateMeta::UpdateLoadMorayEel()
//{
//
//}

void MorayEElOperateMeta::LoadMorayEel(int groundNumber, int hGroundModel)
{
	for (int i = 0; i < IS_CREATE_; i++)
	{
		//モデル番号がー１つまり、まだ、生成されていない
		if (hModel_[i] == -1)
		{
			//ウツボの生成する番号（1オリジン）　＝＝　引数にもらった地面番号（0オリジン）
				//値を合わせるために　0オリジンを＋１
			if (createPlate[i] == groundNumber + 1)
			{
				//ウツボの生成
				//親：ゲームシーンオブジェクト
				MorayEel* pMorayEel =
					Instantiate<MorayEel>(pSceneObject_);

				
				//モデルハンドルを取得
				hModel_[i] = pMorayEel->GetModelHundle();

				//地面にウツボのモデルを添わせる
					//地面生成時点、すぐには地面の大きさは変えられていないので、
					//すぐに計算をしては、地面のサイズが地面生成時の初期の値になってしまう。
				FitGround(i , pMorayEel , hGroundModel);



			}
		
		}

	}


}



void MorayEElOperateMeta::FitGround(int morayEelNum, GameObject* pMorayEel ,int hGroundModel)
{
	//引数のモデルハンドルのモデルと
	//レイキャストによる当たり判定を行う
	
	RayCastData* pRayCast = new RayCastData;
	//レイの方向を　真下へ
	pRayCast->dir = XMVectorSet(0, -1, 0, 0);
	//レイのスタート位置を　地面プレート吹きのランダム位置から
	pRayCast->start = dispPos[morayEelNum];

	//レイキャスト実行
	Model::RayCast(hGroundModel , pRayCast);

	//衝突した場合、
	//衝突地点、
		//この場合、地面の表面に当たるので、
		//その表面の位置までウツボのモデルを下げて、その位置を、ウツボの定位置とする。
	if (pRayCast->hit)
	{
		//ウツボの位置を更新
		XMVECTOR vec = XMVectorSet(dispPos[morayEelNum].vecX,
			dispPos[morayEelNum].vecY - pRayCast->dist,
			dispPos[morayEelNum].vecZ,
			0);

		//オブジェクトの位置を更新
		pMorayEel->SetPosition(vec);

		//自身のメンバ変数の座標を更新
		dispPos[morayEelNum] = vec;

	
	}
	else
	{
		//失敗した場合
		//生成したことをなかったことにする
		pMorayEel->KillMe();

		//モデル番号を消去し、アクセス不可能へ
		hModel_[morayEelNum] = -1;
		
	}



	delete pRayCast;

}



//プレイヤーの座標を確認して、
//プレイヤーとレイキャストの当たり判定を行う地面番号を与える。
void MorayEElOperateMeta::ChangeRayCastModel()
{
	//プレイヤーの座標を調べる
	//プレイヤーの座標がそもそも、地面には絶対に触れない高さ
		//地面のY座標が４００なので、プレイヤーのY座標がー100程度であれば、絶対に地面には触れない。なので、その時点で、プレイヤーには、
		//レイでの衝突判定を行わないようにさせる


	Player* pPlayer = (Player*)pPlayer_;

	//プレイヤーの現在位置を取得
	XMVECTOR playPos = pPlayer->GetPosition();

	if (playPos.vecY >= MIN_GROUND_POS_Y)
	{
		//−１を渡して、
		//レイキャストをしなくてよいとする
		pPlayer->ChangeMorayeelModelHundle(-1);
		//プログラム終了
		return;
	}

	int morayNumber = -1;

	float minLength = 9999999;


	//全ウツボの座標を受け取り、
	//プレイヤーと一番近いウツボを受け取る
	for (int i = 0; i < IS_CREATE_; i++)
	{
		//ウツボがすでに生成済みであり
		if (hModel_[i] != -1)
		{
			//ウツボの位置を取得
			XMVECTOR morayEelPos = dispPos[i];
			morayEelPos = XMVectorSet(morayEelPos.vecX, morayEelPos.vecY, morayEelPos.vecZ, 0);


			//プレイヤーの現在値取得
			XMVECTOR playPos = pPlayer_->GetPosition();
			playPos = XMVectorSet(playPos.vecX, playPos.vecY, playPos.vecZ, 0);



			//距離をY座標抜きで行うために
			//地面のY座標をプレイヤーのY座標に合わせる
			morayEelPos.vecY = playPos.vecY;


			//プレイヤーと地面との距離をとる
			float length = XMVector3Length(morayEelPos - playPos).vecX;

			//現在の最小の長さのほうが大きいとき
			//かつ
			//ここまでは近づける最長の長さを下回っているとき
			if (minLength > length &&
				length < MAX_DIST)
			{
				morayNumber = i;
				minLength = length;

			}


		}

	}

	//最小の長さを持つウツボを求められなかった
	if (morayNumber == -1)
	{
		//-1を返す
		pPlayer->ChangeMorayeelModelHundle(-1);

	}
	else
	{
		//プレイヤー間が最小の長さのウツボのモデル番号ハンドルを
		//送る
		pPlayer->ChangeMorayeelModelHundle(hModel_[morayNumber]);
	}

}


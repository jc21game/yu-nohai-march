//インクルード
#include "DoRightTurn.h"
#include "FishBase.h"


//コンストラクタ
DoRightTurn::DoRightTurn(GameObject* pGameObject)
{
	pObject_ = pGameObject;
	pFish_ = dynamic_cast<FishBase*>(pObject_);
}

//デストラクタ
DoRightTurn::~DoRightTurn()
{
}

//実行可能か
bool DoRightTurn::ShouldExecute()
{
	//タイミング指定なし
	return !pFish_->IsCongnition();
}

//実行
void DoRightTurn::Execute()
{
	//左回転
	pFish_->SetRotateY(pFish_->GetRotate().vecY + pFish_->GetRotateSpeedX());
}

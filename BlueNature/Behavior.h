#pragma once
#include <random>
#include <memory>
#include "Engine/GameObject.h"
#include "Engine/Timer.h"

class FishBase;

//行動の種類
enum BehaviorPattern
{
	ADVANCE_ = 0,		//前進
	LEFT_TURN_,		//左旋回
	RIGHT_TURN_,	//右旋回
	RISE_,			//上昇
	DESCENT_,		//降下
	NO_MOVE_,		//停止
	BEHAVIOR_MAX_,
	NO_CHOICED_		//未選択
};


class Behavior {

protected:

	GameObject* pObject_;		//ゲームオブジェクトポインタ
	GameObject* pTarget_;
	std::string targetName_;
	FishBase* pFish_;			//pObjectをFishBaseにキャストして使用するため用意
	
public:

	
	//デストラクタ
	//params : NULL
	//return : NULL
	virtual ~Behavior() = default;

	//実行可能かどうか
	//params : NULL
	//return : 実行可能か
	virtual bool ShouldExecute() = 0;

	//実行
	//params : NULL
	//return : NULL
	virtual void Execute() = 0;	
};


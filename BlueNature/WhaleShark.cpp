#include "WhaleShark.h"
#include "RunAway.h"
#include "GetCloser.h"
#include "Engine/BoxCollider.h"
#include "GameMetaAI.h"



//コンストラクタ
WhaleShark::WhaleShark(GameObject* parent)
	: FishBase(parent, "WhaleShark")
	, pLeftEye_(nullptr), pRightEye_(nullptr), pBody_(nullptr)
{

	//自身のオブジェクトのコストをセットする(正確に言うと更新、FishBaseにて、初期コストは設定されているので、更新を行わずともエラーにはならない)
	SetThisCost(10);

	//CSVファイルよりコストを取得
	//MetaCsvType：FISH_INFORMATION（魚情報CSVファイル）
	//FishType：BUTTER_FLY_FISH（チョウチョウウオ）
	//return：10
	int cost = GameMetaAI::GetObjectInfoCsv(FISH_INFORMATION, WHALE_SHARK);

	//自身のメンバの構造体変数に、自身のコストをセット（CSVファイルから読み込んだ）
	SetThisCost(cost);

	//基本の移動速度
	pStatus_->moveSpeed_ = 0.2f;

	//各回転速度
	pStatus_->rotateSpeedX_ = 0.05f;
	pStatus_->rotateSpeedY_ = 0.5f;

	//魚本体の当たり判定
	//pStatus_->bodyCenter_ = XMVectorSet(-12.f, 140.f, 88.f, 0.f);
	//pStatus_->bodySize_ = XMVectorSet(250.f, 90.f, 550.f, 0.f);
	pStatus_->bodyCenter_ = XMVectorSet(0.f, 0.f, 0.f, 0.f);
	pStatus_->bodySize_ = XMVectorSet(100.f, 50.f, 500.f, 0.f);


	////左目の認識範囲
	//pStatus_->leftEyeCenter_ = XMVectorSet(-142.f, 150.f, 265.f, 0.f);
	//pStatus_->leftEyeRange_ = XMVectorSet(100.f, 100.f, 130.f, 0.f);

	////右目の認識範囲
	//pStatus_->rightEyeCenter_ = XMVectorSet(118.f, 150.f, 265.f, 0.f);
	//pStatus_->rightEyeRange_ = XMVectorSet(100.f, 100.f, 130.f, 0.f);

	//最大認識時間
	pStatus_->maxCongnitionTime_ = 1.f;
	pStatus_->maxVisuallyTime_ = 1.f;

	MAX_ANIM_FRAME_ = 240;
}

//デストラクタ
WhaleShark::~WhaleShark()
{

}

//初期化
void WhaleShark::Initialize()
{
	FishBase::Initialize();


	//モデルロード
	LoadModel("3DModel/MS2_3DModel/WhaleShark2/WhaleShark2.fbx");


	/**********各当たり判定(認識範囲)の追加**********/
	//身体
	pBody_ = new BoxCollider(pStatus_->bodyCenter_, pStatus_->bodySize_);
	AddCollider(pBody_);

	////左目
	//pLeftEye_ = new BoxCollider(pStatus_->leftEyeCenter_, pStatus_->leftEyeRange_);
	//AddCollider(pLeftEye_);

	////右目
	//pRightEye_ = new BoxCollider(pStatus_->rightEyeCenter_, pStatus_->rightEyeRange_);
	//AddCollider(pRightEye_);

	


	//拡大
	transform_.scale_ = XMVectorSet(5.0f, 5.0f, 5.0f, 0);
	//transform_.position_.vecY = 20.f;

	//メタAIに自身を追加させる（名前空間）
	//params1：自身を指揮するメタAIの種類・タイプ（enum値）
	//params2：自身のオブジェクトの標準コスト
	//params3：自身のオブジェクトの種類・タイプ（enum値→int）
	GameMetaAI::AddMetaAIMember(FISH_OPERATE_META, this, GetThisCost(), (int)WHALE_SHARK);
}

//更新
void WhaleShark::Update()
{


	PlayAnimation();

	CalcVerticalAngle();

	//認識した
	DidCongnition();

	DidVisually();

	//見えているか
	WhaleShark::Look(FindObject("Player"));

	//CharacterAIの実行
	pCharAI_->Execute();


}

//解放
void WhaleShark::Release()
{
	//メタAIに自身を消去させる（名前空間）
	//params1：自身を指揮するメタAIの種類・タイプ（enum値）
	//params2：自身のオブジェクトの標準コスト
	//params3：自身のオブジェクトの種類・タイプ（enum値→int）
	GameMetaAI::RemoveMetaAIMember(FISH_OPERATE_META, this, GetThisCost(), (int)WHALE_SHARK);
}

void WhaleShark::OnCollision(GameObject * pTarget)
{
	Timer::Reset();
	if (pTarget->GetObjectName() == "Player")
	{
		//ジンベイザメの場合、
//接触時プレイヤーを押し出す
//プレイヤーと、ジンベイザメの方向ベクトルを取得
		XMVECTOR playerPos = pTarget->GetPosition();
		XMVECTOR dir = playerPos - this->GetPosition();

		//プレイヤーとジンベイザメの距離を出す
		//float distance = XMVector3Length(dir).vecX;

		//正規化
		dir = XMVector3Normalize(dir);

		dir *= 0.3f;
		//Ｙ座標をした方向にする
		dir.vecY = -0.1f;



		//プレイヤーを押し出す
		/*if (distance > 50.0f)
		{
			pTarget->SetPosition(pTarget->GetPosition() + (dir));
		}
		else*/
		{
			pTarget->SetPosition(pTarget->GetPosition() + (dir));
		}



		//プレイヤーを押し出す
		pTarget->SetPosition(pTarget->GetPosition() + (dir));

		pStatus_->congnitionTime_ = 0;
	}
}

//近づきたい相手を目視した場合に、認識フラグを変更する処理
void WhaleShark::Look(GameObject * pTarget)
{
	//相手がプレイヤーのとき
	if (pTarget->GetObjectName() == "Player")
	{
		InRange(this, pTarget, (pStatus_->rightEyeRange_ / 2), (pStatus_->leftEyeRange_ / 2));
		
		
		
		//右目か左目でプレイヤーを目視したとき
		if (pStatus_->isRightEye_ ^ pStatus_->isLeftEye_)
		{
			


			//目視フラグ更新
			pStatus_->isVis_ = true;
		}
	}
}




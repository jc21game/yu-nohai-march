#pragma once
#include "FishBase.h"

/*
*
* テスト用の魚オブジェクト
* 作者名：伊達龍二
* 制作日：2020/10/26〜
* その他記述すべきことがある場合の記述
*	↓
*/
/*

テスト用の魚オブジェクト
	！！のちに消します。！！

	テスト目的：魚の種類が欲しかったため。
*/


//バタフライフィッシュを管理するクラス
class TestTankBodyObject : public FishBase
{

public:
	//コンストラクタ
	//params : 親オブジェクト
	//return : NULL
	TestTankBodyObject(GameObject* parent);

	//デストラクタ
	//params : NULL
	//return : NULL
	~TestTankBodyObject();

	//初期化
	//params : NULL
	//return : NULL
	void Initialize() override;

	//更新
	//params : NULL
	//return : NULL
	void Update() override;


	//解放
	//params : NULL
	//return : NULL
	void Release() override;
};


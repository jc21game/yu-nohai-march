#pragma once
#include "Behavior.h"

class Advance : public Behavior 
{

public:

	//コンストラクタ
	//params : ゲームオブジェクトポインタ
	//return : NULL
	Advance(GameObject* pGameObject);

	//デストラクタ
	//params : NULL
	//return : NULL
	~Advance();

	//実行可能か
	//params : NULL
	//return : 実行可能ならばTrue
	bool ShouldExecute() override;

	//実行
	//params : NULL
	//return : NULL
	void Execute() override;
};


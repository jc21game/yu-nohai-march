#include "FishBase.h"
#include "Engine/Global.h"
#include "Advance.h"
#include "DoLeftTurn.h"
#include "DoRightTurn.h"
#include "Rise.h"
#include "Descent.h"
#include "NoMove.h"

//コンストラクタ
FishBase::FishBase(GameObject* parent, const std::string& name)
	: GameObject(parent, name),modelHandle_(-1), eyeRate_(4.0f),animFrame_(0), MAX_ANIM_FRAME_(90)
	,metaInfo(new MetaInfo)
	
{
	
	pPlayer_ = FindObject("Player");

	//AI機能にアクセスするためのオブジェクト
	pCharAI_ = std::make_unique<CharacterAI>();

	//各魚のステータスにアクセスするためのオブジェクト
	pStatus_ = std::make_unique<FishStatus>();

	//各ステータスの初期化
	pStatus_->viewDir_ = XMVectorSet(0.f,0.f,0.1f,0.f);
	pStatus_->moveSpeed_ = 0.1f;
	pStatus_->rotateSpeedX_ = 0.5f;
	pStatus_->rotateSpeedY_ = 0.5f;
	pStatus_->bodyCenter_ = g_XMZero;
	pStatus_->bodySize_ = g_XMZero;
	pStatus_->leftEyeCenter_ = g_XMZero;
	pStatus_->leftEyeRange_ = g_XMZero;
	pStatus_->rightEyeCenter_ = g_XMZero;
	pStatus_->rightEyeRange_ = g_XMZero;
	pStatus_->congnitionTime_ = 0.f;
	pStatus_->maxCongnitionTime_ = 3.f;
	pStatus_->visuallyTime_ = 0.f;
	pStatus_->maxVisuallyTime_ = 0.f;

	front_ = XMVectorSet(0.f, 0.f, 1.f, 0.f);
	
}

//デストラクタ
FishBase::~FishBase()
{
	SAFE_DELETE(metaInfo);
}

//初期化
void FishBase::Initialize()
{
	pCharAI_->SetDefaultBehavior(ADVANCE_, std::make_unique<Advance>(this));
	pCharAI_->SetDefaultBehavior(LEFT_TURN_, std::make_unique<DoLeftTurn>(this));
	pCharAI_->SetDefaultBehavior(RIGHT_TURN_, std::make_unique<DoRightTurn>(this));
	pCharAI_->SetDefaultBehavior(RISE_, std::make_unique<Rise>(this));
	pCharAI_->SetDefaultBehavior(DESCENT_, std::make_unique<Descent>(this));
	pCharAI_->SetDefaultBehavior(NO_MOVE_, std::make_unique<NoMove>(this));

}

//更新
void FishBase::Update()
{
	
}

//描画
void FishBase::Draw()
{
	

	Model::SetTransform(modelHandle_, transform_);
	Model::Draw(modelHandle_);
}
//解放
void FishBase::Release()
{

}

//モデルロード
void FishBase::LoadModel(const std::string& filePath)
{
	modelHandle_ = Model::Load(filePath);
	assert(modelHandle_ >= 0);
}

//プレイヤーの距離・方向を更新
void FishBase::UpdatePlayerDir()
{
	objectPos_ = this->GetPosition();	//逃げる側の位置
	targetPos_ = pPlayer_->GetPosition();	//衝突した対象の位置
	playerDir_ = targetPos_ - objectPos_;			//上2つの向きベクトル
}

//どのくらい縦に回転しているか、角度を求める
void FishBase::CalcVerticalAngle()
{

	XMMATRIX mat = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));
	pStatus_->viewDir_ = XMVector3TransformCoord(pStatus_->viewDir_, mat);

	pStatus_->viewDir_ = XMVector3Normalize(pStatus_->viewDir_);
	front_ = XMVector3Normalize(front_);

	float dot = XMVector3Dot(pStatus_->viewDir_, front_).vecX;

	float angle = acos(dot);

	pStatus_->verticalAngle_ = XMConvertToDegrees(angle);
}

//アニメーション再生
void FishBase::PlayAnimation()
{
	Model::SetAnimFrame(modelHandle_, animFrame_, animFrame_, 0);
	animFrame_++;
	if (animFrame_ >= MAX_ANIM_FRAME_)
		animFrame_ = 0;
}


//認識した時の処理
void FishBase::DidCongnition() 
{
	//認識したならば
	if (pStatus_->isCong_)
	{
		//認識時間の更新
		UpdateCongnitionTime();

		//プレイヤーの方向を算出する
		UpdatePlayerDir();
	
		//認識時間が設定された最大時間以上ならば
		if (pStatus_->congnitionTime_ >= pStatus_->maxCongnitionTime_) 
		{
			//フラグを降ろす
			pStatus_->isCong_ = false;
		}
	}
}

//認識しているときの処理
void FishBase::DidVisually()
{
	//見えているとき
	if (pStatus_->isVis_)
	{
		//経過時間の計測
		UpdateVisuallyTime();

		//設定した時間よりも値が大きくなったとき
		if (pStatus_->visuallyTime_ >= pStatus_->maxVisuallyTime_)
		{
			//フラグを降ろす
			pStatus_->isVis_ = false;

			//有っても無くても良い処理
			Timer::Reset();
			pStatus_->visuallyTime_ = 0;
		}
	}
}


//認識時間の更新
void FishBase::UpdateCongnitionTime() 
{
	pStatus_->congnitionTime_ += Timer::GetElapsedSecounds();
}


//認識時間の更新
void FishBase::UpdateVisuallyTime()
{
	//この計算結果だと最初以外常に同じ値が入り続ける
	/*pStatus_->visuallyTime_ +=
		Timer::GetElapsedSecounds() - pStatus_->visuallyTime_;*/
	pStatus_->visuallyTime_ += Timer::GetElapsedSecounds();
}


//認識しているかどうか
bool FishBase::IsCongnition()
{
	return pStatus_->isCong_;
}

float FishBase::GetVerticalAngle()
{
	return pStatus_->verticalAngle_;
}


//移動速度の取得
float FishBase::GetMoveSpeed() 
{
	return pStatus_->moveSpeed_;
}

float FishBase::GetRotateSpeedX()
{
	return pStatus_->rotateSpeedX_;
}

float FishBase::GetRotateSpeedY()
{
	return pStatus_->rotateSpeedY_;
}

XMVECTOR FishBase::GetPlayerDir()
{
	return playerDir_;
}

//目視しているかどうか
bool FishBase::IsVisually()
{
	return pStatus_->isVis_;
}

//見えている範囲内にいるかチェック
void FishBase::InRange(GameObject * own, GameObject * target, XMVECTOR rightRange, XMVECTOR leftRange)
{
	//相手の位置を保持
	XMVECTOR tarPos = target->GetPosition();
	XMVECTOR tarRotate = target->GetRotate();


	//自分の位置を保持
	XMVECTOR ownPos = own->GetPosition();
	XMVECTOR ownRotate = own->GetRotate();

	//自分と相手の距離を出す
	XMVECTOR disPos = tarPos - ownPos;

	//相手が自分の右側にいるかをチェック-----------------------------------------------------------
	if (0 < disPos.vecX  && disPos.vecX < (rightRange.vecX * (eyeRate_ - 1)) &&
		(-rightRange.vecY) < disPos.vecY && disPos.vecY < (rightRange.vecY) &&
		(-rightRange.vecZ * eyeRate_) < disPos.vecZ && disPos.vecZ < (rightRange.vecZ * eyeRate_))
	{
		//見えている
		pStatus_->isRightEye_ = true;
	}
	else
	{
		//見えていない
		pStatus_->isRightEye_ = false;
	}

	//相手が自分の左側にいるかをチェック----------------------------------------------------------
	if ((-leftRange.vecX * (eyeRate_ - 1)) < disPos.vecX && disPos.vecX < 0 &&
		(-leftRange.vecY) < disPos.vecY && disPos.vecY < (leftRange.vecY) &&
		(-leftRange.vecZ * eyeRate_) < disPos.vecZ && disPos.vecZ < (leftRange.vecZ * eyeRate_))
	{
		//見えている
		pStatus_->isLeftEye_ = true;
	}
	else
	{
		//見えていない
		pStatus_->isLeftEye_ = false;
	}
}

//XMVECTOR FishBase::GetRightRange()
//{
//	return pStatus_->rightEyeRange_;;
//}
//
//XMVECTOR FishBase::GetLeftRange()
//{
//	return pStatus_->leftEyeRange_;
//}
//
bool FishBase::GetIsRightEye()
{
	return pStatus_->isRightEye_;
}

bool FishBase::GetIsLeftEye()
{
	return pStatus_->isLeftEye_;
}
//
//XMVECTOR FishBase::GetRightCenter()
//{
//	return pStatus_->rightEyeCenter_;
//}
//
//XMVECTOR FishBase::GetLeftCenter()
//{
//	return pStatus_->leftEyeCenter_;
//}

/* GE2A09 菅野陽暉	 最終変更日：2020/09/16 */
/* 地面を表示するクラス、表示範囲指定のみ */
#pragma once

//インクルード
#include "Engine/GameObject.h"


//マクロ

#define SPLIT_COUNT 3			//陸の個数 X * X　の「X」	


//現在未使用
//伊達編集→ここで一気に作るのではなく、メタAIに生成管理を行わせたい。（プレイヤーが地面配置位置に近づいたら、メタAIのほうで、LoadGroundを呼び、ロードさせるような処理にしようと、考えている）
#define SPLIT_DATE_COUNT 3		//伊達編集 = 地面プレート9枚　１行３つ



#define SPLIT_SIZE 1000.0f		//一陸分の大きさ	//伊達編集＝だが、FBXモデルのサイズが１＊１＊１ではないため、ゲーム中は500というきりの良い数値にはならない。

								//モデルの大きさ２*2*2
								//それを１０００倍なので、横２０００縦２０００



#define MODEL_SIZE 2.0f		//FBXデータでのモデルのサイズ（Scale値）（最終的なサイズはSPLIT_SIZE＊MODEL_SIZE　の大きさになる。）
#define DISPLAY_RANGE 25.0f		//陸を表示する範囲(円の半径)


#define SPLIT_POS_Y -400.0f		//伊達編集＝　地面のY座標値


//プレイヤーのプロトタイプ宣言　
class Player;

//地面を管理するクラス
class Ground : public GameObject
{
private:
	//モデル番号
	int hModel_[SPLIT_COUNT][SPLIT_COUNT];

	int worldModel_;


	//現在未使用
	//伊達追記
	//追記理由：元のモデル番号を消さないように、かつ、自分が求めている形にするため、新たに生成
	//モデル番号
	int hDateModel_[SPLIT_DATE_COUNT][SPLIT_DATE_COUNT];


	//地面の描画位置
	XMVECTOR pos_[SPLIT_COUNT][SPLIT_COUNT];



	//プレイヤーの情報
	Player* pPlayer_;

public:
	//コンストラクタ
	Ground(GameObject* parent);

	//デストラクタ
	~Ground();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//★11/4　追記者：伊達龍二
	//★追記理由：メタAIの実装段階で、地面のモデル番号などを取得しなければいけない事態が生じたため、モデル番号を受け取る関数を追加した。
	//モデル番号を取得する関数
	//引数：なし
	//戻値：地面のモデル番号（ハンドル）
	int GetModelHundle() {return hModel_[0][0];	//(現在は１つしかないので、)１番目の地面プレートのモデル番号を返す
	}

	//地面モデル番号を取得する
	//引数の番号にて選択
	//引数：モデル識別番号
	int GetOneModelHundle(int groundNumber);



	//世界の地面モデルのモデル番号を返す
	//引数：なし
	//戻値：地面のモデル番号（ハンドル）
	int GetWorldGroundModelHundle() {return worldModel_;
	}


	//現在未使用
	//12/17　追記者：伊達龍二
	//★追記理由：ゲーム開始時に一気に地面モデルをロードしないために、メタAIから指定されたタイミングに地面モデルを生成させる
		//もしかしたら、0番目の初期の地面も、メタAIから呼んだほうが良いかも？
	//引数にて与えられた番号の地面モデルをロードする
	//引数：生成する地面モデルの番号（0オリジン）
	//戻値：生成した地面モデルのモデルハンドル（Model.cppにおけるハンドル番号）
	int LoadGround(int groundNumber);


	//引数にて与えられた番号の地面座標を取得
		//一番近い地面を取得するために使う
	//引数：地面番号
	XMVECTOR GetGroundPos(int groundNumber);




};
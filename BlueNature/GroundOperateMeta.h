#pragma once
/*
*
	指揮メタAI
	地面モデル指揮メタAI
* 地面の生成と、地面のモデル番号の所有
* 作者名：伊達龍二
* 制作日：2020/10/25〜
* その他記述すべきことがある場合の記述
*		enumのプレート番号と、地面のモデルの番号が共通するようにする

*/


#include <vector>
#include "MetaBase.h"


class MorayEElOperateMeta;
class GameObject;
class Ground;

//地面プレートの番号
enum GROUND_PLATE_HUNDLE
{
	//地面情報
	//�@地面は海面に突き出ない
	//�A洞窟がない

	GROUND_0  = 1,
	GROUND_1,
	GROUND_2,
	GROUND_3,
	GROUND_4,
	GROUND_5,
	GROUND_6,
	GROUND_7,
	GROUND_8,
	//GROUND_9,

	GROUND_MAX = GROUND_8

};


//地面をロードできるようになる距離
	//プレイヤーと、地面生成予定の位置が以下の距離以下になったら
	//地面のロードを宣言する
#define IS_CREATE_DISP 300.0f

//地面が絶対に存在しないであろう高さ
#define MIN_GROUND_POS_Y -100.0f




class GroundOperateMeta :
	public MetaBase
{
private:
	//地面のモデルハンドルの登録する可変長配列
	//std::vector<int> groundModelHundle_;

	//地面のモデルハンドルの登録する配列
	int groundModelHundle_[GROUND_MAX];

	//世界全体の地面を持つモデルのハンドル
	int hWorldGround_;




	//地面を生成するシーンのオブジェクトポインタ
	GameObject* pSceneObject_;
	//地面のオブジェクトポインタ（地面のオブジェクトは１つで、そのオブジェクト内に複数の地面モデルを取得する）
	Ground* pGroundObject_;

	//プレイヤーのポインタ
	GameObject* pPlayer_;

	//ウツボ生成指揮クラス
		//地面が生成されたら、
		//ウツボも生成させる
	MorayEElOperateMeta* pMorayEelMeta_;



	//地面のロード制御を行う
		//毎フレーム地面モデルのまだロードを完了していないモデルのロードを行う（条件付き）
	void UpdateLoadGround();

	//地面のロード
		//引数：生成する地面番号（0オリジン）
	void LoadGround(int createNum);

	//プレイヤーのレイキャストによる
	//当たり判定に用いるモデル番号を切り替える
	void ChangeRayCastModel();



public:
	//コンストラクタ
	//引数：なし
	//戻値：なし
	GroundOperateMeta();
	//デストラクタ
	//引数：なし
	//戻値：なし
	~GroundOperateMeta();


	// MetaBase を介して継承されました
	//初期化（地面オブジェクトの生成）
	//引数：シーンのオブジェクトポインタ
	//引数：プレイヤーのオブジェクトポインタ（自身の指揮メタAIでは、プレイヤーオブジェクトは使用しないが、ベースクラスにキャストした際に一度に呼べるように関数形式はどのクラスでも共通した形にした。）
	//戻値：なし
	void Initialize(GameObject * pSceneObject, GameObject * pPlayer) override;
	//更新
	//引数：管理下のオブジェクトリスト（毎フレーム更新として何かの指揮をする場合は、このリストに、そのオブジェクト群を送ってもらう（その際、リストへの追加は、地面オブジェクトがメタAIにリストへの追加を呼び込む（魚指揮メタAIと同じ）呼び方を行う））
			//指揮メタAIから、メタAIのリストに追加する形は、理想ではない。
	//戻値：なし
	void Update(std::list<GameObject*>& lookObject) override;
	//解放
	//引数：なし
	//戻値：なし
	void Release() override;

	//引数にて地面番号を受け取り、モデル番号を返す
	//引数：地面プレート番号（enum値）
	//戻値：地面のモデルハンドル（番号）
	int GetModelHundle(GROUND_PLATE_HUNDLE groundHundle);

	int GetWorldGroundHundle() { return hWorldGround_; };

};


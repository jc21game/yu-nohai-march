#pragma once

//インクルード
#include "Player.h"
#include "Engine/Camera.h"
#include "Engine/Input.h"


//enum
//視点
enum POINT_OF_VIEW
{
	FPS = 0,
	TPS = 1,
};

//enum
//カメラ操作
enum OPERATE_CAM_TYPE
{
	KEY = 0,
	MOUSE = 1,
};


//プレイヤーのカメラを管理するネームスペース
namespace PlayerCamera
{
	//引数:ウィンドウハンドル
	//戻値:なし
	//ウィンドウハンドル設定
	void SetWindowHandle(HWND hWnd);

	//引数: プレイヤークラスのポインタ
	//戻値: なし
	//プレイヤーのカメラに関する動作関数
	void SetPlayerCamera(Player* player);

	//引数: プレイヤークラスのポインタ
	//戻値: カメラの回転行列
	//カメラ自身に関する動作関数
	XMMATRIX RotateOwn(Player* player);

	//引数:なし
	//戻値:マウス操作か
	//現在マウスでカメラを操作するかどうか
	bool GetOperate();
}
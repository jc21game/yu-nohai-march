#pragma once
/*
*
	メタAI（各指揮メタAIの上に立つAI）
*　オブジェクトを指揮するメタAIを所有し、各指揮メタAIのUpdate処理を呼び込み
*	また、オブジェクト情報などのCSVデータを所有する。
* 作者名：伊達龍二
* 制作日：2020/10/20〜
* その他記述すべきことがある場合の記述
*
*/

//第一要素から（第一要素に登録した要素を検索地として）登録された第二要素を検索することのできる機能
#include <map>
//可変長配列（途中で消去することが容易）
#include <list>
#include <string>
#include "Engine/GameObject.h"
//CSV読み取りのための、情報を取得
#include "FishCsvInfo.h"



//指揮メタAIの種類名　をenumの値で確保
enum MetaType
{
	GROUND_OPERATE_META = 0,	//地面（海底）の操作を行うメタAI
	FISH_OPERATE_META,			//魚（キャラクタ）の操作を行うメタAI

	SEA_OPERATE_META,			//海の背景色の操作を行うメタAI


	//~~
	META_MAX

};

//CSVファイルのタイプ　をenum値で確保
//各指揮メタAI下で、必要な情報を持つCSVファイルをenum値で取得しておく
enum MetaCsvType
{
	FISH_INFORMATION = 0,	//魚の名前、標準コスト

	GROUND_ENVIRONMENT,		//地面番号、地面座標（ワールド座標値）

	FISH_COST_GROUND_ENV,	//地面に対する各魚の優先度

	FISH_HEIGHT_GROUND_ENV,	//地面に対する各魚の出現Y座標の高さ

	CSV_MAX


};



//各メタAIの総括を行うクラス（各オブジェクトの所有、各メタAIの更新呼び出しなど）
//シーン内における各オブジェクトの監視、指揮を行うメタAIを所有し、動かす名前空間
//同時に、各監視、指揮を実装するメタAI（後述：指揮メタAI）にて使用するオブジェクトの所有、登録、解放を行う。
namespace GameMetaAI
{

	/*extern変数宣言*/
	//地面数
		//地面指揮メタAIが所有する地面数から取得
	extern const int GROUND_COUNT_;	//後に宣言するというextern宣言（名前空間には、ヘッダで宣言し、中身の初期化はcppでということができないので、（コンストラクタもないし。）
									//そのため、のちに必ず中身確保するので、アクセス元はココとしておく。）
									//Direct３Dにおいても使用している





	/**ゲームループ内のいずれかのタイミングにて呼びこんでもらう**********************************************************************/
	//初期化
	//シーンが開始したときの、そのシーンにおける各指揮メタAIの初期化など
	//引数：メタAIを実装したシーンのポインタ（オブジェクトを新規に作成するときに、使用する？）
	//引数：プレイヤーのポインタ（ゲームオーバー、ゲーム途中で消去がない前提）
	//戻値：なし
	void Initialize(GameObject* pSceneObject, GameObject* pPlayer);
	/**ゲームループ内のいずれかに毎フレーム呼んでもらう**********************************************************************/
	//毎フレーム更新を行う関数
		//各指揮メタAIに該当オブジェクト群を送り、オブジェクトの監視、指揮（指揮）を行ってもらう
	//引数：なし
	//戻値：なし
	void Update();

	/**シーン終了時、あるいは切り替え時に呼んでもらう**********/
	//解放
	//引数：なし
	//戻値：なし
	void Release();

	//リスト内のオブジェクト全要素の消去
	//引数：なし
	//戻値：なし
	void AllReleaseMember();

	//オブジェクトを登録する
	//特定オブジェクトの追加(Add)
		//引数のenumの値にて追加先を指定
	//引数：自身を指揮するメタAIのタイプ
	//引数：上記のタイプで示される、オブジェクト群に追加する自身のオブジェクトのポインタ
	//引数：オブジェクトのコスト（コストを持たない場合は、null or 0）
	//引数：タイプ（int）(enum値)
	//戻値：なし
	void AddMetaAIMember(MetaType metaType, GameObject* pAddObject,
		int addCost, int type);


	//オブジェクトを解放する
	//特定オブジェクトの解放(Remove)
	//引数：自身を指揮していたメタAIのタイプ（自身のタイプを所有していたオブジェクトのポインタ）
	//引数：オブジェクト群から消去する自身のオブジェクトのポインタ
	//引数：オブジェクトのコスト（コストを持たない場合は、null or 0）
	//引数：タイプ（int）(enum値)
	//戻値：なし
	void RemoveMetaAIMember(MetaType metaType, GameObject* pRemoveObject,
		int subCost, int type);



	//情報取得(Csvより)
	//オブジェクト情報を所有しているCSVファイルから、オブジェクトの情報取得
		//取得可能情報：オブジェクト名、コスト
	//引数：CSVファイルタイプ (GameMetaAI.h :: enum値)（CSVファイルを指定し、タイプによって、CSVReaderを指定）
	//引数：オブジェクトタイプ（FishCsvInfo.h :: enum値）（必ず、CSVファイルに存在している、enum値に存在している値を使用する（enumによって示されるint値が必ず一致するようにする））
			//セルの行
	//戻値：オブジェクトのコスト
	int GetObjectInfoCsv(MetaCsvType metaCsvType, FISH_TYPE fishType);


	//オブジェクト別の優先度を取得
		//引数のオブジェクトタイプを列として、引数のrowで示されている優先度を取得
		//取得可能情報：オブジェクトごとの優先度（文字列）
	//引数：CSVファイルタイプ (GameMetaAI.h :: enum値)（CSVファイルを指定し、タイプによって、CSVReaderを指定）
	//引数：オブジェクトタイプ
			//セルの列
	//引数：優先度を決めるオブジェクトなどの番号や、タイプ（地面の番号をもとにオブジェクトの優先度を出すときに、地面の番号が入る）
			//セルの行
	//戻値：優先度の文字列
	std::string GetObjectPriorityCsv(MetaCsvType metaCsvType,
		FISH_TYPE fishType, int row);


	//地面情報を持っているCSVファイルから、
	//地面の座標系の情報を取得する
	//引数：地面のプレート番号
	//戻値：CSVファイルから取得した地面のプレート番号で示される座標
	XMVECTOR GetGroundPos(int groundPlateNumber);

	//地面のモデル番号を取得
	//引数：地面のプレート番号
	//戻値：地面のプレート番号で示される、モデルハンドル番号（Modelにロードしたモデルデータのモデル番号）
	int GetGroundModelHundle(int groundPlateNumber);

	//CSVより、
	//地面番号と、魚オブジェクト番号とで
	//魚オブジェクトごとのその地面における、出現高さの受け取り（出現位置の高さを０〜１の割合で受け取り、段階によって、出現位置を変更させる。）
	//引数：地面のプレート番号
	//引数：オブジェクトのタイプ（出現の座標の高さを知りたいオブジェクト）
	float GetObjectHeight(int groundPlateNumber, FISH_TYPE objectHundle);


	int GetWorldGroundHundle();


};


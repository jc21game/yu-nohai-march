//インクルード
#include "TitleScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"
#include "Engine/Model.h"
#include "Engine/SceneManager.h"


//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene")
	, TITLE_SIZE(1.2f)
	, TITLE_POS(0.3f)
	, START_SIZE(1.2f)
	, START_POS(-0.4f)
	, titleHandle_(-1)
	, startHandle_(-1)
	, imageAlpha_(0.1f)
	, alphaIncDec_(0.02f)
	, player_(nullptr)
	, credit_(-1)
	, creditPict_(-1)

{
	//クレジット表示位置
	creditPos_.position_ = XMVectorSet(-6.1f, 6, 11.5f, 0);
	creditPos_.rotate_ = XMVectorSet(0, 90, -0.2f, 0);
	pictPos_.position_ = XMVectorSet(-0.775f, -0.756f, 0, 0);
	pictPos_.scale_ = XMVectorSet(0.875f, 0.7775f, 0, 0);
}

//初期化
void TitleScene::Initialize()
{
	//タイトルロゴ画像のロード
	titleHandle_ = Image::Load("2DModel/title.png");
	assert(titleHandle_ >= 0);

	//スタートラベル画像のロード
	startHandle_ = Image::Load("2DModel/start.png");
	assert(startHandle_ >= 0);

	//クレジット画像ロード
	credit_ = Model::Load("Credit/Credit.fbx");
	creditPict_ = Image::Load("Credit/Credit2.png");
	assert(credit_ >= 0);
	assert(creditPict_ >= 0);
	Model::SetTransform(credit_, creditPos_);
	Image::SetTransform(creditPict_, pictPos_);

}

//更新
void TitleScene::Update()
{
	//「Enter」でプレイシーンへ
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//プレイヤーを動作可能に
		player_->Enter();

		//タイトルシーンの削除
		KillMe();
	}

	//アルファ値の設定
	if (imageAlpha_ <= 0.0f || imageAlpha_ >= 1.0f)
	{
		alphaIncDec_ *= -1.0f;
	}

	//アルファ値の増減
	imageAlpha_ += alphaIncDec_;

	/*マウスクリック位置との比較*/
	//ビューポート行列
	float w = Direct3D::screenWidth_ / 2.0f;
	float h = Direct3D::screenHeight_ / 2.0f;
	XMMATRIX vp = {
		w, 0, 0, 0,
		0, -h, 0, 0,
		0, 0, 1, 0,
		w, h, 0, 1
	};

	//各逆行列
	XMMATRIX invVP = XMMatrixInverse(nullptr, vp);
	XMMATRIX invPrj = XMMatrixInverse(nullptr, Camera::GetProjectionMatrix());
	XMMATRIX invView = XMMatrixInverse(nullptr, Camera::GetViewMatrix());
	XMMATRIX invMat = XMMatrixMultiply(XMMatrixMultiply(invVP, invPrj), invView);

	//クリック位置（手前）
	XMVECTOR mousePosFront = Input::GetMousePosition();
	mousePosFront.vecZ = 0;

	//クリック位置（奥）
	XMVECTOR mousePosBack = Input::GetMousePosition();
	mousePosBack.vecZ = 1.0f;

	//変換	
	mousePosFront = XMVector3TransformCoord(mousePosFront, invMat);
	mousePosBack = XMVector3TransformCoord(mousePosBack, invMat);

	//レイを飛ばす(マウスの位置)
	RayCastData data;
	data.start = mousePosFront;
	data.dir = mousePosBack - mousePosFront;
	Model::RayCast(credit_, &data);

	//クレジットをマウスクリックでクレジットシーンへ
	if (Input::IsMouseButton(0x00) && data.hit)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CREDIT);
	}
}

//描画
void TitleScene::Draw()
{
	//クレジット画像の表示
	Image::Draw(creditPict_);

	//タイトルロゴ画像の表示
	transform_.scale_ = XMVectorSet(TITLE_SIZE, TITLE_SIZE, 1.0f, 0.0f);
	transform_.position_.vecY = TITLE_POS;
	Image::SetTransform(titleHandle_, transform_);
	Image::Draw(titleHandle_);

	//スタートラベル画像の表示
	transform_.scale_ = XMVectorSet(START_SIZE, START_SIZE, 1.0f, 0.0f);
	transform_.position_.vecY = START_POS;
	Image::SetAlpha(startHandle_, imageAlpha_);
	Image::SetTransform(startHandle_, transform_);
	Image::Draw(startHandle_);
}

//開放
void TitleScene::Release()
{
}

//プレイヤー情報の設定
void TitleScene::SetPlayer(GameObject * player)
{
	player_ = player;
}

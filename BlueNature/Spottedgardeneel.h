#pragma once
#include "FishBase.h"

class BoxCollider;

//日本名：チンアナゴ
//英　名：スポッジデネールフィッシュを管理するクラス
class Spottedgardeneel : public FishBase
{
	//各種コライダー
	BoxCollider* pLeftEye_;		//左目
	BoxCollider* pRightEye_;	//右目
	BoxCollider* pBody_;		//身体

	//左目
	XMVECTOR LeftEyeCenter_;	//中心位置
	XMVECTOR LeftEyeRange_;		//認識範囲

	//右目
	XMVECTOR RightEyeCenter_;	//中心位置
	XMVECTOR RightEyeRange_;	//認識範囲

	//身体
	XMVECTOR bodyCenter_;		//中心位置
	XMVECTOR bodySize_;			//サイズ


public:
	//コンストラクタ
	//params : 親オブジェクト
	//return : NULL
	Spottedgardeneel(GameObject* parent);

	//デストラクタ
	//params : NULL
	//return : NULL
	~Spottedgardeneel();

	//初期化
	//params : NULL
	//return : NULL
	void Initialize() override;

	//更新
	//params : NULL
	//return : NULL
	void Update() override;

	//解放
	//params : NULL
	//return : NULL
	void Release() override;

	//衝突が起こった時の処理
	//params : 衝突した相手オブジェクト
	//return : NULL
	void OnCollision(GameObject* pTarget) override;

	//近づきたい相手を目視した場合に、認識フラグを変更する処理
	//params : 当たった対象オブジェクト
	//return : NULL
	void Look(GameObject* pTarget) override;
};


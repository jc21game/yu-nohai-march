#include "Blueheadtilefish.h"
#include "RunAway.h"
#include "Advance.h"
#include "DoLeftTurn.h"
#include "DoRightTurn.h"
#include "GetCloser.h"
#include "Engine/BoxCollider.h"
#include "GameMetaAI.h"



//コンストラクタ
Blueheadtilefish::Blueheadtilefish(GameObject* parent)
	: FishBase(parent, "Blueheadtilefish")
	, pLeftEye_(nullptr), pRightEye_(nullptr), pBody_(nullptr)
{

	//自身のオブジェクトのコストをセットする(正確に言うと更新、FishBaseにて、初期コストは設定されているので、更新を行わずともエラーにはならない)
	SetThisCost(10);

	//CSVファイルよりコストを取得
	//MetaCsvType：FISH_INFORMATION（魚情報CSVファイル）
	//FishType：BUTTER_FLY_FISH（チョウチョウウオ）
	//return：10
	int cost = GameMetaAI::GetObjectInfoCsv(FISH_INFORMATION, BLUE_HEADTILE);

	//自身のメンバの構造体変数に、自身のコストをセット（CSVファイルから読み込んだ）
	SetThisCost(cost);

	//基本の移動速度
	pStatus_->moveSpeed_ = 0.1f;

	//魚本体の当たり判定
	pStatus_->bodyCenter_ = XMVectorSet(0.f, 0.f, -11.f, 0.f);
	pStatus_->bodySize_ = XMVectorSet(10.f, 10.f, 26.f, 0.f);

	//左目の認識範囲
	pStatus_->leftEyeCenter_ = XMVectorSet(-7.f, 0.f, -6.f, 0.f);
	pStatus_->leftEyeRange_ = XMVectorSet(7.f, 6.f, -10.f, 0.f);

	//右目の認識範囲
	pStatus_->rightEyeCenter_ = XMVectorSet(7.f, 0.f, -6.f, 0.f);
	pStatus_->rightEyeRange_ = XMVectorSet(7.f, 6.f, 10.f, 0.f);

	//最大認識時間
	pStatus_->maxCongnitionTime_ = 1.f;
	pStatus_->maxVisuallyTime_ = 1.f;


	
	
}

//デストラクタ
Blueheadtilefish::~Blueheadtilefish()
{

}

//初期化
void Blueheadtilefish::Initialize()
{

	FishBase::Initialize();

	//モデルロード
	LoadModel("3DModel/MS2_3DModel/Blueheadtilefish/Blueheadtilefish.fbx");


	/**********各当たり判定(認識範囲)の追加**********/
	//身体
	pBody_ = new BoxCollider(pStatus_->bodyCenter_, pStatus_->bodySize_);
	AddCollider(pBody_);

	//左目
	pLeftEye_ = new BoxCollider(pStatus_->leftEyeCenter_, pStatus_->leftEyeRange_);
	AddCollider(pLeftEye_);

	//右目
	pRightEye_ = new BoxCollider(pStatus_->rightEyeCenter_, pStatus_->rightEyeRange_);
	AddCollider(pRightEye_);


	/*****行動追加*****/
	pCharAI_->AddBehavior(0, std::make_unique<RunAway>(this, "Player"));	//逃げる
	pCharAI_->AddBehavior(1, std::make_unique<GetCloser>(this, "Player"));

	/*pCharAI_->AddDefaultBehavior(ADVANCE_, std::make_unique<Advance>(this));
	pCharAI_->AddDefaultBehavior(LEFT_TURN_, std::make_unique<DoLeftTurn>(this));
	pCharAI_->AddDefaultBehavior(RIGHT_TURN_, std::make_unique<DoRightTurn>(this));*/


	//transform_.rotate_.vecZ -= 45;
	//transform_.rotate_.vecX = -45;


	//pCharAI_->AddBehavior(0, std::make_unique<GetCloser>(this, "Player"));	//近づく

	//メタAIに自身を追加させる（名前空間）
	//params1：自身を指揮するメタAIの種類・タイプ（enum値）
	//params2：自身のオブジェクトの標準コスト
	//params3：自身のオブジェクトの種類・タイプ（enum値→int）
	GameMetaAI::AddMetaAIMember(FISH_OPERATE_META, this, GetThisCost(), (int)BLUE_HEADTILE);
}

//更新
void Blueheadtilefish::Update()
{

	PlayAnimation();

	CalcVerticalAngle();

	//認識した
	DidCongnition();

	DidVisually();

	//見えているか
	Blueheadtilefish::Look(FindObject("Player"));

	//CharacterAIの実行
	pCharAI_->Execute();


}

//解放
void Blueheadtilefish::Release()
{
	//メタAIに自身を消去させる（名前空間）
	//params1：自身を指揮するメタAIの種類・タイプ（enum値）
	//params2：自身のオブジェクトの標準コスト
	//params3：自身のオブジェクトの種類・タイプ（enum値→int）
	GameMetaAI::RemoveMetaAIMember(FISH_OPERATE_META, this, GetThisCost(), (int)BLUE_HEADTILE);
}

void Blueheadtilefish::OnCollision(GameObject * pTarget)
{
	Timer::Reset();
	if (pTarget->GetObjectName() == "Player")
	{
		pStatus_->congnitionTime_ = 0;

		pStatus_->isCong_ = true;

	}
}

//近づきたい相手を目視した場合に、認識フラグを変更する処理
void Blueheadtilefish::Look(GameObject * pTarget)
{
	//相手がプレイヤーのとき
	if (pTarget->GetObjectName() == "Player")
	{
		InRange(this, pTarget, pStatus_->rightEyeRange_, pStatus_->leftEyeRange_);
		//右目か左目でプレイヤーを目視したとき
		if (pStatus_->isRightEye_ ^ pStatus_->isLeftEye_)
		{
			//目視フラグ更新
			pStatus_->isVis_ = true;
		}
	}
}

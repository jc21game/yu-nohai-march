#pragma once
#include "Behavior.h"
#include <string>

class GetCloser :public Behavior
{
	XMVECTOR ownPos_;		//自身の位置
	XMVECTOR tarPos_;		//相手の位置
	XMVECTOR arrive_;		//近づく際の目標地点
	XMVECTOR disPos_;		//自身と相手の位置の差
	XMVECTOR ownDir_;		//自身の方向
	XMVECTOR disRotate_;	//自身の方向ベクトルと自身と相手の位置の差の差
	XMVECTOR move_;			//移動方向ベクトル
	float rotateAngle_;		//回転したい角度
	float moveSpped_;		//近づく際のの速度
	float cutAngle_;		//分割した回転角度
	float cut_;				//分割数
	char randam_;			//ランダムで行動を決定

public:
	//コンストラクタ
	//params : ゲームオブジェクトポインタ、どのオブジェクトに近づくのか
	//return : NULL
	GetCloser(GameObject* pGameObject, std::string target);

	//デストラクタ
	//params : NULL
	//return : NULL
	~GetCloser();

	//実行可能か
	//params : NULL
	//return : 実行可能ならばTrue
	bool ShouldExecute() override;

	//実行
	//params : NULL
	//return : NULL
	void Execute() override;
};


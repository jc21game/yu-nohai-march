#pragma once
#include "Behavior.h"

//下降に関するクラス
class Descent : public Behavior
{
	float MAX_ROTATE_;
public:

	Descent(GameObject* pObject);

	~Descent();

	bool ShouldExecute() override;

	void Execute() override;
};


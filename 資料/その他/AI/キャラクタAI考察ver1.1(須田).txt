提案書　キャラクタの行動選択肢生成にストラテジを活用
FishAI.h
enum ACTIONS
{
MOVE,
ROTATION
};
class FishAI
{
virtual void Action() = 0;
};
//Move及びRotationクラスはフライウェイトを採用しても良い
class Move : public FishAI
{
//移動アクション
void Action() override;
};
class Rotation : public FishAI
{
//回転アクション
void Action() override;
};
class FishActions
{
public:
//デストラクタでmapで使用したクラスを解放(フライウェイト未使用)
~FishActions();
std::map<int, FishAI*>action;
action[MOVE]         = new Move();
action[ROTATION] = new Rotation ();
};
ｰｰｰｰｰｰｰｰｰｰｰｰｰｰ
FishA.h
#include "FishAI"
class FishA : public GameObject
{
public:
int selectAction;
FishActions* fa = new FishActions;
//認識
//状況判断関数
//選択肢検討
//状況判断によって得た情報からどの行動が好ましいかある程度考える
//行動決定
//selectAction = 選択肢検討で候補に上がった選択肢から選ばれた行動番号
//行動
fa->action[selectAction].Action();
};
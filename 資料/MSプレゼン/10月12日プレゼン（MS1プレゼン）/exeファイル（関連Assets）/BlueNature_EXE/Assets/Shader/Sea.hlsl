//───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D		g_texture: register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー

//───────────────────────────────────────
 // コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	float4x4	g_matWVP;			// ワールド・ビュー・プロジェクションの合成行列		「」
	float4x4	g_matNormalTrans;	// 法線の変換行列（回転行列と拡大の逆行列）			「」
	float4x4	g_matWorld;			// ワールド変換行列									「」
	float4		g_vecLightDir;		// ライトの方向ベクトル								「」
	float4		g_vecDiffuse;		// ディフューズカラー（マテリアルの色）				「」
	float4		g_vecAmbient;		// アンビエントカラー（影の色）						「」
	float4		g_vecSpeculer;		// スペキュラーカラー（ハイライトの色）				「」
	float4		g_vecCameraPosition;// 視点（カメラの位置）								「」
	float		g_shuniness;		// ハイライトの強さ（テカリ具合）					「」
	float		g_time;				// 経過時間											「」
	bool		g_isTexture;		// テクスチャ貼ってあるかどうか						「」

};

//───────────────────────────────────────
// 頂点シェーダー出力＆ピクセルシェーダー入力データ構造体
//───────────────────────────────────────
struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float4 normal : TEXCOORD2;		//法線
	float2 uv	  : TEXCOORD0;		//UV座標
	float4 pos2	  : TEXCOORD1;		//位置
};

//───────────────────────────────────────
// ゲルストナー波
//───────────────────────────────────────
void gerstnerWave(in float4 localVtx, float t, float waveLen, float Q, float R, float2 browDir, inout float4 localVtxPos, inout float4 localNormal) {

	browDir = normalize(browDir);
	const float pi = 3.1415926535f;
	const float grav = 9.8f;
	float A = waveLen / 12.0f;
	float _2pi_per_L = 2.0f * pi / waveLen;
	float d = dot(browDir, localVtx.xz);
	float th = _2pi_per_L * d + sqrt(grav / _2pi_per_L) * t;

	float4 posW = float4(0.0, R * A * sin(th), 0.0, 0.0);
	posW.xz = Q * A * browDir * cos(th);

	// ゲルストナー波の法線
	float4 normalW = float4(0.0, 1.0, 0.0, 0.0);
	normalW.xz = -browDir * R * cos(th) / (7.0f / pi - Q * browDir * browDir * sin(th));

	localVtxPos += posW;
	localNormal += normalize(normalW);
}

//───────────────────────────────────────
// 頂点シェーダ
//───────────────────────────────────────
VS_OUT VS(float4 pos : POSITION, float4 Normal : NORMAL, float2 Uv : TEXCOORD)
{
	VS_OUT outData;

	outData.pos2 = pos;

	float4 oPosW = float4(0.0, 0.0, 0.0, 0.0);
	float4 oNormalW = float4(0.0, 0.0, 0.0, 0.0);
	float t = g_time;
	gerstnerWave(outData.pos2, t + 2.0, 0.8, 0.7, 0.3, float2(0.2, 0.3), oPosW, oNormalW);
	gerstnerWave(outData.pos2, t, 1.2, 0.3, 0.5, float2(-0.4, 0.7), oPosW, oNormalW);
	//gerstnerWave(outData.pos2, t + 3.0, 1.8, 0.3, 0.5, float2(0.4, 0.4), oPosW, oNormalW);
	//gerstnerWave(outData.pos2, t, 2.2, 0.4, 0.4, float2(-0.3, 0.6), oPosW, oNormalW);
	outData.pos2 += oPosW;

	// 座標変換
	outData.pos = mul(outData.pos2, g_matWVP);
	outData.uv = Uv;
	outData.normal = normalize(oNormalW);

	return outData;
}

//───────────────────────────────────────
// フレネル反射率を算出
//───────────────────────────────────────
float fresnel(in float4 toCameraDirW, in float4 normalW, in float n1, in float n2) {
	float A = n1 / n2;
	float B = dot(toCameraDirW, normalW);
	float C = sqrt(1.0 - A * A * (1.0 - B * B));
	float V1 = (A * B - C) / (A * B + C);
	float V2 = (A * C - B) / (A * C + B);
	return (V1 * V1 + V2 * V2) * 0.5;
}


//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(VS_OUT inData) : SV_Target
{

	// sample the texture
	float4 col = float4(1.0, 1.0, 1.0, 1.0);
	float4 normalW = normalize(inData.normal);
	float4 lightDir = g_vecLightDir;
	float4 toLightDirW = normalize(lightDir);
	float4 waterColor = g_vecDiffuse;
	waterColor[3] = 0.9;

	// フレネル反射率算出
	// 頂点座標からカメラへのレイと法線
	float camera = g_vecCameraPosition;
	float4 fromVtxToCameraW = normalize(camera - inData.pos2);
	//float R = fresnel(fromVtxToCameraW, normalW, 1.000292, 1.3334); // 反射率

	// 視線に対して垂直に近い面は薄いのでちょっと発光
	float4 lightColor = float4(0.5, 1.0, 1.0, 1.0);
	float waveTopAddPower = 1.0;
	float waveTopAddColor = lightColor * pow(0.85 - abs(normalW), 10.0) * waveTopAddPower;

	// 環境マップサンプル
	//float4 reflectDirW = reflect(-fromVtxToCameraW, normalW);
	float4 envColor = g_vecAmbient;//UNITY_SAMPLE_TEXCUBE(_Cubemap, reflectDirW);

	// ディフューズ色
	// 水の色と環境マップを合成 → ライトとの角度で陰影
	float4 srcColor = waterColor;// *(1.0 - R) + envColor * R;
	float diffusePower = dot(normalW, -toLightDirW);
	float4 diffuseColor = srcColor * diffusePower;

	// スペキュラ
	float4 halfDirW = normalize(toLightDirW + fromVtxToCameraW);
	float4 specularColor = pow(max(0.0, dot(halfDirW, normalW)), 30.0f);
	col = diffuseColor + waveTopAddColor + specularColor * 0.75f;

	//UNITY_APPLY_FOG(i.fogCoord, col);

	return col;
}